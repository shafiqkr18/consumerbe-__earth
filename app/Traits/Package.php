<?php

namespace App\Traits;

trait Package
{
    /*
    |--------------------------------------------------------------------------
    | Define which config is being queried
    |--------------------------------------------------------------------------
    |
    |
    */

    public function set_config($index=null,$payload=null){

        $els_config_root        =   env( 'API_VERSION' ) . '/els/';
        $els_config_env_root    =   env( 'API_VERSION' ) . '/els/' . env( 'ELS_ENVIRONMENT' ) . '/';

        if( $this::datastore === "els" ){

            $search             =   new \SearchPKG\Controllers\SearchController;

            return $search
                        ->set_index($index)
                        ->set_configuration_file( load( $els_config_env_root.$index ) )
                        ->set_connection_credentials( array_get(load( $els_config_root.'aws' ),'credentials',[]) )
                        ->set_host( array_get(load($els_config_root.'aws'), 'host', '') )
                        ->set_search_array( $payload )
                        ->connect();

        }

        if( $this::datastore === "els_a" ){

            $analytic       =   new \SearchPKG\Controllers\AnalyticController;

            return $analytic
                        ->set_index($index)
                        ->set_configuration_file( load( $els_config_env_root.$index ) )
                        ->set_connection_credentials( array_get(load( $els_config_root.'aws' ),'credentials',[]) )
                        ->set_host( array_get(load($els_config_root.'aws'), 'host', '') )
                        ->set_search_array( $payload )
                        ->connect();

        }

        if( $this::datastore === "els_u" ){

            $utility        =   new \SearchPKG\Controllers\UtilityController;
            $config_root    =   env( 'API_VERSION' ). '/els/';

            return $utility
                        ->set_connection_credentials( array_get(load( $config_root.'aws' ),'credentials',[]) )
                        ->set_host( array_get(load($config_root.'aws'), 'host', '') )
                        ->connect();

        }

        // NO/SQL databases
        else if( $this::datastore === "ddb" ){

            $data      =   new \DataPKG\Controllers\DataController;

            return $data
                        ->setConfig( load( env( 'API_VERSION' ). '_els/'.$index ) )
                        ->setCredentials( load( 'aws' )[ env( 'API_VERSION' ) ][ 'credentials' ] )
                        ->connect();

        }
        else{

            return response()->json( array( 'messaging' => 'datastore ' . $datastore . ' not identified' ), 403 );

        }

    }

}
