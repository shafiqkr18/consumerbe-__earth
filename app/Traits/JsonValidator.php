<?php

namespace App\Traits;

trait JsonValidator
{

    protected   $leaf, $payload, $method;

    public function set_leaf($leaf){
        $this->leaf     =   $leaf;
        return $this;
    }

    public function set_method($method){
        $this->method     =   $method;
        return $this;
    }

    public function set_payload($payload){
        $this->payload  =   $payload;
        return $this;
    }

    public function validate_request($schema_path=''){

        /**
         *  1.   Pick schema
         *  2.   Initiate validation
         *  3.   Return results
         */

        $schema_path        =   ( !empty($schema_path) ) ? $schema_path : storage_path().'/schemas/'.env('API_VERSION').'/request.json';
        $leaf               =   $this->leaf;
        $method             =   $this->method;
        $payload            =   $this->payload;
        // dd($leaf,$method,$payload);

        #   1
        $schemas            =   json_decode( file_get_contents( $schema_path ) );
        $schema             =   '';

        if(str_contains($leaf,'.')){
            $leafs      =   explode('.',$leaf);
            $schema     =   $schemas->controllers;
            foreach($leafs as $key => $value){
                if(!empty($schema->$value)){
                    $schema = $schema->$value;
                }
            }
            $schema =   !empty($schema->$method) ? $schema->$method : '';
        }
        else{
            $schema =   !empty($schemas->controllers->$leaf->$method) ? $schemas->controllers->$leaf->$method : '';
        }

        // dd($leaf, $method, $payload, $schema);

        $schemaStorage      =   new \JsonSchema\SchemaStorage();
        $schemaStorage->addSchema( $schema_path, $schema );

        #   2
        $validator          =   new \JsonSchema\Validator( new \JsonSchema\Constraints\Factory($schemaStorage) );
        $validator->validate( $payload, $schema );

        // dd( $payload,$schema,$validator );

        $errors             =   [];

        #   3
        if ($validator->isValid()) {
            return true;
        }
        else {
            //JSON does not validate. Violations
            foreach ($validator->getErrors() as $error) {
                $errors[$error['property']] = sprintf("%s",$error['message']);
            }
            return $errors;
        }

    }

    public function validate_response( $response, $leaf_cruds ){
        // dd($response);
        list($leaf,$method)     =   explode(".",$leaf_cruds);

        #   3
        $schema_path        =   storage_path().'/schemas/'.env('API_VERSION').'/response.json';
        $schemas            =   json_decode( file_get_contents( $schema_path ) );
        $schema             =   $schemas->schema->$leaf->$method;

        // dd($schemas,$schema);

        $schemaStorage      =   new \JsonSchema\SchemaStorage();
        $schemaStorage->addSchema( $schema_path, $schema );

        #   4
        $validator          =   new \JsonSchema\Validator( new \JsonSchema\Constraints\Factory($schemaStorage));
        $validator->validate( $response, $schema, \JsonSchema\Constraints\Constraint::CHECK_MODE_APPLY_DEFAULTS );

        // dd( $leaf,$response,$schema,$validator );

        $errors             =   [];

        if ($validator->isValid()) {
            return true;
        }
        else {
            echo "JSON does not validate. Violations:\n";
            foreach ($validator->getErrors() as $error) {
                echo "<br>".sprintf("[%s] %s\n", $error['property'], $error['message']);
            }
        }

    }

}
