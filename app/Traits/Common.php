<?php

namespace App\Traits;

trait Common
{
    /*
    |--------------------------------------------------------------------------
    | Set id per index
    |--------------------------------------------------------------------------
    |
    |
    */

    public function generate_id($model){

        switch( $model ){
            case 'property':
                $ref_no     =   preg_replace(['#/+#','/\\\\/'],'-',array_get( $this->payload, 'ref_no' ));
                $id         =   'zp-'.$ref_no;
                break;
            case 'agent':
                $email      =   rtrim( strtr( base64_encode(array_get($this->payload, 'contact.email')), '+/', '-_' ), '=');
                $id         =   'za-'.$email;
                break;
            case 'agency':
                $name       =   preg_replace('/-+/', '-', kebab_case(preg_replace('/[^a-z0-9]/i', ' ', strtolower(array_get($this->payload, 'contact.name')))));
                $suffix     =   !empty(array_get($this->payload,'contact.orn')) ? array_get($this->payload,'contact.orn') : random_number(5);
                $id         =   'zb-'.$name.'-'.$suffix;
                break;
            case 'developer':
                $name       =   preg_replace('/-+/', '-', kebab_case(preg_replace('/[^a-z0-9]/i', ' ', strtolower(array_get($this->payload, 'contact.name')))));
                $suffix     =   !empty(array_get($this->payload,'contact.orn')) ? array_get($this->payload,'contact.orn') : random_number(5);
                $id         =   'zd-'.$name.'-'.$suffix;
                break;
            case 'project':
                $name       =   preg_replace('/-+/', '-', kebab_case(preg_replace('/[^a-z0-9]/i', ' ', strtolower(array_get($this->payload, 'name')))));
                $id         =   'zj-'.$name;
                break;
            case 'service_type':
                $name       =   preg_replace('/-+/', '-', kebab_case(preg_replace('/[^a-z0-9]/i', ' ', strtolower(array_get($this->payload, 'name')))));
                $id         =   'zst-'.$name;
                break;
            case 'service':
                $name       =   preg_replace('/-+/', '-', kebab_case(preg_replace('/[^a-z0-9]/i', ' ', strtolower(array_get($this->payload, 'contact.name')))));
                $id         =   'zs-'.$name;
                break;
            case 'agent_analytics':
                $id         =   'zaa-'.random_number();
                break;
            case 'developer_analytics':
                $id         =   'zda-'.random_number();
                break;
            case 'project_analytics':
                $id         =   'zja-'.random_number();
                break;
            case 'property_analytics':
                $id         =   'zpa-'.random_number();
                break;
            case 'service_analytics':
                $id         =   'zsa-'.random_number();
                break;
            case 'takeover':
                $agency     =   preg_replace('/-+/', '-', kebab_case(preg_replace('/[^a-z0-9]/i', ' ', strtolower(array_get($this->payload, 'agency_id','no-agency')))));
                $project    =   preg_replace('/-+/', '-', kebab_case(preg_replace('/[^a-z0-9]/i', ' ', strtolower(array_get($this->payload, 'project_id','no-project')))));
                $id         =   'takeover-'.encrypt_data($agency.'-'.$project);
                break;
            case 'publicity_video':
                $id         =   'zpv-'.random_number();
                break;
            default:
                return response()->json( [ 'message' => 'Invalid leaf' ], 400 );
        }
        if( !empty( $id ) ){
            return $id;
        }
    }

    public function get_details($model){
        if( array_has( $this->payload, $model.'._id' ) || $model == 'service_type'){
            $id         =   array_get( $this->payload, $model.'._id' );
            $id         =   ($model == 'service_type') ? array_get( $this->payload, 'type._id' ) : $id;
            $details    =   $this->get_model_details($model, $id);

            if( !empty( $details ) && !is_null(array_get($details,'_id'))){
                if( !empty( $id ) && $id != 'invalid' ){
                    return $details;
                }
                else{
                    return response()->json( [ 'message' => $model.' does not exist.' ], 400 );
                }
            }
        }
        else{
            return response()->json( [ 'message' => 'Please select '.$model ], 400 );
        }
    }

    public function get_model_details($model, $id){
        switch( $model ){
            case 'property':
                $id         =   starts_with( $id, 'zp-' ) ? $id : 'invalid';
                $details    =   $id != 'invalid' ? app('App\Http\Controllers\Property')->show($id)->getData(1) : '';
                break;
            case 'agent':
                $id         =   starts_with( $id, 'za-' ) ? $id : 'invalid';
                $details    =   $id != 'invalid' ? app('App\Http\Controllers\Agent')->show($id)->getData(1) : '';
                break;
            case 'agency':
                $id         =   starts_with( $id, 'zb-' ) ? $id : 'invalid';
                $details    =   $id != 'invalid' ? app('App\Http\Controllers\Agency')->show($id)->getData(1) : '';
                break;
            case 'developer':
                $id         =   starts_with( $id, 'zb-' ) ? $id : 'invalid';
                $details    =   $id != 'invalid' ? app('App\Http\Controllers\Agency')->show($id)->getData(1) : '';
                break;
            case 'project':
                $id         =   starts_with( $id, 'zj-' ) ? $id : 'invalid';
                $details    =   $id != 'invalid' ? app('App\Http\Controllers\Project')->show($id)->getData(1) : '';
                break;
            case 'service_type':
                $id         =   array_has( $this->payload, $model.'._id' ) ? array_get( $this->payload, $model.'._id' ) : array_has( $this->payload, 'type._id' ) ? array_get( $this->payload, 'type._id' ) : '' ;
                $id         =   starts_with( $id, 'zst-' ) ? $id : 'invalid';
                $details    =   $id != 'invalid' ? app('App\Http\Controllers\ServiceType')->show($id)->getData(1) : '';
                break;
            case 'service':
                $id         =   starts_with( $id, 'zs-' ) ? $id : 'invalid';
                $details    =   $id != 'invalid' ? app('App\Http\Controllers\Service')->show($id)->getData(1) : '';
                break;
            default:
                return response()->json( [ 'message' => 'Invalid Model' ], 400 );
        }
        return $details;
    }

    public function validate_id($model, $id){

        switch( $model ){
            case 'property':
                $valid   = starts_with($id, 'zp-');
                break;
            case 'agent':
                $valid   = starts_with($id, 'za-');
                break;
            case 'agency':
                $valid   = starts_with($id, 'zb-');
                break;
            case 'developer':
                $valid   = starts_with($id, 'zd-');
                break;
            case 'project':
                $valid   = starts_with($id, 'zj-');
                break;
            case 'service':
                $valid   = starts_with($id, 'zs-');
                break;
            case 'property_analytics':
                $valid   = starts_with($id, 'zpa-');
                break;
            case 'agent_analytics':
                $valid   = starts_with($id, 'zaa-');
                break;
            case 'developer_analytics':
                $valid   = starts_with($id, 'zda-');
                break;
            case 'project_analytics':
                $valid   = starts_with($id, 'zja-');
                break;
            case 'service_analytics':
                $valid   = starts_with($id, 'zsa-');
                break;
            case 'publicity_video':
                $valid   = starts_with($id, 'zpv-');
                break;
            default:
                return response()->json( [ 'message' => 'Invalid leaf' ], 400 );
        }
        if( !empty( $valid ) ){
            return $valid;
        }
    }

    public function search_json( $payload, $json_data ){

        $payload    =   array_dot( $payload );

        // Group arrays together
        foreach( $payload as $k => $v ){
            // Identify and group min, max range
            $suffix     =   substr(strrchr($k, "."), 1);

            if( $suffix == 'min' || $suffix == 'max' ){
                if( preg_match('/(.*?).'.$suffix.'/', $k, $match) == 1 ){
                    $payload[$match[1]][$suffix]   =  $v;
                }
                array_forget( $payload, $k );
            }
            else{
                if( preg_match( "/.\d{1,2}/", $k, $match ) == 1 ){
                    $payload[ preg_replace( "/.\d{1,2}/", "", $k ) ][]   =  $v;
                    array_forget( $payload, $k );
                }
                else{
                    $payload[ $k ]   =  $v;
                }
            }
        }

        $size       =   array_get( $payload, 'size', 50 );
        array_forget( $payload, 'size' );

        //Filter results
        $results    =   collect( $json_data );
        foreach( $payload as $keys => $value ){
            $results     =   $results->filter(function( $resource ) use( $keys, $value, $payload ) {
                $resource    =   array_dot( $resource );
                foreach( $resource as $k => $v ){
                    if( preg_match( "/.\d{1,2}/", $k, $match ) == 1 ){
                        $resource[ preg_replace( "/.\d{1,2}/", "", $k ) ][]   =  $v;
                        array_forget( $resource, $k );
                    }
                    else{
                        $resource[ $k ]   =  $v;
                    }
                }

                if( is_array( $value ) ){
                    foreach( $value as $k => $v ){
                        if( $k === 'min' || $k === 'max' ){
                            if( $k === 'min' ){
                                return $payload[ $keys ][ $k ] <= $resource[ $keys ];
                            }
                            else{
                                return $payload[ $keys ][ $k ] >= $resource[ $keys ];
                            }
                        }
                        else{
                            if( !empty($resource[ $keys ]) && is_array($resource[ $keys ])){
                                foreach( $value as $i => $j ){
                                    return in_array( $j, $resource[ $keys ] );
                                }
                            }
                            else{
                                return in_array( $resource[ $keys ], $value );
                            }
                        }
                    }
                }
                elseif(isset($resource[ $keys ])){
                    return $payload[ $keys ] == $resource[ $keys ];
                }

            });
        }

        return [ count( $results ), $results->slice( 0, $size )->values()->toArray() ];

    }

    public function format_seo_article_response($data){
        $formatted_response = [];
        $map    = [
            "titles"            => "title",
            "titles_ar"         => "title_ar",
            "descriptions"      => "description",
            "descriptions_ar"   => "description_ar",
            "images"            => "image",
            "images_alt"        => "image_alt",
            "images_alt_ar"     => "image_alt_ar"
        ];
        foreach($data as $key => $value){
            if(isset($map[$key])){
                foreach($value as $k => $v){
                    $k++;
                    $formatted_response['section_'.$k][$map[$key]] = $v;
                }
            }
            else{
                $formatted_response[$key]   =   $value;
            }
        }
        return $formatted_response;
    }
}
