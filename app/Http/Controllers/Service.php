<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class Service extends Controller
{
    const leaf          =   'service';
    const datastore     =   'els';
    const metadata      =   [];

    use \App\Traits\Package;

    public function store( Request $request = null ){
        $this->payload  =  collect( $request->all() )->toArray();

        if( !empty(array_get( $this->payload, 'contact.name' ) ) ){
            $id             =   $this->generate_id($this::leaf);
            $exists         =   app('App\Http\Controllers\Service')->show($id)->getData(1);
            if(!array_has($exists, 'contact.name')){
                if(!array_has( $this->payload, 'type.name' )){
                    array_set( $this->payload, 'type', $this->get_details('service_type') );
                }
                array_set( $this->payload, 'stub', str_replace( 'zs-', '', $id ) );
                return Parent::store();
            }
            else{
                return response()->json(['message' => 'Service already exists'], 400);
            }
        }
        else{
            return response()->json(['message' => 'Please enter name of the Service'], 400);
        }
    }

    protected function construct_query_for_similar( $service ){

        return [
            'settings'  =>  [
                'status'    =>  array_get( $service, 'settings.status', true )
            ],
            'size'      =>  3
        ];

    }

}
