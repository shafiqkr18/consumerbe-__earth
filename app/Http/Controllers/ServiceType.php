<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ServiceType extends Controller
{

    const leaf          =   'service_type';
    const datastore     =   'els';
    const metadata      =   [];

    use \App\Traits\Package;

    public function store( Request $request = null ){

        $this->payload  =  collect( $request->all() )->toArray();

        if( !empty(array_get( $this->payload, 'name' ) ) ){
            $id             =   $this->generate_id($this::leaf);
            $exists         =   app('App\Http\Controllers\ServiceType')->show($id)->getData(1);
            if(!array_has($exists, 'name')){
                array_set( $this->payload, 'stub', str_replace( 'zst-', '', $id ) );
                return Parent::store();
            }
            else{
                return response()->json(['message' => 'Service type already exists'], 400);
            }
        }
        else{
            return response()->json(['message' => 'Please enter type of the Service'], 400);
        }
    }

    protected function construct_query_for_similar( $service ){

        return [
            'settings'  =>  [
                'status'    =>  array_get( $service, 'settings.status', true )
            ],
            'size'      =>  3
        ];

    }

}
