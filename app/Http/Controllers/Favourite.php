<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use BaoPham\DynamoDb\Facades\DynamoDb;

class Favourite extends User
{
    const leaf          =   'favourite';
    const datastore     =   'ddb';
    const metadata      =   [];

    public function __construct(Request $request){
        Parent::__construct();
        $this->payload      =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : '';
    }
    /**
    * Display a listing of the favourites.
    *
    * @param  string  $user_id, $model, $model_id
    * @return \Illuminate\Http\JsonResponse
    */
    public function index(Request $request, ...$vars)
    {
        $user_id    =   array_get($vars,'0');
        $model      =   array_get($vars,'1');
        $email      =   $this->decrypt_user($user_id);
        #Get all favourites of the user
        $favourite_rsp =  DynamoDb::table('favourites-uae')
                      ->setKeyConditionExpression('#email = :email')
                      ->setExpressionAttributeNames(['#email' => 'email'])
                      ->setExpressionAttributeValues([':email' => DynamoDb::marshalValue($email)])
                      ->prepare()
                      ->query()
                      ->toArray();
        $favourites = [];
        if(!empty($favourite_rsp['Items'])){
            $filter_favourites =  [];
            foreach($favourite_rsp['Items'] as $item){
                $filter_favourites[] = array_undot(DynamoDb::unmarshalItem($item));
            }
            if($model){
                $filter_favourites = collect($filter_favourites);
                if(array_has($this->payload,'subtype')){
                    $filter_favourites = $filter_favourites->where('type', $model)
                                                           ->whereIn('subtype', array_get($this->payload,'subtype'));
                }
                else{
                    $filter_favourites = $filter_favourites->where('type', strtolower($model));
                }
                $filter_favourites = array_values($filter_favourites->toArray());
            }
            if(!empty($filter_favourites)){
                $favourites['total']    =   $favourite_rsp['Count'];
                $favourites['data']     =   $filter_favourites;
            }
        }
        $favourites = !empty($favourites)?$favourites:[ 'message' => 'No Results' ];
        return response()->json($favourites, 200);
    }

    /**
    * Store a newly created favourite in storage.
    *
    * @param  string  $user_id, $model, $model_id
    * @return \Illuminate\Http\JsonResponse
    */
    public function store(Request $request, ...$vars)
    {
        // User
        // Model being favourited
        // ID of model
        /**
         * 1.   Get model data
         * 2.   Store
         *
         */
        $user       =   array_get($vars,'0',false);
        $model      =   array_get($vars,'1',false);
        $model_id   =   array_get($vars,'2',false);
        $favourite  =   [];
        if($model){
            // Favourite a model
            if($model_id){
                // TODO Favourite::Agent, Favourite::Agency, Favourite::Project, Favourite::Developer
                // 1.
                if($model === 'property'){
                    $model_data         =   app('\App\Http\Controllers\Property')->show($model_id)->getData(1);
                    //
                    $favourite_model    =   new \DataPKG\Models\FavouritePropertyDdb;
                }
                //  If model_id isn't present, subtype is search
                array_set($favourite,'subtype','model');
            }
            // Favouriting a search
            else{
                // 1.
                if($model === 'property'){
                    $model_data         =   $request->all();
                    //
                    $favourite_model    =   new \DataPKG\Models\FavouritePropertySearchDdb;
                }
                //  If model_id isn't present, subtype is search
                array_set($favourite,'subtype','search');
            }

            array_set($favourite,$model,$model_data);
            //
            array_set($favourite,'email',$this->decrypt_user($user));
            //
            array_set($favourite,'_id',md5(serialize($model_data)));
            //
            array_set($favourite,'type',$model);

        }

        $favourite  =   array_dot($favourite);

        foreach($favourite as $key=>$value){
            $favourite_model->{$key}  = $value;
        }
        // dd($favourite,$favourite_model);
        //
        if($favourite_model->save()){
            return response()->json(array_undot($favourite),200);
        }
        else{
            return response()->json(['Could not favourite Property'], 503);
        }
    }

    /**
    * Display the specified favourite.
    *
    * @param  string  $user_id, $model, $model_id
    * @return \Illuminate\Http\JsonResponse
    */
    public function show(...$vars)
    {
      $user_id    =   array_get($vars,'0');
      $model      =   array_get($vars,'1');
      $model_id   =   array_get($vars,'2');
      $email      =   $this->decrypt_user($user_id);
      if($model){
          # Favourite id
          if($model_id){
            $favourite =  DynamoDb::table('favourites-uae')
                      ->setKey(DynamoDb::marshalItem([ 'email' => $email, '_id' => $model_id ]))
                      ->prepare()
                      ->getItem()
                      ->toArray();
            $favourite_data =  array_undot(DynamoDb::unmarshalItem($favourite['Item']));
            $favourite_data = !empty($favourite_data)?$favourite_data:[ 'message' => 'No Results' ];
            return response()->json($favourite_data, 200);
          }
          else{
            return response()->json(['Model id must be set'], 404);
          }
      }
      else{
        return response()->json(['Invalid Model'], 404);
      }
    }

    /**
    * Remove the specified favourite from storage.
    *
    * @param  string  $user_id, $model, $model_id
    * @return \Illuminate\Http\JsonResponse
    */
    public function destroy(...$vars)
    {
        $user_id    =   array_get($vars,'0');
        $model      =   array_get($vars,'1');
        $model_id   =   array_get($vars,'2');
        $email      =   $this->decrypt_user($user_id);

        DynamoDb::table('favourites-uae')
                ->setKey(DynamoDb::marshalItem([ 'email' => $email, '_id' => $model_id ]))
                ->prepare()
                ->deleteItem();

        return response()->json( [ 'message' => 'Record deleted successfully' ], 200 );
    }
}
