<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class Agency extends Controller
{

    const leaf          =   'agency';
    const datastore     =   'els';
    const metadata      =   [];

    use \App\Traits\Common;

    protected function construct_query_for_similar( $agency ){

        return [
            'contact'  =>  [
                'address' => [
                    'state' => [
                        array_get( $agency, 'contact.address.state', '' )
                    ],
                    'city' => [
                        array_get( $agency, 'contact.address.city', '' )
                    ]
                ]
            ],
            'settings'  =>  [
                'status'    =>  array_get( $agency, 'settings.status', 1 )
            ],
            'size'      =>  5
        ];

    }

    public function store( Request $request = null )
    {
        /**
        * Register RDS
        * Store els
        * Upload logo S3
        */
        $this->payload  =  collect( $request->all() )->toArray();
        if(!array_has($this->payload, 'credentials.password') || !array_has($this->payload, 'credentials.password_confirmation')){
            return response()->json( [ 'error' => 'password and password_confirmation are required' ], 404 );
        }
        else{
            //RDS format
            if( array_get($this->payload,'credentials.password',1) === array_get($this->payload,'credentials.password_confirmation',2) ){
                if(array_has($this->payload, 'settings.role_id') && !in_array(array_get($this->payload,'settings.role_id',0),['4','5','6'])){
                    array_forget( $this->payload,'settings.role_id' );
                }
                $credentials = array_get($this->payload, 'credentials');
                array_set($credentials,'name',array_get($this->payload, 'contact.name'));
                array_set($credentials,'email',array_get($this->payload, 'contact.email'));
                array_set($credentials,'role_id',array_get($this->payload, 'settings.role_id', 4));
                // Generate Id
                if(!array_has($this->payload, '_id')){
                    array_set($this->payload, '_id', $this->generate_id($this::leaf));
                }
                $credentials['_id'] =  array_get($this->payload, '_id');
                $user_data = new \Illuminate\Http\Request();
                $auth  =  new Auth();
                $auth_rsp = $auth->register($user_data->replace($credentials));
                if($auth_rsp->getStatusCode() == 201){
                    if(!array_has($this->payload,'settings.payment_tier')){
                        array_set($this->payload,'settings.payment_tier',false);
                    }
                    array_forget($this->payload, ['credentials', 'logo']);
                    // ELS
                    return Parent::store();
                }
                else{
                    return $auth_rsp;
                }
            }
            else{
                return response()->json(['message' => 'Passwords dont match'], 400);
            }
        }
    }

    public function update(Request $request, ...$vars){
        /**
        * Update Index
        * Bulk update dependencies
        */

        $bulk = array_get($this->payload, 'bulk', false);
        $update_response = Parent::update($request,...$vars);

        if($update_response->getStatusCode() == 200){
            if(array_has($this->payload,'settings.role_id')){
                $auth  =  new Auth();
                $auth->update($request,array_get($vars,'0'));
            }
        }

        //Bulk Update agent and properties
        if($bulk && $update_response->getStatusCode() == 200){
            /** Agent document update_by_query */
            $this->payload                      =  [];
            $this->payload[$this::leaf]['_id']  =  array_get($vars,'0',null);
            $query = $this->payload;
            array_set($this->payload, $this::leaf, $this->get_details($this::leaf));
            Parent::update_by_query($query,$this->payload,'agent');
            /** Property document update_by_query */
            $prefix = 'agent';
            $new_payload[$prefix] = $this->payload;
            $new_query[$prefix]   = $query;
            Parent::update_by_query($new_query,$new_payload,'property', $prefix);
            /** Project document update_by_query */
            $prefix = 'developer';
            $project_query[$prefix]     =   array_get($query,'agency');
            $project_payload[$prefix]   =   array_get($this->payload,'agency');
            Parent::update_by_query($project_query,$project_payload,'project');
            return $update_response;
        }
        else{
            return $update_response;
        }
    }

}
