<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class S3 extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | AWS S3 functions
    |--------------------------------------------------------------------------
    */

    /**
    * If object exist in an S3 bucket
    * @param  [type] $bucket [description]
    * @param  [type] $key    [description]
    * @return [type]         [description]
    */
    public function does_s3_object_exist( $bucket, $key ){

        try{
            return $this->connect()->doesObjectExist( $bucket, $key );
        }
        catch( Aws\S3\Exception\S3Exception $e) {

            return response()->json(['message' => 'Error: Trying to set S3 Object'], $e->getCode());

        }

    }

    /**
    * Put Object in S3 bucket
    * @param  [type] $bucket        [description]
    * @param  [type] $key           [description]
    * @return [type] $file          [description]
    * @return [type] $encryption    [description]
    */
    public function put_s3_object( $bucket, $key, $file ){

        try{
            $this->connect()->putObject([
                'Bucket' 					=>		$bucket,
                'Key' 					    =>		$key,
                'SourceFile' 				=>		$file,
                'CacheControl'      =>    'max-age=7884000,public',
                'Expires'           =>    'Sun, 01 Jan 2034 00:00:00 GMT',
                'StorageClass'      =>    'REDUCED_REDUNDANCY',
                'ACL'               =>    'public-read'
            ]);

        } catch( \Aws\S3\Exception\S3Exception $e ) {
            return response()->json(['message' => 'Error: Trying to set S3 Object'], $e->getCode());

        }

    }

    /**
    * Get data from an S3 bucket
    * @param  [type] $bucket [description]
    * @param  [type] $key    [description]
    * @return [type]         [description]
    */
    public function get_all_s3_objects( $bucket, $prefix, $encryption = 'none' ){

        try {

            $files 	=	$this->connect()->listObjects([
                'Bucket' 				   =>		$bucket,
                'Prefix'                   =>       $prefix,
                'ServerSideEncryption' 	   =>		$encryption
            ]);

            return $files->getPath( 'Contents' );

        } catch( \Aws\S3\Exception\S3Exception $e) {

            return $this->ses_manager->send_error_email( 'Error: Trying to set S3 Object', $e );

        }

    }

    public function get_s3_object( $bucket, $key, $encryption = 'none' ){
        try {
            $files 	=	$this->connect()->getObject([
                'Bucket' 				        =>		$bucket,
                'Key' 				        => 		$key,
                'ServerSideEncryption' 	    =>		$encryption
            ]);
            return response()->json( [ $files->get('Body')->getContents() ], 200 );
        }
        catch( \Aws\S3\Exception\S3Exception $e) {
            return response()->json( array( 'error' => $e->getMessage() ), $e->getCode() );
        }
    }

    /*
    |--------------------------------------------------------------------------
    | AWS S3 connect
    |--------------------------------------------------------------------------
    */
    private function connect(){
        try{
            $config_root    =   env( 'API_VERSION' ). '/els/';
            return new \Aws\S3\S3Client([
                'region'        =>   array_get(load( $config_root.'aws' ),'credentials.region',''),
                'version'       =>  'latest',
                'credentials'   =>  [
                    'key'           =>  array_get(load( $config_root.'aws' ),'credentials.key',''),
                    'secret'        =>  array_get(load( $config_root.'aws' ),'credentials.secret','')
                ]
            ]);
        }
        catch ( Aws\S3\Exception\S3Exception $e ) {
            return  $this->ses_manager->send_error_email( 'Error: Trying to connect S3', $e );
        }
    }
}
