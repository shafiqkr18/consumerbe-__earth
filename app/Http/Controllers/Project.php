<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class Project extends Controller
{

    const leaf          =   'project';
    const datastore     =   'els';
    const metadata      =   [];

    use \App\Traits\Package;

    public function store(Request $request = null){

        /**
         *  1.  Check if project already $exists
         *      1.1.  Store project in the database
         */

        $this->payload  =   collect( $request->all() )->toArray();
        if( !empty(array_get( $this->payload, 'name' ) ) ){

            $id             =   $this->generate_id($this::leaf);
            $exists         =   app('App\Http\Controllers\Project')->show($id)->getData(1);
            #   1
            if(!array_has($exists, 'name')){

                if(!array_has( $this->payload, 'developer.contact.email' )){
                    array_set( $this->payload, 'developer', $this->get_details('developer') );
                }
                array_set( $this->payload, 'ref_no', str_replace( 'zj-', '', $id ) );
                return Parent::store();

            }
            else{
                return response()->json(['message' => 'Project already exists'], 400);
            }
        }
        else{
            return response()->json(['message' => 'Please enter name of the project'], 400);
        }

    }

    public function update(Request $request, ...$vars){
        if(array_has( $this->payload, 'developer._id' )){
            $request->request->add(['developer' => $this->get_details('developer')]);
        }
        return Parent::update($request,end($vars));
    }

    protected function construct_query_for_similar( $project ){

        return [
            'settings'  =>  [
                'status'    =>  array_get( $project, 'settings.status', 1 )
            ],
            'size'      =>  5
        ];

    }

}
