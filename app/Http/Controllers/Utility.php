<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class Utility extends Controller
{
    const datastore     =   'els_u';

    use \App\Traits\Package;
    use \App\Traits\JsonValidator;

    public function __construct( ){
        $this->S3                   =   new S3;
        $this->bucket_production    =   'zp-' .env( 'COUNTRY' ) . '-configs';
        $this->bucket_staging       =   'zp-earth';
        $this->bucket_name          =   ( env('ELS_ENVIRONMENT') === 'production' ) ? 'zp-' .env( 'COUNTRY' ) . '-configs' : 'zp-earth';
    }

    public function all_brokerages(){

        $brokerages     =   [];
        $aggrs          =   $this->set_config()->get_all_brokerages_agg()[ 'aggregations' ];
        foreach( $aggrs[ 'agency_id' ][ 'buckets' ] as $key => $each_id ){
            if( !empty( $each_id[ 'key' ] ) ){
                $brokerages['brokerages'][$key]['name']    =   array_get($each_id,'name.hits.hits.0._source.contact.name', $each_id[ 'key' ] );
                $brokerages['brokerages'][$key]['value']   =   $each_id[ 'key' ];
                foreach( $each_id['agency_email'][ 'buckets' ] as $each_email ){
                    if( !empty( $each_email[ 'key' ] ) ){
                        $brokerages['brokerages'][$key]['email']    =   $each_email[ 'key' ];
                    }
                }
            }
        }
        #Create file locally
        \Storage::disk('data')->put('brokerages.php', '<?php return ' . var_export( $brokerages, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/brokerages.php';
        $this->S3->put_s3_object( $this->bucket_name, 'brokerages', $file );
        #Delete file locally
        \Storage::disk('data')->delete('brokerages.php');
        return response()->json( array( 'message' => 'brokerages file generated' ), 200 );

    }

    public function all_developers(){

        $developers     =   [];
        $aggrs          =   $this->set_config()->get_all_developers_agg()[ 'aggregations' ];

        foreach( $aggrs['developer_id'][ 'buckets' ] as $key => $each_id ){
            if( !empty( $each_id[ 'key' ] ) ){
                $developers['developers'][$key]['name']    =   array_get($each_id,'name.hits.hits.0._source.contact.name', $each_id[ 'key' ] );
                $developers['developers'][$key]['value']   =   $each_id[ 'key' ];
                foreach( $each_id['developer_email'][ 'buckets' ] as $each_email ){
                    if( !empty( $each_email[ 'key' ] ) ){
                        $developers['developers'][$key]['email']    =   $each_email[ 'key' ];
                    }
                }
            }
        }
        #Create file locally
        \Storage::disk('data')->put('developers.php', '<?php return ' . var_export( $developers, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/developers.php';
        $this->S3->put_s3_object( $this->bucket_name, 'developers', $file );
        #Delete file locally
        \Storage::disk('data')->delete('developers.php');
        return response()->json( array( 'message' => 'developers file generated' ), 200 );

    }

    public function all_service_types(){

        $services       =   [];
        $aggrs          =   $this->set_config()->get_all_service_types_agg()[ 'aggregations' ];
        foreach( $aggrs[ 'service_type_name' ][ 'buckets' ] as $key => $each_name ){
            if( !empty( $each_name[ 'key' ] ) ){
                foreach( $each_name['service_type_id'][ 'buckets' ] as $each_id ){
                    if( !empty( $each_id[ 'key' ] ) ){
                        $services['service_type'][$key]['name']    =   title_case($each_name[ 'key' ]);
                        $services['service_type'][$key]['value']   =   $each_id[ 'key' ];
                    }
                }
            }
        }
        #Create file locally
        \Storage::disk('data')->put('service-types.php', '<?php return ' . var_export( $services, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/service-types.php';
        $this->S3->put_s3_object( $this->bucket_name, 'service-types', $file );
        #Delete file locally
        \Storage::disk('data')->delete('service-types.php');
        return response()->json( array( 'message' => 'service types file generated' ), 200 );

    }

    public function location_list(){

        $location      =   [];
        $countries     =   [];
        $cities        =   [];
        $aggrs          =   $this->set_config()->get_locations_agg()[ 'aggregations' ];
        $subcommunities =  load('location-subcommunities');
        foreach( $aggrs[ 'city' ][ 'buckets' ] as $each_city ){
            if( !empty( $each_city[ 'key' ] ) ){
                $areas            =   [];
                foreach( $each_city[ 'areas' ][ 'buckets' ] as $each_area ){
                    if( !empty( $each_area[ 'key' ] ) ){
                        $building_name      =   [];
                        $keySub  =  array_get($each_area,'area.hits.hits.0._source.area', $each_area[ 'key' ] );
                        foreach( $each_area[ 'buildings' ][ 'buckets' ] as $each_building ){
                            if( !empty( $each_building[ 'key' ] ) )
                                $building_name[]    =   array_get($each_building,'building.hits.hits.0._source.building', $each_building[ 'key' ] );
                        }
                        $areas[ $keySub ][ 'buildings' ]  =   $building_name;
                    }
                }
                $city_name = array_get($each_city,'city.hits.hits.0._source.city', $each_city[ 'key' ] );
                $cities[ $city_name ][ 'areas' ]  =   $areas;
            }
        }

        $cities = array_merge_recursive_distinct($cities,$subcommunities);

        $cities_keys = array_keys($cities);
        foreach($cities_keys as $city_key){
            if( array_search($city_key, load( 'cities' )) === false ) {
                array_forget($cities, $city_key);
            }
        }

        $countries[ 'United Arab Emirates (UAE)' ][ 'cities' ]  =   $cities;
        $location[ 'location'][ 'countries' ]  =   $countries;
        #Create file locally
        \Storage::disk('data')->put('locations.php', '<?php return ' . var_export( $location, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/locations.php';
        $this->S3->put_s3_object( $this->bucket_name, 'locations', $file );
        #Delete file locally
        \Storage::disk('data')->delete('locations.php');
        return response()->json( array( 'message' => 'Location file generated' ), 200 );

    }

    public function location_coordinates_list(){

        $location      =   [];
        $countries     =   [];
        $cities        =   [];
        $aggrs          =   $this->set_config()->get_location_coordinates_agg()[ 'aggregations' ];
        foreach( $aggrs[ 'city' ][ 'buckets' ] as $each_city ){
            if( !empty( $each_city[ 'key' ] ) ){
                $city_name = array_get($each_city,'city.hits.hits.0._source.city', $each_city[ 'key' ] );
                $areas            =   [];
                foreach( $each_city[ 'areas' ][ 'buckets' ] as $each_area ){
                    if( !empty( $each_area[ 'key' ] ) ){
                        $area  =  array_get($each_area,'area.hits.hits.0._source.area', $each_area[ 'key' ] );
                        foreach( $each_area[ 'lat' ][ 'buckets' ] as $each_lat ){
                            if( !empty( $each_lat[ 'key' ] ) ){
                                $latitude    =   $each_lat[ 'key' ];
                                foreach( $each_lat[ 'lng' ][ 'buckets' ] as $each_lng ){
                                    if( !empty( $each_lng[ 'key' ] ) ){
                                        $longitude    =   $each_lng[ 'key' ];
                                    }
                                }
                                $coordinates[ $area.' - '.$city_name ][ 'longitude' ]  =   $longitude;
                            }
                        }
                        $coordinates[ $area.' - '.$city_name ][ 'latitude' ]  =   $latitude;
                    }
                }
                $location[ 'coordinates' ]  =   $coordinates;
            }
        }
        // dd($location);
        $i = 0;
        foreach($location[ 'coordinates' ] as $loc => $coords){
            if(!empty(array_get($coords,'latitude')) && !empty(array_get($coords,'longitude'))){
                $locations[$i]['name']      =   $loc;
                if(array_get($coords,'latitude') < array_get($coords,'longitude'))
                    $locations[$i]['value']   =   array_get($coords,'latitude').','.array_get($coords,'longitude');
                else
                    $locations[$i]['value']   =   array_get($coords,'longitude').','.array_get($coords,'latitude');
                $i++;
            }
        }
        #Create file locally
        \Storage::disk('data')->put('location-coordinates.php', '<?php return ' . var_export( $locations, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/location-coordinates.php';
        $this->S3->put_s3_object( $this->bucket_name, 'location-coordinates', $file );
        #Delete file locally
        \Storage::disk('data')->delete('location-coordinates.php');
        return response()->json( array( 'message' => 'Location coordinates file generated' ), 200 );

    }

    public function property_type_list(){

        $types    =   [];
        $res_comm =   [];

        $aggrs    =   $this->set_config()->get_property_types_agg()[ 'aggregations' ];

        foreach( $aggrs[ 'residential_commercial' ][ 'buckets' ] as $each_res_comm ){
            if( !empty( $each_res_comm[ 'key' ] ) ){
                $rent_buy    =   [];
                foreach( $each_res_comm[ 'rent_buy' ][ 'buckets' ] as $each_rent_buy ){
                    if( !empty( $each_rent_buy[ 'key' ] ) ){
                        $property_type      =   [];
                        $key_rent_buy       =   title_case($each_rent_buy[ 'key' ]);
                        foreach( $each_rent_buy[ 'type' ][ 'buckets' ] as $each_type ){
                            if( !empty( $each_type[ 'key' ] ) )
                                $property_type[]    =   title_case($each_type[ 'key' ]);
                        }
                        $rent_buy[ $key_rent_buy ][ 'types' ]  =   $property_type;
                    }
                }
                $res_comm_key = title_case($each_res_comm[ 'key' ]);
                $res_comm[ $res_comm_key ][ 'rent_buy' ]  =   $rent_buy;
            }
        }
        $types[ 'residential_commercial' ]  =   $res_comm;
        #Create file locally
        \Storage::disk('data')->put('types.php', '<?php return ' . var_export( $types, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/types.php';
        $this->S3->put_s3_object( $this->bucket_name, 'types', $file );
        #Delete file locally
        \Storage::disk('data')->delete('types.php');
        return response()->json( array( 'message' => 'Property types file generated' ), 200 );

    }

    public function brokerage_brokers(){

        $brokerage  = [];
        $aggrs      =   $this->set_config()->get_brokerage_brokers_agg()[ 'aggregations' ];
        foreach( $aggrs[ 'agency_id' ][ 'buckets' ] as $agency_key => $each_agency_id ){
            if( !empty( $each_agency_id[ 'key' ] ) ){
                $brokerage['brokerage'][$agency_key]['name']   =   array_get($each_agency_id,'agency_name.hits.hits.0._source.agency.contact.name', $each_agency_id[ 'key' ] );
                $brokerage['brokerage'][$agency_key]['id']   =   $each_agency_id[ 'key' ];
                foreach( $each_agency_id['agent_name'][ 'buckets' ] as $agent_key => $each_agent_name ){
                    if( !empty( $each_agent_name[ 'key' ] ) ){
                        foreach( $each_agent_name['agent_id'][ 'buckets' ] as $each_agent_id ){
                            if( !empty( $each_agent_id[ 'key' ] ) ){
                                $brokerage['brokerage'][$agency_key]['brokers'][$agent_key]['name']   =   title_case($each_agent_name[ 'key' ]);
                                $brokerage['brokerage'][$agency_key]['brokers'][$agent_key]['id']     =   $each_agent_id[ 'key' ];
                            }
                        }
                    }
                }
            }
        }
        #Create file locally
        \Storage::disk('data')->put('brokerage-brokers.php', '<?php return ' . var_export( $brokerage, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/brokerage-brokers.php';
        $this->S3->put_s3_object( $this->bucket_name, 'brokerage-brokers', $file );
        #Delete file locally
        \Storage::disk('data')->delete('brokerage-brokers.php');
        return response()->json( array( 'message' => 'brokerage brokers file generated' ), 200 );
    }

    public function broker_areas(){

        $agency_areas  =   [];
        $total_areas   =   [];
        $aggrs  =   $this->set_config()->get_broker_areas_agg()[ 'aggregations' ];
        foreach( $aggrs[ 'agency' ][ 'buckets' ] as $each_agency ){
            if( !empty( $each_agency[ 'key' ] ) ){
                $areas  =   [];
                foreach( $each_agency[ 'areas' ][ 'hits' ]['hits'] as $each_areas ){
                    if( !empty( $each_areas[ '_source' ] ) && is_array(array_get($each_areas[ '_source' ],'contact.areas'))){
                        $areas = array_merge_recursive($areas,array_get($each_areas[ '_source' ],'contact.areas'));
                        $total_areas = array_merge_recursive($total_areas,$areas);
                    }
                }
            }
            $agency = array_get($each_agency, 'areas.hits.hits.0._source.agency.contact.name',$each_agency[ 'key' ]);
            $agency_areas['brokerage'][$agency] = array_unique($areas);
        }
        $agency_areas['total_areas']  = array_unique($total_areas);
        #Create file locally
        \Storage::disk('data')->put('brokerage-areas.php', '<?php return ' . var_export( $agency_areas, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/brokerage-areas.php';
        $this->S3->put_s3_object( $this->bucket_name, 'brokerage-areas', $file );
        #Delete file locally
        \Storage::disk('data')->delete('brokerage-areas.php');
        return response()->json( array( 'message' => 'brokerage brokers file generated' ), 200 );

    }

    public function project_developers(){

        $developers     =   [];
        $aggrs          =   $this->set_config()->get_project_developer_agg()[ 'aggregations' ];
        foreach( $aggrs[ 'developer_id' ][ 'buckets' ] as $developer_key => $each_developer_id ){
            if( !empty( $each_developer_id[ 'key' ] ) ){
                $developers['project']['developers'][$developer_key]['name']    =   array_get($each_developer_id,'developer_name.hits.hits.0._source.developer.contact.name', $each_developer_id[ 'key' ] );
                $developers['project']['developers'][$developer_key]['value']   =   $each_developer_id[ 'key' ];
            }
        }
        #Create file locally
        \Storage::disk('data')->put('project-developers.php', '<?php return ' . var_export( $developers, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/project-developers.php';
        $this->S3->put_s3_object( $this->bucket_name, 'project-developers', $file );
        #Delete file locally
        \Storage::disk('data')->delete('project-developers.php');
        return response()->json( array( 'message' => 'project developers file generated' ), 200 );

    }

    public function project_global_developers(){

        $developers     =   [];
        $aggrs          =   $this->set_config()->get_project_global_developer_agg()[ 'aggregations' ];

        foreach( $aggrs[ 'country' ][ 'buckets' ] as $key => $each_country ){
            if( !empty( $each_country[ 'key' ] ) ){
                foreach( $each_country[ 'developer_name' ][ 'buckets' ] as $developer_key => $each_developer_name ){
                    if( !empty( $each_developer_name[ 'key' ] ) ){
                        foreach( $each_developer_name['developer_id'][ 'buckets' ] as $each_developer_id ){
                            if( !empty( $each_developer_id[ 'key' ] ) ){
                                $developers['project'][$each_country['key']]['developers'][$developer_key]['name']    =   array_get($each_developer_name,'developer_name.hits.hits.0._source.developer.contact.name', $each_developer_name[ 'key' ] );
                                $developers['project'][$each_country['key']]['developers'][$developer_key]['value']   =   $each_developer_id[ 'key' ];
                            }
                        }
                    }
                }
            }
        }
        #Create file locally
        \Storage::disk('data')->put('project-global-developers.php', '<?php return ' . var_export( $developers, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/project-global-developers.php';
        $this->S3->put_s3_object( $this->bucket_name, 'project-global-developers', $file );
        #Delete file locally
        \Storage::disk('data')->delete('project-global-developers.php');
        return response()->json( array( 'message' => 'Project global developers file generated' ), 200 );

    }

    public function services_service_types(){

        $service        =   [];
        $aggrs          =   $this->set_config()->get_services_service_types_agg()[ 'aggregations' ];
        foreach( $aggrs[ 'service_type_name' ][ 'buckets' ] as $each_key => $each_name ){
            if( !empty( $each_name[ 'key' ] ) ){
                foreach( $each_name['service_type_id'][ 'buckets' ] as $each_id ){
                    if( !empty( $each_id[ 'key' ] ) ){
                        $service['service']['service_type'][$each_key]['name']    =   title_case($each_name[ 'key' ]);
                        $service['service']['service_type'][$each_key]['value']   =   $each_id[ 'key' ];
                    }
                }
            }
        }
        #Create file locally
        \Storage::disk('data')->put('services-service-types.php', '<?php return ' . var_export( $service, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/services-service-types.php';
        $this->S3->put_s3_object( $this->bucket_name, 'services-service-types', $file );
        #Delete file locally
        \Storage::disk('data')->delete('services-service-types.php');
        return response()->json( array( 'message' => 'service types in services file generated' ), 200 );

    }

    public function agency_email_preferences(){

        $agency_email_preferences   =   [];
        $result                     =   $this->set_config()->get_all_brokerages();

        if( array_get( $result, 'hits.total', 0 ) > 0 ){
            array_set($agency_email_preferences,'total',array_get($result,'hits.total',0));
            $hits       =   array_get($result,'hits.hits',[]);
            for($i=0;$i<count($hits);$i++){
                list($stub[$i],$email[$i]) = explode('@',array_get($hits[$i],'_source.contact.email',''));
                $email[$i] = str_replace('.','_',$email[$i]);
                array_set($agency_email_preferences,$email[$i],array_get($hits[$i],'_source.settings.lead',[]));
                array_set($agency_email_preferences,$email[$i].'.receive_emails',true);
            }
        }
        #Create file locally
        \Storage::disk('data')->put('agency_email_preferences.php', '<?php return ' . var_export( $agency_email_preferences, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/agency_email_preferences.php';
        $this->S3->put_s3_object( $this->bucket_name, 'agency_email_preferences', $file );
        #Delete file locally
        \Storage::disk('data')->delete('agency_email_preferences.php');
        return response()->json( array( 'message' => 'agency email preferences file generated' ), 200 );

    }

    public function get_clients(){

        $brokerages     =   [];
        $aggrs          =   $this->set_config()->get_all_brokerages_agg()[ 'aggregations' ];
        foreach( $aggrs[ 'agency_id' ][ 'buckets' ] as $key => $each_id ){
            if( !empty( $each_id[ 'key' ] ) ){
                $brokerages['brokerages'][$key]['name']    =   array_get($each_id,'name.hits.hits.0._source.contact.name', $each_id[ 'key' ] );
                $brokerages['brokerages'][$key]['value']   =   $each_id[ 'key' ];
                foreach( $each_id['agency_email'][ 'buckets' ] as $each_email ){
                    if( !empty( $each_email[ 'key' ] ) ){
                        $brokerages['brokerages'][$key]['email']        =   $each_email[ 'key' ];
                        $brokerages['brokerages'][$key]['type']         =   'agency';
                        $brokerages['brokerages'][$key]['stub']         =   explode('@',$each_email[ 'key' ])[1];
                        $brokerages['brokerages'][$key]['logo']         =   cdn_asset('agency/logo/'.explode('@',$each_email[ 'key' ])[1]);
                    }
                }
            }
        }
        $developers     =   [];
        $aggrs          =   $this->set_config()->get_all_developers_agg()[ 'aggregations' ];
        foreach( $aggrs[ 'developer_id' ][ 'buckets' ] as $key => $each_id ){
            if( !empty( $each_id[ 'key' ] ) ){
                $developers['developers'][$key]['name']    =   array_get($each_id,'name.hits.hits.0._source.contact.name', $each_id[ 'key' ] );
                $developers['developers'][$key]['value']   =   $each_id[ 'key' ];
                foreach( $each_id['developer_email'][ 'buckets' ] as $each_email ){
                    if( !empty( $each_email[ 'key' ] ) ){
                        $developers['developers'][$key]['email']        =   $each_email[ 'key' ];
                        $developers['developers'][$key]['type']         =   'developer';
                        $developers['developers'][$key]['stub']         =   explode('@',$each_email[ 'key' ])[1];
                        $developers['developers'][$key]['logo']         =   cdn_asset('agency/logo/'.explode('@',$each_email[ 'key' ])[1]);
                    }
                }
            }
        }

        $clients  =   array_merge(array_get($brokerages, 'brokerages'),array_get($developers,'developers'));

        $count = array_count_values(
            array_column($clients, 'value')
        );

        foreach($clients as $entities){
            $result[ $entities['value'] ][ 'datasource' ]        =   ($count[$entities['value']] > 1) ? 'both' : ($entities['type'] == 'agency' ? 'property' : 'project' );
            $result[ $entities['value'] ][ $entities['type'] ]   =   $entities;
        }
        #Create file locally
        \Storage::disk('data')->put('clients.php', '<?php return ' . var_export( [ 'clients' => $result ], true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/clients.php';
        $this->S3->put_s3_object( $this->bucket_name, 'clients', $file );
        #Delete file locally
        \Storage::disk('data')->delete('clients.php');
        return response()->json( array( 'message' => 'client config file generated' ), 200 );

    }

    public function projects_by_developers(){

        $projects   =   [];
        $aggrs      =   $this->set_config()->get_projects_by_developer_agg()[ 'aggregations' ];
        foreach( $aggrs[ 'developer_id' ][ 'buckets' ] as $developer_key => $each_developer_id ){
            if(!empty($each_developer_id['key'])){
                $projects['developer'][$developer_key]['name']    =   array_get($each_developer_id,'developer_name.hits.hits.0._source.developer.contact.name',$each_developer_id['key']);
                $projects['developer'][$developer_key]['value']   =   $each_developer_id['key'];
                foreach( $each_developer_id['project_name'][ 'buckets' ] as $project_key => $each_project_name ){
                    if( !empty( $each_project_name[ 'key' ] ) ){
                        foreach( $each_project_name['project_id'][ 'buckets' ] as $each_project_id ){
                            if( !empty( $each_project_id[ 'key' ] ) ){
                                $projects['developer'][$developer_key]['projects'][$project_key]['name']   =   array_get($each_project_name,'project_name.hits.hits.0._source.name',$each_project_name[ 'key' ]);
                                $projects['developer'][$developer_key]['projects'][$project_key]['id']     =   $each_project_id[ 'key' ];
                            }
                        }
                    }
                }
            }
        }
        #Create file locally
        \Storage::disk('data')->put('projects-by-developers.php', '<?php return ' . var_export( $projects, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/projects-by-developers.php';
        $this->S3->put_s3_object( $this->bucket_name, 'projects-by-developers', $file );
        #Delete file locally
        \Storage::disk('data')->delete('projects-by-developers.php');
        return response()->json( array( 'message' => 'Projects by developers file generated' ), 200 );
    }

    public function export_leads($model = 'property'){

        /**
         *  1.  Fetch leads
         *  2.  Export and store in s3
         */

        $payload                =   [];
        $payload[ 'action' ]    =   ['email'];
        $payload[ 'size' ]      =   1;

        $request = new \Illuminate\Http\Request();
        $request->replace($payload);

        $controller     =   $model == 'property' ? new Analytics\Property($request) : new Analytics\Project($request);
        $return         =   $controller->index();
        $return         =   $return->getData(1);

        $leads          =   ( !empty( array_get($return, 'data', false) ) ) ? array_get( $return, 'data' ) : [];

        if( count( $leads ) > 0 ){

            $filename = storage_path().'/data/'.$model.'_lead_data.csv';
            if (!file_exists(dirname($filename))) {
                mkdir(dirname($filename), 0777, true);
            }

            if( file_exists( $filename ) )
                unlink( $filename );

            $output  = fopen( $filename, 'w+' );
            fprintf($output, chr(0xEF).chr(0xBB).chr(0xBF));

            $columns = array( 'name', 'phone', 'date' );
            fputcsv( $output, $columns );

            foreach( $leads as $lead ){

                if( array_get( $lead, 'user.contact.email' ) != 'anonymous@domain.com'
                    && array_get( $lead, 'user.contact.email' ) != 'Anonymous@domain.com'
                    && array_get( $lead, 'user.contact.email' ) != 'test@test.com'
                    && array_get( $lead, 'user.contact.email' ) != 'test@domain.com'
                    && array_get( $lead, 'user.contact.email' ) != 'sarah@wewantzoom.com'
                    && array_get( $lead, 'user.contact.email' ) != 'gary@wewantzoom.com'
                    && array_get( $lead, 'user.contact.email' ) != 'ravindra0319@gmail.com'
                    && array_get( $lead, 'user.contact.email' ) != 'rizmap@gmail.com'
                    && array_get( $lead, 'user.contact.email' ) != 'lisy@yahoo.com'
                    && array_get( $lead, 'user.contact.email' ) != 'info@obg.ae'
                    && array_get( $lead, 'user.contact.email' ) != 'doe'
                    && !empty(array_get( $lead, 'user.contact.phone' ))){
                    $name       =   array_get( $lead, 'user.contact.name' );
                    $phone      =   array_get( $lead, 'user.contact.phone' );
                    $date       =   array_get( $lead, 'timestamp.created' );

                    fputcsv( $output, array( $name, $phone, $date ) );
                }
            }

            $this->S3->put_s3_object( 'zp-lead-data', $model.'_lead_data', $filename );

            return response()->json( array( 'message' => basename($filename).' file generated' ), 200 );

        }

    }

    public function brokerage_call_tracking(){

        $tracking     =   [];
        $aggrs        =   $this->set_config()->get_brokerage_call_tracking_agg()[ 'aggregations' ];
        foreach( $aggrs[ 'agency_name' ][ 'buckets' ] as $key => $each_name ){
            if( !empty( $each_name[ 'key' ] ) ){
                foreach( $each_name['agency_tracking'][ 'buckets' ] as $each_traking ){
                    if( !empty( $each_traking[ 'key' ] ) ){
                        $tracking[$key]['name']    =   array_get($each_name,'name.hits.hits.0._source.contact.name', $each_name[ 'key' ] );
                        $tracking[$key]['value']   =   starts_with($each_traking[ 'key' ], '+') ? $each_traking[ 'key' ] : '+'.$each_traking[ 'key' ];
                    }
                }
            }
        }
        array_set($final_tracking,'brokerages',array_values($tracking));
        #Create file locally
        \Storage::disk('data')->put('brokerages-tracking.php', '<?php return ' . var_export( $final_tracking, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/brokerages-tracking.php';
        $this->S3->put_s3_object( $this->bucket_name, 'brokerages-tracking', $file );
        #Delete file locally
        \Storage::disk('data')->delete('brokerages-tracking.php');
        return response()->json( array( 'message' => 'brokerages call tracking file generated' ), 200 );

    }

    public function project_completion_status(){

        $project_completion_status  =   [];
        $aggrs                      =   $this->set_config()->get_project_completion_status_agg()[ 'aggregations' ];

        foreach( $aggrs[ 'completion_status' ][ 'buckets' ] as $key => $each_name ){
            if( !empty( $each_name[ 'key' ] ) ){
                $project_completion_status['project']['completion_status'][$key]['name']    =   title_case(str_replace('_', ' ', array_get($each_name,'name.hits.hits.0._source.status', $each_name[ 'key' ] )));
                $project_completion_status['project']['completion_status'][$key]['value']   =   $each_name[ 'key' ];
            }
        }
        #Create file locally
        \Storage::disk('data')->put('project-completion-status.php', '<?php return ' . var_export( $project_completion_status, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/project-completion-status.php';
        $this->S3->put_s3_object( $this->bucket_name, 'project-completion-status', $file );
        #Delete file locally
        \Storage::disk('data')->delete('project-completion-status.php');
        return response()->json( array( 'message' => 'Project completion status file generated' ), 200 );

    }

    public function project_global_completion_status(){

        $project_completion_status  =   [];
        $aggrs                      =   $this->set_config()->get_project_global_completion_status_agg()[ 'aggregations' ];

        foreach( $aggrs[ 'country' ][ 'buckets' ] as $key => $each_country ){
            if( !empty( $each_country[ 'key' ] ) ){
                foreach( $each_country[ 'completion_status' ][ 'buckets' ] as $key => $each_name ){
                    if( !empty( $each_name[ 'key' ] ) ){
                        $project_completion_status['project'][$each_country['key']]['completion_status'][$key]['name']    =   title_case(str_replace('_', ' ', array_get($each_name,'name.hits.hits.0._source.status', $each_name[ 'key' ] )));
                        $project_completion_status['project'][$each_country['key']]['completion_status'][$key]['value']   =   $each_name[ 'key' ];
                    }
                }
            }
        }
        #Create file locally
        \Storage::disk('data')->put('project-global-completion-status.php', '<?php return ' . var_export( $project_completion_status, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/project-global-completion-status.php';
        $this->S3->put_s3_object( $this->bucket_name, 'project-global-completion-status', $file );
        #Delete file locally
        \Storage::disk('data')->delete('project-global-completion-status.php');
        return response()->json( array( 'message' => 'Project global completion status file generated' ), 200 );

    }

    public function property_completion_status(){

        $property_completion_status =   [];
        $aggrs                      =   $this->set_config()->get_property_completion_status_agg()[ 'aggregations' ];

        foreach( $aggrs[ 'completion_status' ][ 'buckets' ] as $key => $each_name ){
            if( !empty( $each_name[ 'key' ] ) ){
                $property_completion_status['property']['completion_status'][$key]['name']    =   title_case(str_replace('_', ' ', array_get($each_name,'name.hits.hits.0._source.completion_status', $each_name[ 'key' ] )));
                $property_completion_status['property']['completion_status'][$key]['value']   =   $each_name[ 'key' ];
            }
        }
        #Create file locally
        \Storage::disk('data')->put('property-completion-status.php', '<?php return ' . var_export( $property_completion_status, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/property-completion-status.php';
        $this->S3->put_s3_object( $this->bucket_name, 'property-completion-status', $file );
        #Delete file locally
        \Storage::disk('data')->delete('property-completion-status.php');
        return response()->json( array( 'message' => 'Property completion status file generated' ), 200 );

    }

    public function project_location_list(){

        $location      =   [];
        $countries     =   [];
        $cities        =   [];
        $aggrs         =   $this->set_config()->get_project_locations_agg()[ 'aggregations' ];

        foreach( $aggrs[ 'city' ][ 'buckets' ] as $each_city ){
            if( !empty( $each_city[ 'key' ] ) ){
                $areas            =   [];
                foreach( $each_city[ 'areas' ][ 'buckets' ] as $each_area ){
                    if( !empty( $each_area[ 'key' ] ) ){
                        $areas[]    =   array_get($each_area,'area.hits.hits.0._source.area', $each_area[ 'key' ] );
                    }
                }
                $city_name = array_get($each_city,'city.hits.hits.0._source.city', $each_city[ 'key' ] );
                $cities[ $city_name ][ 'areas' ]  =   $areas;
            }
        }

        // return $cities;

        $countries[ 'United Arab Emirates (UAE)' ][ 'cities' ]  =   $cities;
        $location[ 'location'][ 'countries' ]  =   $countries;
        #Create file locally
        \Storage::disk('data')->put('project-locations.php', '<?php return ' . var_export( $location, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/project-locations.php';
        $this->S3->put_s3_object( $this->bucket_name, 'project-locations', $file );
        #Delete file locally
        \Storage::disk('data')->delete('project-locations.php');
        return response()->json( array( 'message' => 'Project location file generated' ), 200 );

    }

    public function project_global_location_list(){

        $location      =   [];
        $countries     =   [];
        $aggrs         =   $this->set_config()->get_project_global_locations_agg()[ 'aggregations' ];

        foreach( $aggrs[ 'country' ][ 'buckets' ] as $each_country ){
            if( !empty( $each_country[ 'key' ] ) ){
                $cities        =   [];
                foreach( $each_country[ 'city' ][ 'buckets' ] as $each_city ){
                    if( !empty( $each_city[ 'key' ] ) ){
                        $areas      =   [];
                        foreach( $each_city[ 'areas' ][ 'buckets' ] as $each_area ){
                            if( !empty( $each_area[ 'key' ] ) ){
                                $areas[]    =   array_get($each_area,'area.hits.hits.0._source.area', $each_area[ 'key' ] );
                            }
                        }
                        $city_name  =   array_get($each_city,'city.hits.hits.0._source.city', $each_city[ 'key' ] );
                        $cities[ $city_name ][ 'areas' ]  =   $areas;
                    }
                }
                $country_name   =   array_get($each_country,'country.hits.hits.0._source.country', $each_city[ 'key' ] );
                $countries[ $country_name ][ 'cities' ]  =   $cities;
            }
        }

        // return $countries;

        $location[ 'location'][ 'countries' ]  =   $countries;
        #Create file locally
        \Storage::disk('data')->put('project-global-locations.php', '<?php return ' . var_export( $location, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/project-global-locations.php';
        $this->S3->put_s3_object( $this->bucket_name, 'project-global-locations', $file );
        #Delete file locally
        \Storage::disk('data')->delete('project-global-locations.php');
        return response()->json( array( 'message' => 'Project global locations file generated' ), 200 );

    }

    public function keywords(){
        $data                   =   load( 'data' );
        $static_keywords        =   array_get($data,'keywords',[]);
        $dynamic_keywords       =   [];
        $keywords               =   [];

        $payload                =   [];
        $payload['active']      =   1;
        $request                =   new \Illuminate\Http\Request();
        $request->replace($payload);
        $links_controller       =   new Seo\Links($request);
        $links_result           =   $links_controller->index('popular_search');
        if($links_result->getStatusCode() == 200){
            $links         =   $links_result->getData(1);
            $links_data    =   array_get($links,'data',[]);
            if(!empty($links_data)){
                $links_keywords = collect($links_data)->pluck('links_keywords')->collapse()->toArray();
                $links_status = collect($links_data)->pluck('links_status')->collapse()->toArray();
                foreach($links_keywords as $key => $value){
                    if(!empty(array_get($links_status,$key))){
                        $dynamic_keywords[] =  $value;
                    }
                }
            }
        }

        $keywords   =   array_values(array_unique(array_merge($static_keywords,$dynamic_keywords)));

        $config['keywords']   =   [];
        if(!empty($keywords)){
            foreach( $keywords as $keyword ){
                if( $keyword !== '' ){
                    $config['keywords'][]   =   array( 'name' => title_case(str_replace('-',' ',$keyword)), 'value' => $keyword );
                }
            }
        }

        \Storage::disk('data')->put('keywords.php', '<?php return ' . var_export( $config, true ) . ';');
        #Upload to S3
        $file         =  storage_path().'/data/keywords.php';
        $this->S3->put_s3_object( $this->bucket_production, 'keywords', $file );
        $this->S3->put_s3_object( $this->bucket_staging, 'keywords', $file );
        #Delete file locally
        \Storage::disk('data')->delete('keywords.php');
        #API call to download keywords config in staging and production
        $links = array_get($data,'links',[]);
        $requests = [
            'staging'       =>  array_get($links,'zp_consumer.staging').'utility/download/keywords',
            'production'    =>  array_get($links,'zp_consumer.production').'utility/download/keywords'
        ];
        $execute    =   $this->api_call($requests);
        return response()->json( array( 'message' => 'Keywords file generated' ), 200 );

    }

    public function api_call(array $requests){
        try{
            $client    =    new \GuzzleHttp\Client;
            foreach($requests as $key => $request){
                $promises[$key] =   $client->getAsync( $request );
            }
            $execute = \GuzzleHttp\Promise\settle($promises)->wait();
            return $execute;
        }
        catch( \GuzzleHttp\Exception\RequestException $e ){
            if( $e->hasResponse() ) {
                return response()->json( json_decode( $e->getResponse()->getBody()->getContents(), 1 ), $e->getResponse()->getStatusCode() );
            }
            else{
                return response()->json( array( 'message' => 'No response' ), 400 );
            }
        }
    }
}
