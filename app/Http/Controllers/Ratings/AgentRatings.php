<?php

namespace App\Http\Controllers\Ratings;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Collection;
use BaoPham\DynamoDb\Facades\DynamoDb;

class AgentRatings extends BaseController
{
    const leaf          =   'agent_ratings';
    const datastore     =   'ddb';
    const metadata      =   [];

    protected $agent_ratings_model, $request;
    use \App\Traits\JsonValidator;
    use \App\Traits\Common;

    public function __construct( Request $request = null ){
        $this->agent_ratings_model  =   new \DataPKG\Models\AgentRatingsDdb;
        $this->payload              =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
    }

    public function index(...$var){

        $agent_id    =   array_get( $var, 0 );
        $filters    =   $this->payload;

        // Get all ratings of a agent
        $ratings_data  =   DynamoDb::table('agent-ratings')
                            ->setKeyConditionExpression('#agent_id = :agent_id')
                            ->setExpressionAttributeNames(['#agent_id' => 'agent_id'])
                            ->setExpressionAttributeValues([':agent_id' => DynamoDb::marshalValue($agent_id)])
                            ->prepare()
                            ->query()
                            ->toArray();

        $ratings    =   [];
        $results  =   [];
        if(!empty($ratings_data['Items'])){
            $get_ratings  =  [];
            foreach($ratings_data['Items'] as $item){
                $get_ratings[]    =   array_undot(DynamoDb::unmarshalItem($item));
            }
            if(!empty($filters)){
                list($count,$ratings)     =   $this->search_json($filters,$get_ratings);
            }
        }

        return response()->json([
                'total' =>  count($ratings),
                'data'  =>  $ratings
            ], 200);
    }

    public function store(Request $request, ...$vars){
        /**
         * 1.   Get data
         * 2.   Store
         */
        $agent_id    =   array_get($vars,'0',false);
        $request    =   is_null( $request->all() ) ? $this->payload  : collect($request->all())->toArray();
        $ratings      =   [];

        if($agent_id){
            $ratings  =   $request;
            $user     =   app('\App\Http\Controllers\User')->show(array_get($ratings,'user_id'))->getData(1);

            array_set($ratings,'user_name',array_get($user,'contact.name'));
            array_set($ratings,'user_picture',"");
            array_set($ratings,'active',0);
            array_set($ratings,'_id','zar-'.md5(serialize(random_number())));
            array_set($ratings,'agent_id',$agent_id);
            
            foreach($ratings as $key=>$value){
                $this->agent_ratings_model->{$key}  = $value;
            }

            if($this->agent_ratings_model->save()){
                return response()->json(array_undot($ratings),200);
            }
            else{
                return response()->json(['Could not store rating'], 503);
            }
        }
        else{
            return response()->json(['Agent ID is required.'], 400);
        }
    }

    public function update(Request $request, ...$vars){

        $exists =   $this->show(...$vars);

        if($exists->status() == 200){

            $request    =   is_null($request) ? $this->payload : $request->all();
            $agent_id    =   array_get($vars,'0',false);
            $id         =   array_get($vars,'1',false);


            array_set($request, 'updated_at', date('Y-m-d\TH:i:s+00:00'));

            $update_items       =   $request;
            $update_expression  =   '';
            $attribute_values   =   [];
            $primary_key        =   [];

            foreach($update_items as $key => $value){
                $update_expression  .=   $key.'=:'.$key.',';
            }

            if(!empty($update_expression)){
                $update_expression  =   rtrim($update_expression, ',');
            }
            else{
                return response()->json(['message' => 'Insufficient Input'],400);
            }
            
            foreach($update_items as $key => $value){
                $attribute_values[':'.$key]  =   DynamoDb::marshalValue($value);
            }

            $execute    =   DynamoDb::table('agent-ratings')
                                    ->setKey(DynamoDb::marshalItem([ 'agent_id' => $agent_id, '_id' => $id ]))
                                    ->setUpdateExpression('SET '.$update_expression)
                                    ->setExpressionAttributeValues($attribute_values)
                                    ->prepare()
                                    ->updateItem();

            if($execute){
                return response()->json([ 'message' => 'Agent ratings updated successfully'], 200);
            }
            else{
                return response()->json([ 'message' => 'Agent ratings cannot be updated'], 400);
            }
        }
        else{
            return $exists;
        }
    }

    public function show(...$vars){
        $agent_id    =   array_get($vars,'0',false);
        $id          =   array_get($vars,'1',false);
        try{
            if($agent_id){
                $ratings     =   DynamoDb::table('agent-ratings')
                                    ->setKey(DynamoDb::marshalItem([ 'agent_id' => $agent_id, '_id' => $id ])        )
                                    ->prepare()
                                    ->getItem()
                                    ->toArray();

                $ratings_data =   array_undot(DynamoDb::unmarshalItem($ratings['Item']));
                $ratings      =   !empty($ratings_data) ? $ratings_data : [ 'message' => 'No Results' ];
                return response()->json($ratings_data, 200);
            }
            else{
                return response()->json(['Agent ID is required.'], 400);
            }
        }
        catch (\Exception $e) {
            return response()->json([ 'message' => 'Agent rating not found'], 500);
        }
    }
}
