<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class Property extends Controller
{

    const leaf          =   'property';
    const datastore     =   'els';
    const metadata      =   [];

    use \App\Traits\Package;

    public function index(){
        if(array_get($this->payload,'budget',false)){
            return $this->budget();
        }
        else{
            // TODO Featured Properties should honour featured.from < now < featured.to
            // TODO Organic Properties should honour featured.to < now
            return Parent::index();
        }
    }

    public function budget(){

        return $this    ->set_config($this::leaf,$this->payload)
                        ->index('budget');

    }

    public function store(Request $request = null){

        /**
         *  1.  Check if property already $exists
         *  2.  Store property in the database
         */

        $this->payload  =  collect( $request->all() )->toArray();
        $ref_no = preg_replace(['#/+#','/\\\\/'],'-',array_get( $this->payload, 'ref_no' ));
        array_set($this->payload,'ref_no',$ref_no);
        $id = 'zp-'.array_get( $this->payload, 'ref_no' );
        $exists = app('App\Http\Controllers\Property')->show($id)->getData(1);

        if( !array_has( $exists, 'ref_no' ) ){
            if(!array_has( $this->payload, 'agent.contact.email' )){
                array_set( $this->payload, 'agent', $this->get_details('agent') );
            }
            if(!array_has( $this->payload, 'settings.is_manual' )){
              array_set( $this->payload, 'settings.is_manual', 1 );
            }
            return Parent::store();
        }
        else{
            return response()->json(['message' => 'Property already exists'], 400);
        }
    }

    public function construct_payload($param = []){
        $payload    =   [];
        $input      =   !empty($param) ? $param : $this->payload;
        $clauses    =   array_intersect_key($input,array_flip(array('sort', 'size','page')));
        $query      =   array_except($input,['location','sort','size','page']);
        if( array_has($input,'location') ){
            $location   =   [];
            $locations  =   array_get($input,'location',[]);
            foreach($locations as $key => $value){
                $locs   =   explode('-',$value);
                if(count($locs) == 1){
                    $location[$key][ 'city' ][]        =   trim($locs[0]);
                }
                if(count($locs) == 2){
                    $location[$key][ 'area' ][]        =   trim($locs[0]);
                    $location[$key][ 'city' ][]        =   trim($locs[1]);
                }
                if(count($locs) == 3){
                    $location[$key][ 'building' ][]    =   trim($locs[0]);
                    $location[$key][ 'area' ][]        =   trim($locs[1]);
                    $location[$key][ 'city' ][]        =   trim($locs[2]);
                }
            }
            $query  =   array_except($query,[ 'city', 'area', 'building' ]);
            foreach($location as $loc){
                $payload[ 'query' ][] =   array_merge($query,$loc);
            }
        }
        else{
            if(!empty($query)){
                $payload[ 'query' ][]   =   $query;
            }
            else{
                $payload[ 'query' ]     =   [];
            }
        }

        if(!empty($param)){
            return array_merge($payload,$clauses);
        }
        else{
            $this->payload = array_merge($payload,$clauses);
        }
    }

    protected function construct_query_for_similar( $property ){

        return [
            'rent_buy'                  =>  [ array_get( $property, 'rent_buy', '' ) ],
            'city'                      =>  [ array_get( $property, 'city', '' ) ],
            'area'                      =>  [ array_get( $property, 'area', '' ) ],
            'type'                      =>  [ array_get( $property, 'type', '' ) ],
            'residential_commercial'    =>  [ array_get( $property, 'residential_commercial', '' ) ],
            'settings'                  =>  [
                'status'                    =>  array_get( $property, 'settings.status', 1 )
            ],
            'size'                      =>  3
        ];

    }

}
