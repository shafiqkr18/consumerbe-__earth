<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use BaoPham\DynamoDb\Facades\DynamoDb;

class Alert extends User
{
    const leaf          =   'alert';
    const datastore     =   'ddb';
    const metadata      =   [];

    use \App\Traits\Common;

    public function __construct(Request $request){
        Parent::__construct();
        $this->alerts_model =   new \DataPKG\Models\AlertsDdb;
        $this->payload      =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : '';
    }

    /**
    * Display a listing of the resource.
    *
    * @param  string  $user_id, $model
    * @return \Illuminate\Http\JsonResponse
    */
    public function index(Request $request, ...$vars){

        $user_id        =   array_get($vars,'0');
        $model          =   array_get($vars,'1');
        $email          =   $this->decrypt_user($user_id);
        $frequencies    =   array_get( $this->payload, 'frequency', [] );

        array_set( $this->payload, 'type', $model ); //dd($this->payload);

        $alert_rsp  =   DynamoDb::table('alerts-staging')
                            ->setFilterExpression('#consumer_email = :consumer_email')
                            ->setExpressionAttributeNames(['#consumer_email' => 'consumer.email'])
                            ->setExpressionAttributeValues([':consumer_email' => DynamoDb::marshalValue($email)])
                            ->prepare()
                            ->scan()
                            ->toArray();

        $alerts             = [];
        $filtered_alerts    = [];

        if(!empty($alert_rsp['Items'])){
            foreach($alert_rsp['Items'] as $item){
                $alerts[] = array_undot(DynamoDb::unmarshalItem($item));
            }
            if(!empty($this->payload)){
                list($count,$filtered_alerts)     =   $this->search_json($this->payload,$alerts);
            }
        }

        return response()->json([
                'total' =>  count($filtered_alerts),
                'data'  =>  $filtered_alerts
            ], 200);

    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  string  $user_id, $model, $model_id
    * @return \Illuminate\Http\JsonResponse
    */
    public function store(Request $request, ...$vars)
    {
        $user       =   array_get($vars,'0',false);
        $model      =   array_get($vars,'1',false);
        $model_id   =   array_get($vars,'2',false);
        $alert_data =   [];

        if($model){
            if($model_id){
                //TODO individual model alerts
                return response()->json(['message' => 'Service currently not available'], 503);
            }
            else{
                $alert_data =   $request->all();
                $alert_model    =   new \DataPKG\Models\AlertsDdb;

                array_set($alert_data,'alert._id',$this->create_alert_id($alert_data));
                array_set($alert_data,'type',$model);
                array_set($alert_data,'subtype','search');

                array_set( $alert_data, 'data.suburb', array_get($alert_data, 'data.area') );
                array_forget( $alert_data, 'data.area' );
            }
        }

        $alert_data  =   array_dot($alert_data);

        foreach($alert_data as $key=>$value){
            $alert_model->{$key}  = $value;
        }
        // dd($alert_data,$alert_model);

        if($alert_model->save()){
            $alert_data = array_undot($alert_data);
            foreach($alert_data as $key=>$value){
                if($key == 'alert'){
                    array_set( $alert_data, '_id', $value['_id'] );
                }
                array_forget( $alert_data, 'alert' );
            }
            return response()->json($alert_data,200);
        }
        else{
            return response()->json(['Could not save alert'], 503);
        }
    }

    /**
    * Display the specified resource.
    *
    * @param  string  $user_id, $model, $model_id
    * @return \Illuminate\Http\JsonResponse
    */
    public function show(...$vars)
    {
        //Code...
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  string  $user_id, $model, $model_id
    * @return \Illuminate\Http\JsonResponse
    */
    public function update(Request $request, ...$vars)
    {
        //Code...
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  string  $user_id, $model, $model_id
    * @return \Illuminate\Http\JsonResponse
    */
    public function destroy(...$vars)
    {
        $user_id    =   array_get($vars,'0');
        $model      =   array_get($vars,'1');
        $model_id   =   array_get($vars,'2');
        $email      =   $this->decrypt_user($user_id);

        $frequency  =   array_get( $this->payload, 'frequency', '' );

        DynamoDb::table('alerts-staging')
                ->setKey(DynamoDb::marshalItem([ 'frequency' => $frequency, 'alert._id' => $model_id ]))
                ->prepare()
                ->deleteItem();

        return response()->json( [ 'message' => 'Alert deleted successfully' ], 200 );
    }

    private function create_alert_id( $data ){

        $alertId       =   array_get( $data, 'frequency' )."_".array_get( $data, 'consumer.email' );
        $alertId       =   !empty(array_get( $data, 'data.rent_buy', '' )) ? array_get( $data, 'data.rent_buy' )."_".$alertId : $alertId;
        $alertId       =   !empty(array_get( $data, 'data.price.min', '' )) ? array_get( $data, 'data.price.min' )."_".$alertId : $alertId;
        $alertId       =   !empty(array_get( $data, 'data.price.max', '' )) ? array_get( $data, 'data.price.max' )."_".$alertId : $alertId;
        $alertId       =   !empty(array_get( $data, 'data.area', '' )) ? array_get( $data, 'data.area' )."_".$alertId : $alertId;

        return strtolower( str_replace( " ", "_", $alertId ) );

    }
}
