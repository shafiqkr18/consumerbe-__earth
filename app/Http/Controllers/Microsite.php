<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use BaoPham\DynamoDb\Facades\DynamoDb;

class Microsite extends BaseController
{
    const leaf          =   'microsite';
    const datastore     =   'ddb';
    const metadata      =   [];

    protected $model, $payload, $table, $fields;
    use \App\Traits\JsonValidator;
    use \App\Traits\Common;

    public function __construct( Request $request = null ){
        $this->model    =   new \DataPKG\Models\MicrositeDdb;
        $this->table    =   $this->model->getTable();
        $this->fields   =   $this->model->getFillable();
        $this->payload  =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
    }

    public function index(){
        /**
         *  1.  Construct request
         *  2.  Get all microsites
         *  3.  Filter records if payload exists
         */

        #   1
        $filters    =   $this->payload;

        #   2
        $data       =   $this->model->get()->toArray();

        $seo        =   [];

        #   3
        list($count,$result)     =   $this->search_json($filters,$data);

        return response()->json([
                'total' =>  $count,
                'data'  =>  array_map('array_undot',$result)
            ], 200);
    }

    public function store(Request $request){
        /**
         *  1. Construct request
         *  2. Check if same microsite exists
         *  3. If exists, send validation error
         *  4. Store records in the database
         */

        #   1
        $request    =   is_null( $request->all() ) ? $this->payload  : collect($request->all())->toArray();
        if(!array_has($request,'active')){
            array_set($request,'active',1);
        }

        #   2
        $id         =   array_get($request,'developer._id');
        $exists     =   $this->show($id);

        #   3
        if($exists->getStatusCode() == 200){
            return response()->json([ 'message' => 'Microsite already exists'], 400);
        }
        elseif($exists->getStatusCode() == 400){

            #   4
            foreach($request as $key => $value){
                if( in_array( $key, $this->fields ) ){
                    $this->model->{$key}  = $value;
                }
            }

            if($this->model->save()){
                return response()->json($request,200);
            }
            else{
                return response()->json(['Could not store microsite'], 503);
            }
        }
        else{
            return $exists;
        }
    }

    public function show($id){
        try{
            $result =   array_get($this->model->where('developer._id',$id)->get()->toArray(),0,false);

            if(!empty($result)){
                return response()->json(array_undot($result),200);
            }
            else{
                return response()->json([ 'message' => $id.' microsite does not exist' ],400);
            }
        }
        catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage()], 500);
        }
    }

    public function update(Request $request,$id){
        /**
         *  1.  Check if record to be updated exists
         *  2.  Prepare request
         *  3.  Prepare update query
         *      3.1.    Prepare update expression
         *      3.2.    Prepare expression attribute names to avoid reserved word error
         *      3.3.    Prepare expression attribute values
         *  4.  Update record
         */

        #   1
        $exists     =   $this->show($id);

        if($exists->getStatusCode() == 200){

            #   2
            $request    =   is_null($request) ? $this->payload : $request->all();
            array_set($request, 'updated_at', date('Y-m-d\TH:i:s+00:00'));

            $update_items       =   [];
            $update_expression  =   '';
            $attribute_names    =   [];
            $attribute_values   =   [];

            #   3
            if(count($request)>0){
                foreach($request as $k => $v){
                    if( in_array( $k, $this->fields ) ){
                        $update_items[$k]  = $v;
                    }
                }
            }

            #   3.1
            foreach($update_items as $key => $value){
                $update_expression  .=   '#'.str_replace('.','_',$key).'=:'.str_replace('.','_',$key).',';
            }
            if(!empty($update_expression)){
                $update_expression  =   rtrim($update_expression, ',');
            }
            else{
                return response()->json(['message' => 'Insufficient Input'],400);
            }

            #   3.2
            foreach($update_items as $key => $value){
                $attribute_names['#'.str_replace('.','_',$key)]  =  $key;
            }

            #   3.3
            foreach($update_items as $key => $value){
                $attribute_values[':'.str_replace('.','_',$key)]  =   DynamoDb::marshalValue($value);
            }

            #   4
            $execute    =   DynamoDb::table($this->table)
                                    ->setKey(DynamoDb::marshalItem(['developer._id' => $id]))
                                    ->setUpdateExpression('SET '.$update_expression)
                                    ->setExpressionAttributeValues($attribute_values)
                                    ->setExpressionAttributeNames($attribute_names)
                                    ->prepare()
                                    ->updateItem();

            if($execute){
                return response()->json([ 'message' => $id.' microsite updated successfully'], 200);
            }
            else{
                return response()->json([ 'message' => $id.' microsite cannot be updated'], 400);
            }
        }
        else{
            return $exists;
        }
    }

    public function destroy(...$vars){
        $id                       =   end($vars);
        $request                  =   new \Illuminate\Http\Request();
        $payload                  =   [];
        $payload[ 'active' ]      =   0;
        $request->replace($payload);
        return $this->update($request,$id);
    }

    public function get_details(){
        try{
            #   1
            $filter  =   !empty($this->payload) ? $this->payload : [];
            $slug    =   array_get($filter,'developer.slug');
            $results =   $this->model->where('developer.slug',$slug)
                                                    ->get()
                                                    ->toArray();

            list($count,$result)     =   $this->search_json($filter,$results);

            if($count > 0){
                return response()->json(array_get($result,0,[]),200);
            }
            else{
                return response()->json([ 'message' => 'Microsite not found'], 400);
            }
        }
        catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage()], 500);
        }
    }

}
