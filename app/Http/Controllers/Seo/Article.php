<?php

namespace App\Http\Controllers\Seo;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Collection;
use BaoPham\DynamoDb\Facades\DynamoDb;

class Article extends BaseController
{
    const leaf          =   'article';
    const datastore     =   'ddb';
    const metadata      =   [];

    protected $seo_article_model, $payload, $table, $fields;
    use \App\Traits\JsonValidator;
    use \App\Traits\Common;

    public function __construct( Request $request = null ){
        $this->seo_article_model    =   new \DataPKG\Models\SeoArticlesDdb;
        $this->table                =   $this->seo_article_model->getTable();
        $this->fields               =   $this->seo_article_model->getFillable();
        $this->payload              =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
    }

    public function index(...$vars){
        /**
         *  1.  Construct request
         *  2.  Get all SEO articles
         *  3.  Filter records if payload exists
         */

        #   1
        $model      =   array_get($vars,0);
        array_set($this->payload,'model',$model);
        $filters    =   array_dot($this->payload);

        #   2
        $data   =   DynamoDb::table($this->table)
                            ->setKeyConditionExpression('#model = :model')
                            ->setExpressionAttributeNames(['#model' => 'model'])
                            ->setExpressionAttributeValues([':model' => DynamoDb::marshalValue($model)])
                            ->prepare()
                            ->query()
                            ->toArray();
        $articles   =   [];

        foreach($data['Items'] as $item){
            $articles[]    =   array_undot(DynamoDb::unmarshalItem($item));
        }

        #   3
        list($count,$articles)     =   $this->search_json($filters,$articles);

        return response()->json([
                'total' =>  count($articles),
                'data'  =>  $articles
            ], 200);
    }

    public function store(Request $request, ...$vars){
        /**
         *  1. Construct request
         *  2. Check if same article exists
         *  3. If exists, send validation error
         *  4. Store record in the database
         */

        #   1
        $request    =   is_null( $request->all() ) ? $this->payload  : collect($request->all())->toArray();
        $model      =   array_get($vars,0);

        array_set($request,'model',$model);

        if(!array_has($request,'active')){
            array_set($request,'active',1);
        }
        if(array_has($request,'property.area')){
            $area = trim(preg_replace("/\([^)]+\)/","",array_get($request,'property.area')));
            array_set($request,'property.area',$area);
        }

        if(array_has($request,$model)){
            $request[$model]    =   array_get($request,$model);
        }

        foreach($this->fields as $field){
            if(str_contains($field,$model.'.')){
                $article_parameter_fields[] = explode('.',$field,2)[1];
            }
        }

        foreach($article_parameter_fields as $field){
            array_set($request,$model.'.'.$field,array_get($request,$model.'.'.$field,''));
        }

        #   2
        $payload['page_name']   =   array_get($request,'page_name');
        $payload[$model]        =   array_get($request,$model);
        $exists                 =   $this->exists($payload,$model);

        #   3
        if($exists->getStatusCode() == 200){
            return response()->json([ 'message' => 'Article already exists'], 400);
        }
        elseif($exists->getStatusCode() == 404){
            array_set($request,'_id','sa-'.md5(serialize(random_number())));

            //Create SEO articles id
            $seo_article_id =   $this->get_seo_article_id(array_get($request,$model),array_get($request,'page_name'));
            array_set($request,'seo_article_id',$seo_article_id);

            $data               =   [];
            $dotted_request     =   array_dot($request);

            foreach($dotted_request as $k => $v){
                if( preg_match( "/.\d{1,2}/", $k, $match ) == 1 && strpos($k, ".") !== false){
                    $data[ preg_replace( "/.\d{1,2}/", "", $k ) ][]   =  $v;
                    array_forget( $dotted_request, $k );
                }
                else{
                    $data[ $k ]   =  $v;
                }
            }

            #   5
            foreach($data as $key => $value){
                if( in_array( $key, $this->fields ) ){
                    $this->seo_article_model->{$key}  = $value;
                }
            }

            if($this->seo_article_model->save()){
                return response()->json(array_undot($data),200);
            }
            else{
                return response()->json(['Could not store article'], 503);
            }
        }
        else{
            return $exists;
        }
    }

    public function update(Request $request, ...$vars){
        /**
         *  1.  Check if record to be updated exists
         *  2.  Check if template already exists
         *  3.  Update record
         */

        #   1
        $show     =   $this->show(...$vars);

        $model      =   array_get($vars,0);
        $id         =   array_get($vars,1);

        if($show->getStatusCode() == 200){

            $request    =   is_null($request) ? $this->payload : $request->all();
            $show       =   $show->getData(1);

            array_forget($request,'page_name');

            if(array_has($request,'property.area')){
                $area = trim(preg_replace("/\([^)]+\)/","",array_get($request,'property.area')));
                array_set($request,'property.area',$area);
            }

            if(array_has($request,$model)){
                $request[$model]    =   array_get($request,$model);
            }

            #   2
            if(array_has($request,$model) AND count(array_get($request,$model)) > 0){

                foreach($this->fields as $field){
                    if(str_contains($field,$model.'.')){
                        $article_parameter_fields[] = explode('.',$field,2)[1];
                    }
                }

                foreach($article_parameter_fields as $field){
                    if(array_has($request,$model.'.'.$field)){
                        array_set($payload,$model.'.'.$field,array_get($request,$model.'.'.$field,''));
                    }
                    else{
                        array_set($payload,$model.'.'.$field,array_get($show,$model.'.'.$field,''));
                    }
                }

                $payload['page_name']   =   array_get($show,'page_name');
                $exists                 =   $this->exists($payload,$model);
                if($exists->getStatusCode() == 200){
                    $exists     =   $exists->getData(1);
                    if($exists['_id'] != $id){
                        return response()->json([ 'message' => 'Article already exists'], 400);
                    }
                }
                if(array_has($payload,$model) && array_has($show,'page_name')){
                    // Create SEO articles id
                    $seo_article_id =   $this->get_seo_article_id(array_get($payload,$model),array_get($show,'page_name'));
                    array_set($request,'seo_article_id',$seo_article_id);
                }
            }

            array_set($request, 'updated_at', date('Y-m-d\TH:i:s+00:00'));

            $data               =   [];
            $dotted_request     =   array_dot($request);

            foreach($dotted_request as $k => $v){
                if( preg_match( "/.\d{1,2}/", $k, $match ) == 1 && strpos($k, ".") !== false){
                    $data[ preg_replace( "/.\d{1,2}/", "", $k ) ][]   =  $v;
                    array_forget( $dotted_request, $k );
                }
                else{
                    $data[ $k ]   =  $v;
                }
            }

            #   3
            $update_items       =   $data;
            $update_expression  =   '';
            $attribute_names    =   [];
            $attribute_values   =   [];

            //Update Expression
            foreach($update_items as $key => $value){
                $update_expression  .=   '#'.str_replace('.','_',$key).'=:'.str_replace('.','_',$key).',';
            }
            if(!empty($update_expression)){
                $update_expression  =   rtrim($update_expression, ',');
            }
            else{
                return response()->json(['message' => 'Insufficient Input'],400);
            }

            //Expression Attribute Names to avoid reserved word error
            foreach($update_items as $key => $value){
                $attribute_names['#'.str_replace('.','_',$key)]  =  $key;
            }

            //Attribute Values
            foreach($update_items as $key => $value){
                $attribute_values[':'.str_replace('.','_',$key)]  =   DynamoDb::marshalValue($value);
            }

            $execute    =   DynamoDb::table($this->table)
                                    ->setKey(DynamoDb::marshalItem([ 'model' => $model, '_id' => $id ]))
                                    ->setUpdateExpression('SET '.$update_expression)
                                    ->setExpressionAttributeValues($attribute_values)
                                    ->setExpressionAttributeNames($attribute_names)
                                    ->prepare()
                                    ->updateItem();

            if($execute){
                return response()->json([ 'message' => 'SEO article updated successfully'], 200);
            }
            else{
                return response()->json([ 'message' => 'SEO article cannot be updated'], 400);
            }
        }
        else{
            return $show;
        }
    }

    public function show(...$vars){
        try{
            $result =   array_get($this->seo_article_model->where('_id',end($vars))->get()->toArray(),0,false);

            if(!empty($result)){
                $response   =   $this->format_seo_article_response(array_undot($result));
                return response()->json($response,200);
            }
            else{
                return response()->json([ 'message' => 'SEO article does not exist' ],404);
            }
        }
        catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage()], 500);
        }
    }

    public function exists($payload,$model){
        try{
            //Get all parameter fields
            foreach($this->fields as $field){
                if(str_contains($field,$model.'.')){
                    $article_parameter_fields[] = explode('.',$field,2)[1];
                }
            }

            foreach($article_parameter_fields as $field){
                array_set($payload,$model.'.'.$field,array_get($payload,$model.'.'.$field,''));
            }
            //Compose article id
            $seo_article_id =   $this->get_seo_article_id(array_get($payload,$model),array_get($payload,'page_name'));

            array_set($payload,'seo_article_id',$seo_article_id);

            array_forget($payload,$model);
            array_forget($payload,'page_name');

            $result =   $this->seo_article_model->where('seo_article_id',$seo_article_id)
                                                ->get()
                                                ->toArray();
            list($count,$article)     =   $this->search_json($payload,$result);

            if($count > 0){
                return response()->json(array_get($article,0,[]),200);
            }
            else{
                return response()->json([ 'message' => 'SEO article not found'], 404);
            }
        }
        catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage()], 500);
        }
    }

    public function get_details(...$vars){
        $payload            =   $this->payload;
        $model              =   array_get($vars,0);

        if(!array_has($payload,'page_name')){
            return response()->json([ 'message' => 'Page name is required' ],400);
        }
        $exists             =   $this->exists($payload,$model);

        if($exists->getStatusCode() == 200){
            $response   =   $exists->getData(1);
            return response()->json(array_undot($response),200);
        }
        else{
            return $exists;
        }
    }

    private function get_seo_article_id($payload,$page){
        array_set($payload, 'page_name', $page);
        $payload = array_dot($payload);
        $payload = replace_null($payload);
        ksort($payload);
        return  md5(serialize($payload));
    }
}
