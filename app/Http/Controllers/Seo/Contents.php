<?php

namespace App\Http\Controllers\Seo;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Collection;
use BaoPham\DynamoDb\Facades\DynamoDb;

class Contents extends BaseController
{
    const leaf          =   'contents';
    const datastore     =   'ddb';
    const metadata      =   [];

    protected $seo_model, $request;
    use \App\Traits\JsonValidator;
    use \App\Traits\Common;

    public function __construct( Request $request = null ){
        $this->seo_model    =   new \DataPKG\Models\SeoDdb;
        $this->payload      =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
    }

    public function index(){
        /**
         *  1.  Construct request
         *  2.  Get all SEO content
         *  3.  Filter records if payload exists
         */

        #   1
        $filters    =   $this->payload;

        if(array_has($filters, 'seo_parameters') && array_has($filters, 'page_name')){
            $exists         =   $this->exists();
            $data[]         =   ($exists->getStatusCode() == 200) ? $exists->getData(1) : null;
            $seo            =   array_filter($data);
        }
        else{
            #   2
            $data   =   $this->seo_model->get()->toArray();
            $seo    =   [];
            #   3
            list($count,$seo)     =   $this->search_json($filters,$data);
        }

        return response()->json([
                'total' =>  count($seo),
                'data'  =>  $seo
            ], 200);
    }

    public function store(Request $request){
        /**
         *  1. Construct request
         *  2. Check if same template exists
         *  3. If exists, send validation error
         *  4. Store records in the database
         */

        #   1
        $request    =   is_null( $request->all() ) ? $this->payload  : collect($request->all())->toArray();
        if(!array_has($request,'active')){
            array_set($request,'active',1);
        }

        if(array_has($request,'project._id')){
            $request['seo_parameters']      =   [];
            $request['seo_parameters'][]    =   array_get($request,'project._id');

            $controller     =   new \App\Http\Controllers\Project();
            $project        =   $controller->show(array_get($request,'project._id'));
            if($project->getStatusCode() == 200){
                $project_details = $project->getData(1);
                if(!empty(array_get($project_details,'_id'))){
                    array_set($request, 'project.name',array_get($project_details,'name'));
                    array_set($request, 'project.developer._id',array_get($project_details,'developer._id'));
                    array_set($request, 'project.developer.name',array_get($project_details,'developer.contact.name'));
                }
                else{
                    return response()->json(['message' => 'Invalid project Id']);
                }
            }
            else{
                return response()->json(['message' => 'Project does not exist']);
            }
        }

        #   2
        $this->payload  =   [];
        $this->payload['page_name']         =   array_get($request,'page_name');
        $this->payload['seo_parameters']    =   array_has($request,'seo_parameters') ? array_get($request,'seo_parameters') : [];
        $exists         =   $this->exists();

        #   3
        if($exists->getStatusCode() == 200){
            return response()->json([ 'message' => 'Template already exists'], 400);
        }
        elseif($exists->getStatusCode() == 400){
            array_set($request,'_id','seo-'.md5(serialize(random_number())));
            $seo_parameters_id  =   $this->get_seo_parameters_id(array_get($request,'seo_parameters'),array_get($request,'page_name'));
            array_set($request,'seo_parameters_id',$seo_parameters_id);

            $data               =   [];
            $dotted_request     =   array_dot($request);
            foreach($dotted_request as $k => $v){
                $ext  =   substr($k, strrpos($k, '.') + 1);
                if(is_numeric($ext)){
                    $data[ str_replace( '.'.$ext, '', $k ) ][]   =  $v;
                    array_forget( $dotted_request, $k );
                }
                else{
                    $data[ $k ]   =  $v;
                }
            }

            #   4
            foreach($data as $key=>$value){
                if( in_array( $key, $this->seo_model->getFillable() ) ){
                    $this->seo_model->{$key}  = $value;
                }
            }

            if($this->seo_model->save()){
                return response()->json($data,200);
            }
            else{
                return response()->json(['Could not store seo contents'], 503);
            }
        }
        else{
            return $exists;
        }
    }

    public function show($id){
        try{
            $result =   array_get($this->seo_model->where('_id',$id)->get()->toArray(),0,false);

            if(!empty($result)){
                return response()->json(array_undot($result),200);
            }
            else{
                return response()->json([ 'message' => 'SEO contents do not exist' ],400);
            }
        }
        catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage()], 500);
        }
    }

    public function update(Request $request,$id){
        /**
         *  1.  Check if record to be updated exists
         *  2.  Check if template already exists
         *  3.  Prepare request
         *  4.  Allow only those fields to be updated that exist in the model
         *  5.  Prepare update query
         *      5.1.    Prepare update expression
         *      5.2.    Prepare expression attribute names to avoid reserved word error
         *      5.3.    Prepare expression attribute values
         *  6.  Update record
         */

        #   1
        $exists     =   $this->show($id);

        if($exists->getStatusCode() == 200){
            $request    =   is_null($request) ? $this->payload : $request->all();
            if(array_has($request, 'project')){
                array_forget($request, 'project');
            }
            $exists     =   $exists->getData(1);

            #   2
            if(array_has($request,'seo_parameters') || array_has($request,'page_name')){
                $this->payload  =   $new_payload    =   [];
                $this->payload['page_name']         =   array_has($request,'page_name') ? array_get($request,'page_name') : array_get($exists,'page_name');
                $this->payload['seo_parameters']    =   array_has($request,'seo_parameters') ? array_get($request,'seo_parameters') : array_get($exists,'seo_parameters');
                $new_payload                        =   $this->payload;
                $details        =   $this->exists();
                if($details->getStatusCode() == 200){
                    $details     =   $details->getData(1);
                    if($details['_id'] != $id){
                        return response()->json([ 'message' => 'Template already exists'], 400);
                    }
                }
            }

            #   3
            if(array_has($request,'seo_parameters') || array_has($request,'page_name')){
                $seo_parameters_id  =   $this->get_seo_parameters_id(array_get($new_payload,'seo_parameters'),array_get($new_payload,'page_name'));
                array_set($request,'seo_parameters_id',$seo_parameters_id);
            }
            array_set($request, 'updated_at', date('Y-m-d\TH:i:s+00:00'));

            $update_request     =   [];
            $update_items       =   [];
            $update_expression  =   '';
            $attribute_names    =   [];
            $attribute_values   =   [];

            $dotted_request     =   array_dot($request);
            foreach($dotted_request as $k => $v){
                $ext  =   substr($k, strrpos($k, '.') + 1);
                if(is_numeric($ext)){
                    $update_request[ str_replace( '.'.$ext, '', $k ) ][]   =  $v;
                    array_forget( $dotted_request, $k );
                }
                else{
                    $update_request[ $k ]   =  $v;
                }
            }

            #   4
            if(count($update_request)>0){
                foreach($update_request as $k => $v){
                    if( in_array( $k, $this->seo_model->getFillable() ) ){
                        $update_items[$k]  = $v;
                    }
                }
            }

            #   5.1
            foreach($update_items as $key => $value){
                $update_expression  .=   '#'.str_replace('.','_',$key).'=:'.str_replace('.','_',$key).',';
            }
            if(!empty($update_expression)){
                $update_expression  =   rtrim($update_expression, ',');
            }
            else{
                return response()->json(['message' => 'Insufficient Input'],400);
            }

            #   5.2
            foreach($update_items as $key => $value){
                $attribute_names['#'.str_replace('.','_',$key)]  =  $key;
            }

            #   5.3
            foreach($update_items as $key => $value){
                $attribute_values[':'.str_replace('.','_',$key)]  =   DynamoDb::marshalValue($value);
            }

            #   6
            $execute    =   DynamoDb::table('seo-contents')
                                    ->setKey(DynamoDb::marshalItem(['_id' => $id]))
                                    ->setUpdateExpression('SET '.$update_expression)
                                    ->setExpressionAttributeValues($attribute_values)
                                    ->setExpressionAttributeNames($attribute_names)
                                    ->prepare()
                                    ->updateItem();

            if($execute){
                return response()->json([ 'message' => 'SEO contents updated successfully'], 200);
            }
            else{
                return response()->json([ 'message' => 'SEO contents cannot be updated'], 400);
            }
        }
        else{
            return $exists;
        }
    }

    public function exists(){
        /**
         *  1.  Get request
         *  2.  Get seo parameter id with seo_parameters and page_name
         *  3.  Return results
         */
        try{
            #   1
            $filter     =   !empty($this->payload) ? $this->payload : [];

            #   2
            if(array_has($filter, 'seo_parameters') && array_has($filter, 'page_name')){
                $seo_parameters_id  =   $this->get_seo_parameters_id(array_get($filter,'seo_parameters'),array_get($filter,'page_name'));
                array_set($filter,'seo_parameters_id',$seo_parameters_id);

                array_forget($filter,'seo_parameters');
                array_forget($filter,'page_name');
                array_forget($filter,array_get($filter,'model'));
                array_forget($filter,'model');
            }

            #   3
            $result =   $this->seo_model->where('seo_parameters_id',$seo_parameters_id)
                                                    ->get()
                                                    ->toArray();

            list($count,$seo)     =   $this->search_json($filter,$result);

            if($count > 0){
                return response()->json(array_get($seo,0,[]),200);
            }
            else{
                return response()->json([ 'message' => 'SEO contents not found'], 400);
            }
        }
        catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage()], 500);
        }
    }

    public function get_details(){
        /**
         *  1.  Get dynamic SEO contents if exists
         *  2.  Prepate payload for SEO article
         *  3.  Merge SEO contents and SEO articles
         *  4.  Return response
         */

        #   1
        $exists             =   $this->exists();
        $contents           =   ($exists->getStatusCode() == 200) ? $exists->getData(1) : [];

        $payload            =   json_decode(json_encode($this->payload), true);
        $article_data       =   [];
        $article            =   [];
        $links              =   [];
        $request            =   [];

        #   2
        if(array_has($payload, 'model') && array_has($payload, array_get($payload,'model'))){

            $model                  =   array_get($payload, 'model');
            $request[$model]        =   array_get($payload, $model);

            /**
             * Uncomment when article is to be shown for all searches under given criteria
             */

            // $seo_article_model    =   new \DataPKG\Models\SeoArticlesDdb;
            // //Get registered article fields
            // foreach($seo_article_model->getFillable() as $field){
            //     if(str_contains($field,$model.'.')){
            //         $article_parameter_fields[] = explode('.',$field)[1];
            //     }
            // }
            // //Filter request and get only registered article fields
            // foreach($article_parameter_fields as $field){
            //     array_set($request,$model.'.'.$field,array_get($payload,$model.'.'.$field,''));
            // }

            $request['page_name']   =   array_get($payload,'page_name');
            $request['domain']      =   array_get($payload, 'seo_domain');
            $request['active']      =   1;
            $article_controller     =   new Article;
            $article_result         =   $article_controller->exists($request, $model);
            if($article_result->getStatusCode() == 200){
                $article_data           =   $this->format_seo_article_response(array_undot($article_result->getData(1)));
            }
            else{
                #SEO default article behaviour
                // $new_payload                =   [];
                // $new_payload['page_name']   =   array_get($payload,'page_name');
                // $new_payload['domain']      =   array_get($payload, 'seo_domain');
                // $new_payload['active']      =   1;
                // $new_payload[ '_id' ]       =   'sa-770b698e6b78a0c6604b455a96005156';
                //
                // $new_request                =   new \Illuminate\Http\Request();
                // $new_request->replace($new_payload);
                // $default_article            =   new Article($new_request);
                // $default_article            =   $default_article->index($model);
                // if($default_article->getStatusCode() == 200){
                //     $default_article            =   $default_article->getData(1);
                //     if(array_get($default_article,'total',0) > 0){
                //         $article_data               =   array_get($default_article,'data.0',[]);
                //         $article_data               =   $this->format_seo_article_response($article_data);
                //     }
                // }
            }
            if(!empty($article_data)){
                $metadata['seo_title']              =   array_get($article_data,'seo_title','');
                $metadata['seo_title_ar']           =   array_get($article_data,'seo_title_ar','');
                $metadata['seo_description']        =   array_get($article_data,'seo_description','');
                $metadata['seo_description_ar']     =   array_get($article_data,'seo_description_ar','');
                $metadata['seo_h1']                 =   array_get($article_data,'seo_h1','');
                $metadata['seo_h1_ar']              =   array_get($article_data,'seo_h1_ar','');
                $metadata['seo_h2']                 =   array_get($article_data,'seo_h2','');
                $metadata['seo_h2_ar']              =   array_get($article_data,'seo_h2_ar','');
                $metadata['seo_h3']                 =   array_get($article_data,'seo_h3','');
                $metadata['seo_h3_ar']              =   array_get($article_data,'seo_h3_ar','');
                $metadata['seo_image_alt']          =   array_get($article_data,'seo_image_alt','');
                $metadata['seo_image_alt_ar']       =   array_get($article_data,'seo_image_alt_ar','');

                $article['article']['section_1']    =   array_get($article_data,'section_1',[]);
                $article['article']['section_2']    =   array_get($article_data,'section_2',[]);
                $article['article']['section_3']    =   array_get($article_data,'section_3',[]);
                $article['article']['section_4']    =   array_get($article_data,'section_4',[]);
                $article['article']['metadata']     =   $metadata;
            }
            else{
                $article['article']                 =   $article;
            }

            $links_controller     =   new Links;
            $links_result         =   $links_controller->exists($request, $model);
            if($links_result->getStatusCode() == 200){
                $links_data         =   $links_result->getData(1);
                $links['links']     =   array_undot($links_data);
                $links['links']     =   array_only($links['links'],['links','links_status','titles','titles_ar','names','names_ar']);
            }
        }

        #   3
        $contents   =   !empty($contents) ? $contents : [];
        $output     =   array_merge($contents, $article);
        $output     =   array_merge($output, $links);

        #   4
        if(count($output) > 0){
            return response()->json(array_undot($output),200);
        }
        else{
            return response()->json([ 'message' => 'SEO contents not found'], 400);
        }
    }

    private function get_seo_parameters_id($payload,$page){
        array_push($payload,$page);
        sort($payload);
        return  md5(serialize($payload));
    }
}
