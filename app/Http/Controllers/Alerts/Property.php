<?php

namespace App\Http\Controllers\Alerts;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use BaoPham\DynamoDb\Facades\DynamoDb;

class Property extends BaseController
{
    const leaf          =   'alert';
    const datastore     =   'ddb';
    const metadata      =   [];

    protected $alert_model, $request;
    use \App\Traits\JsonValidator;
    use \App\Traits\Common;

    public function __construct( Request $request = null ){
        $this->alert_model  =   new \DataPKG\Models\AlertsDdb;
        $this->payload      =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
    }

    public function index(){

        $alerts     =   $this->alert_model->get()->toArray();
        $filters    =   $this->payload;

        list($count,$alerts)     =   $this->search_json($filters,$alerts);

        return response()->json( [
                'total' =>  count($alerts),
                'data'  =>  $alerts
            ] );
    }
}
