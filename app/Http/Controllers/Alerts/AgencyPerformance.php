<?php

namespace App\Http\Controllers\Alerts;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Collection;
use BaoPham\DynamoDb\Facades\DynamoDb;

class AgencyPerformance extends BaseController
{
    const leaf          =   'agency_performance';
    const datastore     =   'ddb';
    const metadata      =   [];

    protected $alert_model, $request;
    use \App\Traits\JsonValidator;
    use \App\Traits\Common;

    public function __construct( Request $request = null ){
        $this->alert_model  =   new \DataPKG\Models\AgencyPerformanceAlertsDdb;
        $this->payload      =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
    }

    public function index(){
        $alerts     =   $this->alert_model->get()->toArray();

        $filters    =   $this->payload;

        list($count,$alerts)     =   $this->search_json($filters,$alerts);

        return response()->json( [
                'total' =>  count($alerts),
                'data'  =>  $alerts
            ] );
    }

    public function store(Request $request, ...$vars){

        $request    =   is_null($request) ? $this->payload : $request->all();

        $agency_id  =   array_get($request,'agency_id');
        $frequency  =   array_get($request,'alert_frequency');
        $type       =   array_get($request,'alert_type');

        if($type == 'setting'){
            $query      =   $this->alert_model  ->where('agency_id',$agency_id)
                                                ->where('alert_frequency',$frequency)
                                                ->where('alert_type',$type)
                                                ->get()
                                                ->toArray();

            $alert      =   array_get($query,0,false);

            if($alert){
                return response()->json(['message' => 'Alert already exists'], 400);
            }
        }

        // Only used for dDb retrieval
        $this->alert_model->agency_id               =   array_get($request,'agency_id');
        $this->alert_model->agency_name             =   array_get($request,'agency_name');
        $this->alert_model->agency_email            =   array_get($request,'agency_email');
        $this->alert_model->target_lead_count       =   array_get($request,'target_lead_count');
        $this->alert_model->final_lead_count        =   array_get($request,'final_lead_count');
        $this->alert_model->alert_frequency         =   array_get($request,'alert_frequency');
        $this->alert_model->alert_type              =   array_get($request,'alert_type');
        $this->alert_model->alert_status            =   array_get($request,'alert_status');
        $encrypted_id                               =   'zas-'.random_number();
        $this->alert_model->_id                     =   $encrypted_id;

        if($this->alert_model->save()){
            return $this->show($encrypted_id);
        }
    }

    public function update(Request $request, ...$vars){

        $request            =   is_null($request) ? $this->payload : $request->all();

        $update_items       =   $request;
        $update_expression  =   '';
        $attribute_values   =   [];

        foreach($update_items as $key => $value){
            $update_expression  .=   $key.'=:'.$key.',';
        }

        if(!empty($update_expression)){
            $update_expression  =   rtrim($update_expression, ',');
        }
        else{
            return response()->json(['message' => 'Insufficient Input'],400);
        }

        foreach($update_items as $key => $value){
            $attribute_values[':'.$key]  =   DynamoDb::marshalValue($value);
        }

        $execute    =   DynamoDb::table('agency-performance-alerts')
                                ->setKey(DynamoDb::marshalItem(['_id' => end($vars)]))
                                ->setUpdateExpression('SET '.$update_expression)
                                ->setExpressionAttributeValues($attribute_values)
                                ->prepare()
                                ->updateItem();

        if($execute){
            return $this->show(end($vars));
        }

    }

    public function show(...$vars){
        $result =   array_get($this->alert_model->where('_id',end($vars))
                                                ->get()
                                                ->toArray(),
                          0,false);

        if(!empty($result)){
            return response()->json($result,200);
        }
        else{
            return response()->json([ 'message' => 'Alert does not exist' ],400);
        }
    }

    public function destroy(...$vars){

        $id   =   end($vars);

        $request                    =   new \Illuminate\Http\Request();
        $payload                    =   [];
        $payload[ 'alert_status' ]  =   0;

        $request->replace($payload);

        return $this->update($request,$id);

    }

}
