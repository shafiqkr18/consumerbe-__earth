<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Brick\PhoneNumber\PhoneNumber;
use Brick\PhoneNumber\PhoneNumberParseException;

class ThirdPartyLQS extends BaseController
{
    public function third_party_integration($data, $model){
        if(array_get($data, 'settings.approved', false)){
            $_id    =   in_array($model, ['project']) ? array_get($data, 'project.developer._id', false) : (in_array($model, ['property']) ? array_get($data, 'property.agent.agency._id', false) : false);
            switch( $_id ){
                #DAMAC LQS Integration
                case 'zb-damac-17493':
                    $this->damac_lead_integration($data);
                    break;
                #AZIZI LQS Integration
                case 'zb-azizi-developments-0000':
                    $this->azizi_lead_integration($data);
                    break;
                #MERAAS LQS Integration
                case 'zb-meraas-56893':
                    $this->meraas_lead_integration($data);
                    break;
                case 'zb-meraas-phd-0000':
                    $this->meraas_lead_integration($data);
                    break;
                case 'zb-piramal-realty-0000':
                    $this->piramal_lead_integration($data);
                    break;
                default:
                    break;
            }
        }
    }

    private function damac_lead_integration($lead_data){
        #DAMAC create token
        $token_payload = ['email' => env('DAMAC_LQS_USER'), 'password' => env('DAMAC_LQS_PASS')];
        $token_headers = ['headers' => ['content-type' => 'application/x-www-form-urlencoded','cache-control' => "no-cache"]];
        $token_rsp     = $this->api_call( env('DAMAC_LQS_URL').'createrefreshtoken','POST', $token_headers, $token_payload );
        if($token_rsp->getStatusCode() === 200){
            $response = json_decode($token_rsp->getBody()->getContents(),1);
            if(array_get($response,'status', false)){
                #DAMAC Send lead
                $token   =   array_get($response,'token', '');
                $headers = ['headers' => ['authorization' => $token,'content-type' => 'application/x-www-form-urlencoded','cache-control' => "no-cache"]];
                $full_name = explode(' ', array_get($lead_data,'user.contact.name',''));
                array_set($payload, 'FirstName', array_get($full_name,0,''));
                array_set($payload, 'LastName', array_get($full_name,1,array_get($full_name,0,'')));
                #PHONE NUMBER
                $phone = '';
                $phone_country = 'United Arab Emirates: 00971';
                try {
                    $phone_number = PhoneNumber::parse(array_get($lead_data,'user.contact.phone',''));
                    if (!$phone_number->isValidNumber()) {
                        $phone = '';
                        $phone_country = 'United Arab Emirates: 00971';
                    }
                    else{
                        $phone = $phone_number->getNationalNumber();
                        $phone_country = $phone_number->getRegionCode().': 00'.$phone_number->getCountryCode();
                    }
                }
                catch (PhoneNumberParseException $e) {
                    //return
                }
                #ADSET
                $adset = !empty(array_get($lead_data,'target.adset_id','')) ? substr(array_get($lead_data,'target.adset_id',''), 4, -4) : '';
                $payload =[
                    'title'         => 'MR.',
                    'firstName'     => array_get($full_name,0,''),
                    'lastName'      => array_get($full_name,1,array_get($full_name,0,'')),
                    'email'         => array_get($lead_data,'user.contact.email',''),
                    'phoneNumber'   => $phone,
                    'city'          => '',
                    'country'       => is_null(array_get($lead_data,'country_source',null)) ? 'United Arab Emirates' : array_get($lead_data,'country_source'),
                    'countryCode'   => $phone_country,
                    'utmSource'     => 'zoom-property',
                    'utmMedium'     => 'online-listings',
                    'utmCampaign'   => 'zoom-propertylistings',
                    'campaignId'    => 'a121n000003wppZ',
                    'campaignName'  => array_get($lead_data,'target.campaign_name',''),
                    'adGroup'       => $adset
                ];

                $rsp = $this->api_call( env('DAMAC_LQS_URL').'sendlead','POST', $headers, $payload );
                $rsp = json_decode($rsp->getBody()->getContents(),1);
                if(array_get($rsp, 'message') == 'success')
                    return response()->json($rsp, 200);
                else
                    return response()->json($rsp, 400);
            }
        }
    }

    private function azizi_lead_integration($lead_data){
        // dd($lead_data);
        #AZIZI Send lead
        $headers = ['headers' => ['content-type' => 'application/x-www-form-urlencoded','cache-control' => "no-cache"]];
        $full_name = array_get($lead_data,'user.contact.name','');
        #PHONE NUMBER
        $phone = array_get($lead_data,'user.contact.phone','');
        #1. check if number starting with 00
        if( str_contains( $phone, '00' ) ) {
            #1.1 change the 00 to + for validation
            $phone = str_replace('00','+', $phone);
        }
        $phone_country = '+971';
        try {
            $phone_number = PhoneNumber::parse($phone);
            if ($phone_number->isPossibleNumber()) {
                $phone = $phone_number->getNationalNumber();
                $phone_country = '+'.$phone_number->getCountryCode();
            }
        }
        catch (PhoneNumberParseException $e) {
            //return $e;
        }

        $payload =[
            'name'          => $full_name,
            'email'         => array_get($lead_data,'user.contact.email',''),
            'mobile'        => ltrim($phone, 0),
            'country_code'  => $phone_country,
            'source'        => 'Zoom Property',
            'language'      => ( strpos( array_get($lead_data,'target.adset_name',''), 'Arabic' ) !== false ) ? 'Arabic' : 'English'
        ];

        $rsp = $this->api_call( env('AZIZI_LQS_URL'),'POST', $headers, $payload );
        $rsp = json_decode($rsp->getBody()->getContents(),1);
        if(array_get($rsp, 'status') == 'success')
            return response()->json($rsp, 200);
        else
            return response()->json($rsp, 400);
    }

    private function meraas_lead_integration($lead_data){

        #MERAAS Send lead
        $headers = ['headers' => ["Content-Type" => "application/x-www-form-urlencoded", 'cache-control' => "no-cache"]];
        $full_name = explode(' ', array_get($lead_data,'user.contact.name',''));
        #PHONE NUMBER
        $phone = array_get($lead_data,'user.contact.phone','');
        #1. check if number starting with 00
        if( str_contains( $phone, '00' ) ) {
            #1.1 change the 00 to + for validation
            $phone = str_replace('00','+', $phone);
        }
        $phone_country = '+971';
        try {
            $phone_number = PhoneNumber::parse($phone);
            if ($phone_number->isPossibleNumber()) {
                $phone = $phone_number->getNationalNumber();
                $phone_country = '+'.$phone_number->getCountryCode();
            }
        }
        catch (PhoneNumberParseException $e) {
            //return $e;
        }

        $payload =[
            'full-name'         => array_get($full_name,0,''),
            'last-name'         => array_get($full_name,1,array_get($full_name,0,'')),
            'email-address'     => array_get($lead_data,'user.contact.email',''),
            'tel-Bluewaters'    => ltrim($phone, 0),
            'property'          => array(array_get($lead_data,'project.name','')),
            'UTM_CampaignID'    => array_get($lead_data,'project._id','') == 'zj-la-sur-townhouses' ? '201911_surlamer2_launch' : ( array_get($lead_data,'project._id','') == 'zj-la-mer-maisons' ? 'Land_Sales_PHD' : ( array_get($lead_data,'project.developer._id','') == 'zb-meraas-phd-0000' ? 'PDLM-ALL_PHD_SEP19' : '201911_ZOOM_ALWAYSON')),
            'utm_campaign'      => array_get($lead_data,'project._id','') == 'zj-la-sur-townhouses' ? '201911_surlamer2_launch' : ( array_get($lead_data,'project._id','') == 'zj-la-mer-maisons' ? 'Land_Sales_PHD' : ( array_get($lead_data,'project.developer._id','') == 'zb-meraas-phd-0000' ? 'PDLM-ALL_PHD_SEP19' : '201911_ZOOM_ALWAYSON')),
            'utm_source'        => 'Zoom Property',
            'utm_medium'        => 'Display',
            'elqFormName'       => 'register-interest',
            'elqSiteID'         => '1417573558'
        ];

        $rsp = $this->api_call( env('MERAAS_LQS_URL'),'POST', $headers, $payload );

        if($rsp->getStatusCode() === 200)
            return response()->json(array('message'=>'success'), 200);
        else
            return response()->json(array('message'=>'error'), 400);
    }

    private function piramal_lead_integration($lead_data){
        if(array_get($lead_data,'source','google') == 'google')
            return;
        #Piramal Send lead
        $headers = ['headers' => ["Content-Type" => "application/json", 'Accept' => "application/json","x-api-key" => "Hgr1Mf8Y9y1WKJ6ksYgBP6ZF1dEPKSwqa7gR1WAD"]];
        #PHONE NUMBER
        $phone = array_get($lead_data,'user.contact.phone','');
        #1. check if number starting with 00
        if( str_contains( $phone, '00' ) ) {
            #1.1 change the 00 to + for validation
            $phone = str_replace('00','+', $phone);
        }
        $phone_country = 'AE';
        try {
            $phone_number = PhoneNumber::parse($phone);
            if ($phone_number->isPossibleNumber()) {
                $phone = '+'.$phone_number->getCountryCode().$phone_number->getNationalNumber();
                $phone_country = $phone_number->getRegionCode();
            }
        }
        catch (PhoneNumberParseException $e) {
            //return $e;
        }

        $payload =[
            '_id'               => str_replace('zja-','prja-',array_get($lead_data,'_id')),
            'action'            => 'campaign',
            'source'            => [
                'main'              =>  (array_get($lead_data,'source','facebook') == 'facebook' || array_get($lead_data,'source','facebook') == 'instagram') ? 'social' : ((array_get($lead_data,'source','facebook') == 'google_search' || array_get($lead_data,'source','facebook') == 'google_display' || array_get($lead_data,'source','facebook') == 'google_remarketing') ? 'google' : 'campaign'),
                "subsource"         =>  array_get($lead_data,'source','facebook'),
                "country"           =>  $phone_country,
            ],
            "call_back"         => array_get($lead_data,'call_back',false),
            "message"           => array_get($lead_data,'message',''),
            "settings"          => [
                "status"            => array_get($lead_data,'settings.status',1),
                "approved"          => array_get($lead_data,'settings.approved',true),
            ],
            "user"              => array_get($lead_data,'user'),
            "project"           =>  [
                "_id"               => str_replace('zj-','prj-',array_get($lead_data,'project._id'))
            ],
            "target"           =>  [
                "campaign_id"  => array_get($lead_data,'target.campaign_id',""),
                "campaign_name"  => array_get($lead_data,'target.campaign_name',""),
            ]
        ];
        array_set($payload,'user.contact.phone',$phone);

        $rsp = $this->api_call( env('PIRAMAL_LQS_URL').'/lead/project','POST', $headers, $payload, true );

        if($rsp->getStatusCode() === 200)
            return response()->json(array('message'=>'success'), 200);
        else
            return response()->json(array('message'=>'error'), 400);
    }

    private function api_call($api_route,$method, $headers, $payload, $json = false){
        try{
            $client         =   new \GuzzleHttp\Client($headers);

            $payload        =   ( $method == 'GET' ) ? [ 'query' => $payload ] : ( $json == true ? ['json'=> $payload] : [ 'form_params' => $payload ]);
            // dd($full_api_route,$payload,$headers);
            return $client->request( $method, $api_route, $payload );

        }
        catch( \GuzzleHttp\Exception\RequestException $e ){
            if( $e->hasResponse() ) {
                return response()->json( json_decode( $e->getResponse()->getBody()->getContents(), 1 ), $e->getResponse()->getStatusCode() );
            }
            else{
                return response()->json( array( 'message' => 'No response' ), 400 );
            }
        }
    }

}
