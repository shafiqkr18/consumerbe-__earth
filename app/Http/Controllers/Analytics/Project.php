<?php

namespace App\Http\Controllers\Analytics;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class Project extends Controller
{
    const leaf          =   'project_analytics';
    const datastore     =   'els';
    const metadata      =   [];

    public function store( Request $request = null )
    {
        $this->payload  =   collect( $request->all() )->toArray();
        if(!array_has( $this->payload, 'project.ref_no' )){
            array_set($this->payload, 'project', $this->get_details('project'));
        }

        // $return = response()->json( [ 'message' => 'success test'], 200 );
        #Spam layer
        #NOTE Apply only for DAMAC at the moment might get General use
        if(array_get($this->payload,'project.developer._id') == 'zb-damac-17493'){
            #1 keep store payload
            $store_payload = $this->payload;
            #2 search lead exist
            $exist = $this->spam_users_lead_projects();
            #3 if lead exist lead modify qualification
            if($exist){
                array_set($store_payload,'qualification.status','duplicate');
                array_set($store_payload,'qualification.note','Duplicate lead');
                array_set($store_payload,'settings.approved', false);
                array_set($store_payload,'message', array_get($store_payload,'message').'-- DUPLICATE');
            }
            $this->payload = $store_payload;
        }
        $return = Parent::store();

        if( $return->getStatusCode() === 200 ){
            if(array_get($this->payload,'action') !== 'call' && array_get($this->payload,'action') !== 'whatsapp' && array_get($this->payload,'settings.approved')){
                $msg = $return->getData(1);
                $msg = array_get($msg,'message');
                array_set($this->payload,'_id',str_after($msg,'created with _id: '));
                $lqs     =  new \App\Http\Controllers\ThirdPartyLQS;
                $lqs->third_party_integration($this->payload, 'project');
            }
        }

        return $return;
    }

    #NOTE Apply only for DAMAC at the moment might get General use
    #LOGIC If the {{Phone Number}} for {{project ABC}} match any lead for the same project in the previous {{28 days}}, send it to the unapproved bucket with label {{Duplicate}}.
    private function spam_users_lead_projects(){
        $exist = false;
        #2.1 build search payload
        $search_payload = [];
        array_set($search_payload,'user.contact.email',array_get($this->payload,'user.contact.email'));
        array_set($search_payload,'user.contact.phone',array_get($this->payload,'user.contact.phone'));
        array_set($search_payload,'project._id',array_get($this->payload,'project._id'));
        array_set($search_payload,'settings.status',1);
        array_set($search_payload,'timestamp.created.from',date('Y-m-d\TH:i:s\Z', strtotime('-28 days', strtotime(date('Y-m-d\TH:i:s\Z')))));
        array_set($search_payload,'timestamp.created.to',date('Y-m-d\TH:i:s\Z'));
        array_set($search_payload,'size',1);
        $this->payload = $search_payload;
        #2.2 Search lead
        $return = Parent::index();
        if( $return->getStatusCode() === 200 ){
            $data = $return->getData(1);
            if(array_get($data,'total') !== 0){
                $exist = true;
            }
        }
        #2.3 return exist
        return $exist;
    }

}
