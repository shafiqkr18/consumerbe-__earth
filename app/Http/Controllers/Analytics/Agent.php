<?php

namespace App\Http\Controllers\Analytics;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class Agent extends Controller
{
    const leaf          =   'agent_analytics';
    const datastore     =   'els';
    const metadata      =   [];

    public function store( Request $request = null )
    {
        $this->payload  =   collect( $request->all() )->toArray();

        if(!array_has( $this->payload, 'agent.contact.email' )){
            array_set($this->payload, 'agent', $this->get_details('agent'));
        }
        return Parent::store();
    }

}
