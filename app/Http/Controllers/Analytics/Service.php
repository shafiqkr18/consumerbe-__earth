<?php

namespace App\Http\Controllers\Analytics;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class Service extends Controller
{
    const leaf          =   'service_analytics';
    const datastore     =   'els';
    const metadata      =   [];

    public function store( Request $request = null )
    {
        $this->payload  =   collect( $request->all() )->toArray();

        if(!array_has( $this->payload, 'service.contact.email' )){
            array_set($this->payload, 'service', $this->get_details('service'));
        }
        return Parent::store();
    }

}
