<?php

namespace App\Http\Controllers\Analytics;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class Property extends Controller
{
  const leaf          =   'property_analytics';
  const datastore     =   'els';
  const metadata      =   [];

  public function store( Request $request = null )
  {
    $this->payload  =   collect( $request->all() )->toArray();

    if( !array_has( $this->payload, 'property.ref_no' ) && !array_has( $this->payload, 'preferences' ) ){
      array_set($this->payload, 'property', $this->get_details('property'));
    }
    $return =  Parent::store();

    if( $return->getStatusCode() === 200 ){
      if(array_get($this->payload,'action') !== 'call' && array_get($this->payload,'action') !== 'whatsapp' && array_get($this->payload,'settings.approved')){
        $lqs     =  new \App\Http\Controllers\ThirdPartyLQS;
        $lqs->third_party_integration($this->payload, 'property');
      }
    }
    return $return;
  }

}
