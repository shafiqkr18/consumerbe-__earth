<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Agent extends Controller
{
    const leaf          =   'agent';
    const datastore     =   'els';
    const metadata      =   [];

    public function store( Request $request = null ){

        /**
         *  1.  Register Developer in RDS
         *  2.  Store details in ElasticSearch
         */

        $this->payload        =   collect( $request->all() )->toArray();
        if(!array_has($this->payload, 'credentials.password') || !array_has($this->payload, 'credentials.password_confirmation')){
            return response()->json( [ 'error' => 'password and password_confirmation are required' ], 404 );
        }
        else{
            if( array_get($this->payload,'credentials.password',1) === array_get($this->payload,'credentials.password_confirmation',2) ){
                #   1
                $credentials = array_get($this->payload, 'credentials');
                array_set($credentials,'name',array_get($this->payload, 'contact.name'));
                array_set($credentials,'email',array_get($this->payload, 'contact.email'));
                array_set($credentials,'role_id',array_get($this->payload, 'settings.role_id', 3));
                // Generate Id
                if(!array_has($this->payload, '_id')){
                    array_set($this->payload, '_id', $this->generate_id($this::leaf));
                }
                array_set($credentials,'_id',array_get($this->payload, '_id'));
                $user_data          =   new \Illuminate\Http\Request();
                $auth               =   new Auth();
                $auth_rsp           =   $auth->register($user_data->replace($credentials));

                if($auth_rsp->getStatusCode() == 201){
                    #   2
                    array_forget( $this->payload,'credentials' );
                    if(!array_has( $this->payload, 'agency.contact.email' )){
                        array_set( $this->payload, 'agency', $this->get_details('agency') );
                    }
                    return Parent::store();
                }
                else{
                    return $auth_rsp;
                }
            }
            else{
                return response()->json(['message' => 'Passwords do not match'], 400);
            }
        }

    }

    public function update(Request $request, ...$vars){
        /**
         * Update Index
         * Bulk update dependencies
         */
        $bulk = array_get($this->payload, 'bulk', false);
        $update_response = Parent::update($request,...$vars);
        //Bulk Update agent and properties
        if($bulk && $update_response->getStatusCode() == 200){
          /** Property document update_by_query */
            $this->payload                      =  [];
            $this->payload[$this::leaf]['_id']  =  array_get($vars,'0',null);
            $query = $this->payload;
            array_set($this->payload, $this::leaf, $this->get_details($this::leaf));

            Parent::update_by_query($query,$this->payload,'property');
            return $update_response;
        }else{
          return $update_response;
        }
    }

    protected function construct_query_for_similar( $agent ){

        return [
            'contact'  =>  [
                'address' => [
                    'state' => [
                        array_get( $agent, 'contact.address.state', '' )
                    ],
                    'city' => [
                        array_get( $agent, 'contact.address.city', '' )
                    ]
                ]
            ],
            'settings'  =>  [
                'status'    =>  array_get( $agent, 'settings.status', 1 )
            ],
            'size'      =>  5
        ];

    }
}
