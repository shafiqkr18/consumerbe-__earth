<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    protected $payload;

    use \App\Traits\Package;
    use \App\Traits\JsonValidator;
    use \App\Traits\Common;

    public function __construct(Request $request = null){
        $this->payload  =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
    }

    /**
     * Delegating to Packages
     */
    public function index(){
        // TODO Featured {model} should honour featured.from < now < featured.to
        // TODO Organic {model} should honour featured.to < now
        $this->construct_payload();
        return $this    ->set_config($this::leaf,$this->payload)
                        ->index();
    }

    public function similar( $id ){

        /**
         *  1. Get model details to construct request
         *  2. Construct request
         *  3. Get similar models
         */
        #   1
        $details   =   $this->show( $id );
        if( $details->getStatusCode() === 200 ){
            #   2
            $this->payload    =   $this->construct_query_for_similar( $details->getData(1) );
            #   3
            return $this->index();
        }
        else{
            return response()->json( [ 'error' => $this::leaf.' not found' ], $details->getStatusCode() );
        }
    }

    public function show( $id ){
        return $this    ->set_config($this::leaf)
                        ->show($id);
    }

    public function store( Request $request = null){
        $request        =  is_null( $request ) ? $this->payload  : collect($request->all())->toArray();
        // Generate Id
        if(!array_has($request, '_id')){
            array_set($request, '_id', $this->generate_id($this::leaf));
        }
        if(!array_has($request,'settings.status')){
            array_set($request,'settings.status',1);
        }
        return $this    ->set_config( $this::leaf )
                        ->store( $request );
    }

    public function update(Request $request, ...$vars){
        $request   =  is_null( $request->all() ) ? $this->payload  : collect($request->all())->toArray();
        $model_id  =  end($vars);
        $valid_id  =  $this->validate_id($this::leaf, $model_id);
        if(!is_null($model_id) || !$valid_id){
            if(array_has($request, 'contact.email')){
                array_forget($request, 'contact.email');
            }
            if(array_has($request, 'ref_no')){
                array_forget($request, 'ref_no');
            }
            if(array_has($request, 'stub')){
                array_forget($request, 'stub');
            }
            if(array_has($request, 'settings.status') && !array_has($request, 'settings.approved')){
                $request['settings']['approved'] = array_get($request,'settings.status') == 1 ? true : false;
            }
            return $this    ->set_config( $this::leaf )
                            ->update( $request, $model_id );
        }
        else{
          response()->json(['message' => 'Invalid Id'], 404);
        }
    }

    public function update_by_query($query,$request,$index, $prefix=''){
        $query = $this->construct_payload($query);
        return $this    ->set_config( $index, $query )
                        ->update_by_query( $request, $prefix );
    }

    public function destroy(...$vars){
        $id   =   end($vars);
        $request                                =   new \Illuminate\Http\Request();
        $payload                                =   [];
        $payload[ 'settings' ][ 'status' ]      =   0;
        $payload[ 'settings' ][ 'approved' ]    =   false;
        $request->replace($payload);
        return $this->update($request,$id);
    }

    public function construct_payload($param = []){
        $payload    =   [];
        $input      =   !empty($param) ? $param : $this->payload;
        $clauses    =   array_intersect_key($input,array_flip(array('sort', 'size','page')));
        $query      =   array_except($input,['sort','size','page']);

        if(!empty($query)){
            $payload[ 'query' ][]   =   $query;
        }
        else{
            $payload[ 'query' ]     =   [];
        }
        if(!empty($param)){
            return array_merge($payload,$clauses);
        }
        else{
            $this->payload = array_merge($payload,$clauses);
        }
    }

}
