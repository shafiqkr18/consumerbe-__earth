<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Brick\PhoneNumber\PhoneNumber;
use Brick\PhoneNumber\PhoneNumberParseException;

class Lead extends User
{
    const leaf          =   'lead';
    const datastore     =   'els';
    const metadata      =   [];

    use \App\Traits\Package;

    public function __construct( Request $request ){
        $this->payload = $request;
    }

    public function all(...$var)
    {
        $model      =   array_get( $var, 0 );

        /**
        *  List all leads of a model
        */
        if( !empty( $model ) ){
            $controller = $this->set_analytics_controller($model);
            return $controller->index();
        }
        /**
        *  List all user leads
        */
        else{

        }
    }

    public function index(...$var)
    {
        $user       =   array_get( $var, 0 );
        $model      =   array_get( $var, 1 );
        $model_id   =   array_get( $var, 2 );

        /**
        *  List all leads of a model
        */
        if( !empty( $model ) ){
            $controller = $this->set_analytics_controller($model);
            return $controller->index();
        }
        /**
        *  List all user leads
        */
        else{

        }
    }

    public function store(Request $request, ...$vars)
    {
        $model = array_get( $vars, 0 );
        if( !empty( $model ) ){
            $controller = $this->set_analytics_controller($model);
            #Removing emoji from user name
            $user       = array_get($this->payload->all(), 'user');
            $lead_name  = remove_emoji(array_get($user,'contact.name','Dear Sir or Madam'));
            $lead_name  = trim(preg_replace(['/[^\x{0600}-\x{06FF}A-Za-z]/u', '/\s+/'], [' ',' '], $lead_name));
            array_set($user, 'contact.name',$lead_name);
            #PHONE NUMBER
            $phone = array_get($user,'contact.phone','');
            $phone = is_null($phone) ? '' : $phone;
            try {
                #1. check if number starting with 00
                if( starts_with( $phone, '00' ) ) {
                    #1.1 change the 00 to + for validation
                    $phone = str_replace('00','+', $phone);
                }
                $region = str_contains( $phone, '+' ) && !starts_with( $phone, '0' ) ? null : ($this->payload->has('country_source') ? $this->payload->input('country_source') : 'UAE');
                $region = $region == 'UAE' ? 'AE' : ($region == 'KSA' ? 'SA' : $region);
                $phone_number = PhoneNumber::parse($phone,$region);
                if ($phone_number->isPossibleNumber()) {
                    $phone = '+'.$phone_number->getCountryCode().$phone_number->getNationalNumber();
                }
            }
            catch (PhoneNumberParseException $e) {
                // return $e;
            }
            array_set($user, 'contact.phone',$phone);
            $this->payload->merge(['user' => $user]);
            if($this->payload->input('settings.approved', false)){
                $timestamp = array_get($this->payload->all(), 'timestamp');
                array_set($timestamp,'approved',date('Y-m-d\TH:i:s\Z'));
                $this->payload->merge(['timestamp' => $timestamp]);
                #QUALIFICATION
                $this->payload->merge(['qualification' => ['status' => 'approved', 'note' => '']]);
            }
            else{
                $this->payload->merge(['qualification' => ['status' => 'unapproved', 'note' => '']]);
            }
            return $controller->store($this->payload);

        }
        else{
            return response()->json( [ 'message' => 'Set model first'], 400 );
        }
    }

    public function update(Request $request, ...$vars)
    {
        $model = array_get( $vars, 0 );
        if( !empty( $model ) ){
            $controller = $this->set_analytics_controller($model);
            $action = $this->payload->has('action') ? $this->payload->input('action') : '';
            #QUALIFICATION
            if(!$this->payload->has('qualification') && $this->payload->input('settings.approved', false)){
                $this->payload->merge(['qualification' => ['status' => 'approved', 'note' => '']]);
            }
            else if(!$this->payload->has('qualification') && !$this->payload->input('settings.approved', false)){
                $this->payload->merge(['qualification' => ['status' => 'unapproved', 'note' => '']]);
            }
            else if($this->payload->has('qualification')){
              $lead_rtn_data  =  $this->show(...$vars);
              if( $lead_rtn_data->getStatusCode() === 200){
                  $lead_data  = $lead_rtn_data->getData(1);
                  $q          = $this->payload->input('qualification');
                  $q['status']= !empty(array_get($q,'status')) ? snake_case(array_get($q,'status')) : ($this->payload->has('settings.approved') && $this->payload->input('settings.approved', false) ? 'approved' : 'unapproved');
                  $q['note']  = empty(array_get($lead_data,'qualification.note','')) ? array_get($q,'note') : array_get($lead_data,'qualification.note','').' | '.array_get($q,'note');
                  $this->payload->merge(['qualification' => $q]);
              }
            }
            $return =  $controller->update($this->payload, ...$vars);
            if( $return->getStatusCode() === 200 && in_array($model, ['property', 'project'])){
                if($action !== 'call' && $action !== 'whatsapp'){
                    $lqs     =  new ThirdPartyLQS;
                    $this->payload->merge(['_id' => end($vars)]);
                    $lqs->third_party_integration($this->payload->all(), $model);
                }
            }
            return $return;
        }
        else{
            return response()->json( [ 'message' => 'Set model first'], 400 );
        }
    }

    public function destroy(...$vars)
    {
        $model = array_get( $vars, 0 );
        if( !empty( $model ) ){
            $controller = $this->set_analytics_controller($model);
            return $controller->destroy(...$vars);
        }
        else{
            return response()->json( [ 'message' => 'Set model first'], 400 );
        }
    }

    /**
    * Display the specified resource.
    *
    * @param  string  $user_id, $model, $model_id
    * @return \Illuminate\Http\JsonResponse
    */
    public function show(...$vars)
    {
        $model  = array_get( $vars, 0 );
        $id     = array_get( $vars, 1 );
        if( !empty( $model ) ){
            $controller = $this->set_analytics_controller($model);
            return $controller->show($id);
        }
        else{
            return response()->json( [ 'message' => 'Set model first'], 400 );
        }
    }

    private function set_analytics_controller($model){
        switch( $model ){
            case 'property':
            $controller = new Analytics\Property($this->payload);
            break;
            case 'agent':
            $controller = new Analytics\Agent($this->payload);
            break;
            case 'project':
            $controller = new Analytics\Project($this->payload);
            break;
            case 'service':
            $controller = new Analytics\Service($this->payload);
            break;
            default:
            return response()->json( [ 'message' => 'Invalid Model' ], 404 );
        }
        return $controller;
    }
}
