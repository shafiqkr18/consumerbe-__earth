<?php

namespace App\Http\Controllers\Team;

use App\ZpTeam;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Collection;
//Required to hash the password
use Illuminate\Support\Facades\Hash;

class Team extends BaseController
{
    const leaf  =   'team';
    use \App\Traits\JsonValidator;

    public function __construct(Request $request = null){
        $this->payload  =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
    }

    public function index(){
        $filter         =   [];
        $team_fields    =   [ 'id', '_id', 'name', 'phone', 'address', 'about', 'dept_id', 'role_id', 'status', 'created_at', 'updated_at' ];

        foreach($this->payload as $key => $value){
            if(in_array($key, $team_fields)){
                $key = 'zp_teams.'.$key;
            }
            $filter[$key]  = $value;
        }

        $user = DB::table('zp_teams')
                ->leftJoin('zp_team_roles as role', 'role._id', '=', 'zp_teams.role_id')
                ->leftJoin('zp_team_departments as dept', 'dept._id', '=', 'zp_teams.dept_id')
                ->where($filter)
                ->select('zp_teams._id', 'zp_teams.name', 'zp_teams.email', 'zp_teams.phone', 'zp_teams.address', 'zp_teams.gender',
                         'zp_teams.qualification', 'zp_teams.nationality', 'zp_teams.picture',
                         'zp_teams.about', 'zp_teams.status', 'zp_teams.resources', 'zp_teams.created_at', 'zp_teams.updated_at',
                         'dept.name as dept_name','dept._id as dept_id','role.role as role_name','role._id as role_id','role.permissions' )
                ->get();

        if( !is_null( $user ) ){
            return response()->json([
                    'total' =>  count($user),
                    'data'  =>  $user
                ], 200);
        }
        else{
            return response()->json(['message' => 'Records not found.'], 400);
        }
    }

    public function store(Request $request){
        $request    =   is_null( $request ) ? $this->payload  : collect($request->all())->toArray();

        if(!array_has($request,'status')){
            array_set($request,'status',1);
        }

        $exists     =   ZpTeam::where([ [ 'email', '=', array_get($request,'email') ] ])->first();

        if( is_null( $exists ) ){
            $record      =   [
                '_id'           =>  'zt-'.rtrim( strtr( base64_encode(array_get($request,'email')), '+/', '-_' ), '=' ),
                'name'          =>  array_get($request,'name'),
                'email'         =>  array_get($request,'email'),
                'phone'         =>  array_get($request,'phone'),
                'password'      =>  Hash::make(array_get($request,'password')),
                'dept_id'       =>  array_get($request,'dept_id'),
                'role_id'       =>  array_get($request,'role_id'),
                'address'       =>  array_get($request,'address'),
                'gender'        =>  array_get($request,'gender'),
                'qualification' =>  array_get($request,'qualification'),
                'nationality'   =>  array_get($request,'nationality'),
                'picture'       =>  array_get($request,'picture'),
                'about'         =>  array_get($request,'about'),
                'resources'     =>  array_get($request,'resources'),
                'status'        =>  array_get($request,'status')
            ];
            try{
                $user = ZpTeam::create($record);
                return response()->json( [ "message" => 'User created with _id: '.$user->_id ], 200 );
            }
            catch( \Exception $e ){
                return  response()->json( [ 'message' => $e->getMessage() ], 500 );
            }
        }
        else{
            return  response()->json( [ 'message' => 'User already exists' ], 400 );
        }
    }

    public function login(Request $request){
        $request    =   is_null( $request ) ? $this->payload  : collect($request->all())->toArray();

        $user   =   DB::table('zp_teams')
                        ->leftJoin('zp_team_roles as role', 'role._id', '=', 'zp_teams.role_id')
                        ->leftJoin('zp_team_departments as dept', 'dept._id', '=', 'zp_teams.dept_id')
                        ->where('zp_teams.email',array_get($request,'email'))
                        ->select('zp_teams._id', 'zp_teams.name', 'zp_teams.email', 'zp_teams.phone', 'zp_teams.address', 'zp_teams.gender',
                                 'zp_teams.password', 'zp_teams.qualification', 'zp_teams.nationality', 'zp_teams.picture',
                                 'zp_teams.about', 'zp_teams.status', 'zp_teams.resources', 'zp_teams.created_at', 'zp_teams.updated_at',
                                 'dept.name as dept_name','dept._id as dept_id','role.role as role_name','role._id as role_id','role.permissions' )
                        ->get();

        if(!is_null($user)){
            $return = json_decode(json_encode($user),1);
            $return = array_get($return,0);
            if( Hash::check(array_get($request,'password'), array_get($return,'password')) ){
                if(array_get($return,'status',0) == 1){
                    array_forget($return, 'password');
                    return $return;
                }
                else{
                    return  response()->json( [ 'message' => 'Your account is inactive. Please contact administrator.' ], 400 );
                }
            }
            else{
                return  response()->json( [ 'message' => 'Incorrect password' ], 400 );
            }
        }
        else{
            return  response()->json( [ 'message' => 'User does not exist' ], 400 );
        }
    }

    public function show($id){
        $user = DB::table('zp_teams')
            ->leftJoin('zp_team_roles', 'zp_team_roles._id', '=', 'zp_teams.role_id')
            ->leftJoin('zp_team_departments as dept', 'dept._id', '=', 'zp_teams.dept_id')
            ->where('zp_teams._id',$id)
            ->select('zp_teams._id', 'zp_teams.name', 'zp_teams.email', 'zp_teams.phone', 'zp_teams.address', 'zp_teams.gender',
                     'zp_teams.qualification', 'zp_teams.dept_id', 'zp_teams.role_id', 'zp_teams.nationality', 'zp_teams.picture',
                     'zp_teams.about', 'zp_teams.status', 'zp_teams.resources', 'zp_teams.created_at', 'zp_teams.updated_at',
                     'dept.name as dept_name','dept._id as dept_id','zp_team_roles.role as role_name','zp_team_roles._id as role_id','zp_team_roles.permissions')
            ->get();

        if( !is_null( $user ) ){
            $return = json_decode(json_encode($user),1);
            return response()->json(array_get($return,0), 200);
        }
        else{
            return response()->json(['message' => 'User not found.'], 400);
        }
    }

    public function update(Request $request, $id){

        $payload    =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
        $update     =   [];

        if(array_has($payload, 'email')){
            array_forget($payload, 'email');
        }
        if(array_has($payload, '_id')){
            array_forget($payload, '_id');
        }
        if(array_has($payload, 'password')){
            array_forget($payload, 'password');
        }

        if(!empty($payload)){
            foreach($payload as $key => $value){
                $update[$key]  =    $value;
            }
            $return  =   ZpTeam::where('_id', '=', $id)->update($update);
            if($return){
                return response()->json(['message' => 'User details updated successfully'], 200);
            }
            else{
                return response()->json(['message' => 'User details could not be updated'], 400);
            }
        }
        else{
            return response()->json(['message' => 'Insufficient Input'], 400);
        }

    }

    public function change_password(Request $request, ...$vars){
        $id         =   array_get($vars,'0',false);
        $request    =   $request->all();

        if( array_get($request,'password',1) === array_get($request,'password_confirmation',2) ){
            $user   =   ZpTeam::where('_id',$id)->first();
            if($user){
                if(password_verify( array_get( $request,'old_password' ), array_get($user,'password'))){
                    $update['password']  =  Hash::make(array_get($request,'password'));
                    $return  =   ZpTeam::where('_id', '=', $id)->update($update);
                    return response()->json(['message' => 'Password updated successfully'], 200);
                }
                else{
                    return response()->json(['message' => 'Incorrect old password.'], 400);
                }
            }
            else{
                return response()->json(['message' => 'These credentials do not match our records.'], 400);
            }
        }
        else{
            return response()->json(['message' => 'Passwords dont match'], 400);
        }
    }

}
