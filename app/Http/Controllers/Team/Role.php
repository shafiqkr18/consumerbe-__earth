<?php

namespace App\Http\Controllers\Team;

use App\ZpTeamRole;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Collection;
//Required to hash the password
use Illuminate\Support\Facades\Hash;

class Role extends BaseController
{
    const leaf  =   'team_role';
    use \App\Traits\JsonValidator;

    public function __construct(Request $request = null){
        $this->payload  =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
    }

    public function index(){
        $filter         =   [];
        $role_fields    =   [ 'id', '_id', 'role', 'dept_id', 'status', 'created_at', 'updated_at' ];

        foreach($this->payload as $key => $value){
            if(in_array($key, $role_fields)){
                $key = 'role.'.$key;
            }
            $filter[$key]  = $value;
        }
        $filter =   array_filter($filter);
        $role   =   DB::table('zp_team_roles as role')
                    ->leftJoin('zp_team_departments as dept', 'dept._id', '=', 'role.dept_id')
                    ->where($filter)
                    ->select('role._id', 'role.role', 'role.dept_id', 'dept.name as dept_name','role.permissions', 'role.status', 'role.created_at', 'role.updated_at')
                    ->get();

        if( !is_null( $role ) ){
            return response()->json([
                    'total' =>  count($role),
                    'data'  =>  $role
                ], 200);
        }
        else{
            return response()->json(['message' => 'Records not found.'], 400);
        }
    }

    public function store(Request $request){
        $request    =   is_null( $request ) ? $this->payload  : collect($request->all())->toArray();

        if(!array_has($request,'status')){
            array_set($request,'status',1);
        }

        $exists     =   ZpTeamRole::where([ [ 'role', '=', array_get($request,'role') ] ])->first();

        if( is_null( $exists ) ){
            $record      =   [
                '_id'           =>  'ztr-'.random_number(10),
                'dept_id'       =>  array_get($request,'dept_id'),
                'role'          =>  array_get($request,'role'),
                'permissions'   =>  array_get($request,'permissions'),
                'status'        =>  array_get($request,'status')
            ];
            try{
                $role = ZpTeamRole::create($record);
                return response()->json( [ "message" => 'Role created with _id: '.$role->_id ], 200 );
            }
            catch( \Exception $e ){
                return  response()->json( [ 'message' => $e->getMessage() ], 500 );
            }
        }
        else{
            return  response()->json( [ 'message' => 'Role already exists' ], 400 );
        }
    }

    public function show($id){
        $role = DB::table('zp_team_roles as role')
                ->leftJoin('zp_team_departments as dept', 'dept._id', '=', 'role.dept_id')
                ->where('role._id',$id)
                ->select('role._id', 'role.role', 'role.dept_id', 'dept.name as dept_name','role.permissions', 'role.status', 'role.created_at', 'role.updated_at')
                ->get();

        if( !is_null( $role ) ){
            $return = json_decode(json_encode($role),1);
            return response()->json(array_get($return,0), 200);
        }
        else{
            return response()->json(['message' => 'Role not found.'], 400);
        }

    }

    public function update(Request $request, $id){

        $payload    =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
        $update     =   [];

        if(array_has($payload, '_id')){
            array_forget($payload, '_id');
        }

        if(!empty($payload)){
            foreach($payload as $key => $value){
                $update[$key]  =    $value;
            }
            $return  =   ZpTeamRole::where('_id', '=', $id)->update($update);
            if($return){
                return response()->json(['message' => 'Team role updated successfully'], 200);
            }
            else{
                return response()->json(['message' => 'Team role could not be updated'], 400);
            }
        }
        else{
            return response()->json(['message' => 'Insufficient Input'], 400);
        }

    }

}
