<?php

namespace App\Http\Controllers\Team;

use App\ZpTeamDepartment;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Collection;
//Required to hash the password
use Illuminate\Support\Facades\Hash;

class Department extends BaseController
{
    const leaf  =   'team_department';
    use \App\Traits\JsonValidator;

    public function __construct(Request $request = null){
        $this->payload  =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
    }

    public function index(){
        $filter     =   $results    =   [];
        foreach($this->payload as $key => $value){
            $filter[$key]  = $value;
        }
        $filter         =   array_filter($filter);
        $department     =   ZpTeamDepartment::where($filter);
        if( !is_null( $department ) ){
            $results = $department->get()->all();
            return response()->json([
                    'total' =>  count($results),
                    'data'  =>  $results
                ], 200);
        }
        else{
            return response()->json(['message' => 'Records not found.'], 400);
        }
    }

    public function store(Request $request){
        $request    =   is_null( $request ) ? $this->payload  : collect($request->all())->toArray();

        if(!array_has($request,'status')){
            array_set($request,'status',1);
        }

        $exists     =   ZpTeamDepartment::where([ [ 'name', '=', array_get($request,'name') ] ])->first();

        if( is_null( $exists ) ){
            $record      =   [
                '_id'           =>  'ztd-'.random_number(10),
                'name'          =>  array_get($request,'name'),
                'about'         =>  array_get($request,'about'),
                'status'        =>  array_get($request,'status')
            ];
            try{
                $department = ZpTeamDepartment::create($record);
                return response()->json( [ "message" => 'Department created with _id: '.$department->_id ], 200 );
            }
            catch( \Exception $e ){
                return  response()->json( [ 'message' => $e->getMessage() ], 500 );
            }
        }
        else{
            return  response()->json( [ 'message' => 'Department already exists' ], 400 );
        }
    }

    public function show($id){
        $department   =   ZpTeamDepartment::where('_id',$id)->first();
        if( !is_null( $department ) ){
            return $department;
        }
        else{
            return response()->json(['message' => 'Department not found.'], 400);
        }
    }

    public function update(Request $request, $id){

        $payload    =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
        $update     =   [];

        if(array_has($payload, '_id')){
            array_forget($payload, '_id');
        }

        if(!empty($payload)){
            foreach($payload as $key => $value){
                $update[$key]  =    $value;
            }
            $return  =   ZpTeamDepartment::where('_id', '=', $id)->update($update);
            if($return){
                return response()->json(['message' => 'Department updated successfully'], 200);
            }
            else{
                return response()->json(['message' => 'Department could not be updated'], 400);
            }
        }
        else{
            return response()->json(['message' => 'Insufficient Input'], 400);
        }

    }

}
