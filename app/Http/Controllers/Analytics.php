<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class Analytics extends BaseController
{
    const leaf          =   'analytics';
    const datastore     =   'els_a';
    const metadata      =   [];

    protected $payload;

    use \App\Traits\Package;
    use \App\Traits\JsonValidator;

    public function __construct(Request $request = null){
        $this->payload  =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
        $this->construct_payload();
    }

    public function total_count($model = 'property'){
        return $this    ->set_config($model, $this->payload)
        ->total_entity();
    }

    public function leads_per_action_count($model = 'property_analytics'){
        // if(!str_contains($model, '_analytics'))
        // return response()->json( [ 'message' => $model.' is not an analytic model' ], 404 );
        $data = $this   ->set_config($model, $this->payload)
        ->aggs_per_action();
        return $this->aggs_response($data);
    }

    public function top_entity($model = 'property_analytics'){
        if(!str_contains($model, '_analytics'))
        return response()->json( [ 'message' => $model.' is not an analytic model' ], 404 );
        $data       = $this   ->set_config($model, $this->payload)
        ->aggs_per_action();
        return $this->aggs_response($data);
    }

    public function leads_histogram($model = 'property_analytics'){
        if(!str_contains($model, '_analytics'))
        return response()->json( [ 'message' => $model.' is not an analytic model' ], 404 );
        $data = $this   ->set_config($model, $this->payload)
        ->aggs_per_action();
        $data        = $data[ 'aggregations' ];
        $keys        = array_keys($data);
        $bucket_key  = array_get($keys, 0);
        $buckets     = array_get($data, $bucket_key, []);
        $response    = [];
        $labels      = array_column($buckets['buckets'], 'key_as_string');
        $datasets    = [];
        $actions     = [];

        foreach($buckets['buckets'] as $bucket){
            end($bucket);         // move the internal pointer to the end of the array
            $single_bucket_key  = key($bucket);
            foreach($bucket[$single_bucket_key]['buckets'] as $leaf_data){
                $actions[$leaf_data['key']][$bucket['key_as_string']] = $leaf_data['doc_count'];
            }
        }
        $index = 0;
        foreach($actions as $key_action => $data_action){
            $datasets[$index]['label']  = $key_action;
            foreach($labels as $label){
                $datasets[$index]['data'][]   = (!empty($data_action[$label])) ? $data_action[$label] : 0;
            }
            $index ++;
        }
        $response[$bucket_key]['labels'] = $labels;
        $response[$bucket_key]['datasets'] = $datasets;
        return response()->json( $response, 200 );
    }

    private function aggs_response($results){
        $data        = $results[ 'aggregations' ];
        $keys        = array_keys($data);
        $bucket_key  = array_get($keys, 0);
        $response    = [];
        $buckets     = array_dot(array_get($data, $bucket_key, []));
        // dd($buckets);
        foreach($buckets as $key => $bucket){
            if(str_contains($key, '.key_as_string')){
                $bucket_name    =  $bucket_key.'.'.str_before($key, '.key_as_string' ).'.date';
            }
            else if(str_contains($key, '.key')){
                $bucket_name    =  $bucket_key.'.'.str_before($key, '.key' ).'.name';
            }else if(str_contains($key, '.doc_count') && !str_contains($key, 'sum_other_doc_count') && !str_contains($key, 'doc_count_error_upper_bound')){
                $bucket_name    =  $bucket_key.'.'.str_before($key, '.doc_count' ).'.total';
            }
            else if(str_contains($key, '.hits.hits.hits.0._source')){
                $bucket_name    =  $bucket_key.'.'.str_before($key, '.hits.hits.hits.0._source' ).'.hits'.str_after($key, '.hits.hits.hits.0._source' );
            }
            else{
                continue;
            }
            array_set($response, $bucket_name, $bucket);
        }

        return response()->json( $response, 200 );
    }

    public function guide_me_listings($model = 'property'){

        //1.-
        if(!array_has($this->payload, 'query.0.coordinates.lat') || !array_has($this->payload, 'query.0.coordinates.lng') )
            return response()->json( [ 'message' => 'Coordinates are required' ], 404 );
        if(!array_has($this->payload, 'query.0.settings.status'))
            array_set($this->payload, 'query.0.settings.status', 1);

        //2.-
        $listings =  $this->set_config($model, $this->payload)
                         ->guide_me_properties_radius();

        return $this->response($listings);
    }

    public function guide_me_stats($model = 'property'){
        /**
        * 1.- Validations( coordinates are required) TODO move to schema
        * 2.- Analytics per specific area
        */
        //TODO Add timestamp after a year of data
        $analytics =  $this->set_config($model, $this->payload)
                         ->aggs_guide_me_stats();

        $analytics   = $analytics[ 'aggregations' ];
        $keys        = array_keys($analytics);
        $bucket_key  = array_get($keys, 0);
        $types         = [];
        $buckets     = array_get($analytics, $bucket_key, []);
        $buckets     = array_dot(array_get($analytics, $bucket_key, []));
        // dd($buckets);
        foreach($buckets as $key => $bucket){
            // dd($key, $bucket);
            if(str_contains($key, '.key') && !str_contains($key, 'rent_buy')){
                $bucket_name    =  $bucket_key.'.'.str_replace('buckets.','',str_before($key, '.key' )).'.name';
            }else if(str_contains($key, '.doc_count') && !str_contains($key, 'sum_other_doc_count') && !str_contains($key, 'doc_count_error_upper_bound') && !str_contains($key, 'rent_buy')){
                $bucket_name    =  $bucket_key.'.'.str_replace('buckets.','',str_before($key, '.doc_count' )).'.total';
            }
            else if(str_contains($key, '.key') && str_contains($key, '.rent_buy') && !str_contains($key, 'sum_other_doc_count') && !str_contains($key, 'doc_count_error_upper_bound') && !str_contains($key, 'price_per_month')){
                $bucket_name    =  $bucket_key.'.'.str_replace('buckets.','',str_before($key, '.rent_buy' )).'.rent_buy'.str_replace(['buckets.','key'],['','name'],str_after($key, '.rent_buy' ));
            }
            else if(str_contains($key, '.avg_price')){
                $bucket_name    =  $bucket_key.'.'.str_replace('buckets.','',str_before($key, '.avg_price' )).'.avg_price';
                $bucket = round($bucket, 2);
            }
            else if(str_contains($key, '.avg_builtup_area')){
                $bucket_name    =  $bucket_key.'.'.str_replace('buckets.','',str_before($key, '.avg_builtup_area' )).'.avg_builtup_area';
                $bucket = round($bucket, 2);
            }
            else if(str_contains($key, '.price_per_month.' ) && str_contains($key, '.key_as_string' )){
                $bucket_name  =  $bucket_key.'.'.str_replace('buckets.','',str_before($key, '.price_per_month.' )).str_replace(['buckets.', '.key_as_string'],['.trends.price.price_per_month.', '.date'],str_after($key, '.price_per_month.' ));
            }
            else if(str_contains($key, '.price_per_month' ) && str_contains($key, '.doc_count' )){
                $bucket_name  =  $bucket_key.'.'.str_replace('buckets.','',str_before($key, '.price_per_month.' )).str_replace(['buckets.', '.doc_count'],['.trends.price.price_per_month.', '.total'],str_after($key, '.price_per_month.' ));
            }
            else if(str_contains($key, '.price_per_month' ) && str_contains($key, '.price.value' )){
                $bucket_name  =  $bucket_key.'.'.str_replace('buckets.','',str_before($key, '.price_per_month.' )).str_replace(['buckets.', 'price.value'],['.trends.price.price_per_month.', 'price'],str_after($key, '.price_per_month.' ));
                if($bucket == 0){
                    $price_key = str_replace('.price','',str_after($bucket_name, '.price_per_month.' ));
                    if($price_key !== "0"){
                        $previous_key = (integer)$price_key - 1;
                        $data_key = str_replace('.price_per_month.buckets.'.$price_key.'.price.value','.price_per_month.buckets.'.$previous_key.'.price.value',$key);
                        $bucket = round(array_get($buckets,$data_key), 2);
                    }
                }
                else{
                    $bucket = round($bucket, 2);
                }
                // dd($bucket_name);

            }
            else{
                continue;
            }
            array_set($types, $bucket_name, $bucket);
        }
        return response()->json($types,200);
    }

    // Response formatter
    public function response($response){
        $stripped_data  =   [];
        //Aggregation with Read response format
        if(array_has($response,'aggregations',false) && count($response['hits']['hits']) == 0){
            // Set total
            array_set($stripped_data,'total',array_get($response,'hits.total',0));
            $key = 0;
            foreach(array_get($response,'aggregations') as $agg){
                foreach(array_get($agg,'buckets') as $bucket){
                    foreach(array_get($bucket,'top_hits.hits.hits') as $hit){
                        array_set($stripped_data,'data.'.$key,array_get($hit,'_source',[]));
                        // Set id
                        array_set($stripped_data,'data.'.$key.'._id',array_get($hit,'_id',null));
                        $key ++;
                    }
                }
            }
        }
        // Read
        else if(array_has($response,'hits.hits',false)){
            // Set total
            array_set($stripped_data,'total',array_get($response,'hits.total',0));

            $hits           =   array_get($response,'hits.hits',[]);
            for($i=0;$i<count($hits);$i++){

                array_set($stripped_data,'data.'.$i,array_get($hits[$i],'_source',[]));
                // Set id
                array_set($stripped_data,'data.'.$i.'._id',array_get($hits[$i],'_id',null));
            }
        }
        // Show
        else{
            $stripped_data  =   array_get($response,'_source',[]);
            array_set($stripped_data,'_id',array_get($response,'_id',null));
        }
        return response()->json($stripped_data,200);
    }

    public function construct_payload($param = []){
        $payload    =   [];
        $input      =   !empty($param) ? $param : $this->payload;
        $clauses    =   array_intersect_key($input,array_flip(array('aggs','source','sort','size','page')));
        $query      =   array_except($input,['aggs','source','sort','size','page']);

        if(!empty($query)){
            $payload[ 'query' ][]   =   $query;
        }
        else{
            $payload[ 'query' ]     =   [];
        }
        if(!empty($param)){
            return array_merge($payload,$clauses);
        }
        else{
            $this->payload = array_merge($payload,$clauses);
        }
    }
}
