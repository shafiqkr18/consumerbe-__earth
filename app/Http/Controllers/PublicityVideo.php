<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use BaoPham\DynamoDb\Facades\DynamoDb;

class PublicityVideo extends BaseController
{
    const leaf          =   'publicity_video';
    const datastore     =   'ddb';
    const metadata      =   [];

    use \App\Traits\JsonValidator;
    use \App\Traits\Common;

    public function __construct( Request $request = null ){
        $this->punlicity_video_model  =   new \DataPKG\Models\PublicityVideoDdb;
        $this->payload                 =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
    }

    public function index(){
        /**
        *  1.  Construct request
        *  2.  Get all takeover images matching request
        */

        #   1
        if(!array_has($this->payload,'active')){
            array_set($this->payload,'active',1);
        }
        #   2
        $data   =   $this->punlicity_video_model->get()->toArray();
        $videos_data    =   [];
        list($count,$videos_data)     =   $this->search_json($this->payload,$data);
        return response()->json([
            'total' =>  count($videos_data),
            'data'  =>  $videos_data
        ], 200);
    }

    public function store(Request $request = null){
        /**
        *  1. Construct request
        *  2. Store records in the database
        */

        #   1
        $request    =   is_null( $request ) ? $this->payload  : collect($request->all())->toArray();

        if(!array_has($request, '_id')){
            array_set($request, '_id', $this->generate_id($this::leaf));
        }
        if(!array_has($request, 'active')){
            array_set($request, 'active', 1);
        }
        if(!array_has($request, 'pv_domain')){
            array_set($request, 'pv_domain', ["staging"]);
        }
        #   2

        foreach($request as $key=>$value){
            if( in_array( $key, $this->punlicity_video_model->getFillable() ) ){
                $this->punlicity_video_model->{$key}  = $value;
            }
        }
        if($this->punlicity_video_model->save()){
            return response()->json( [ 'message' => 'Publicity video saved with _id: '.array_get($request,'_id') ], 200 );
        }
        else{
            return response()->json(['Could not store note'], 503);
        }
    }

    public function show($id){
        try{
            $result =   array_get($this->punlicity_video_model->where('_id',$id)->get()->toArray(),0,false);

            if(!empty($result)){
                return response()->json($result,200);
            }
            else{
                return response()->json([ 'message' => 'Publicity video does not exist' ],400);
            }
        }
        catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage()], 500);
        }
    }

    public function update(Request $request,$id){
        /**
        *  1.  Check if record to be updated exists
        *  2.  Compare _ids if not the same create new index
        *  3.  Update record
        */

        #   1
        $exists     =   $this->show($id);
        if($exists->getStatusCode() !== 200){
            return $exists;
        }

        $request    =   is_null($request) ? $this->payload : $request->all();

        array_set($request, 'updated_at', date('Y-m-d\TH:i:s+00:00'));

        #   3
        $update_items       =   $request;
        $update_expression  =   '';
        $attribute_values   =   [];

        foreach($update_items as $key => $value){
            $update_expression  .=   $key.'=:'.$key.',';
        }

        if(!empty($update_expression)){
            $update_expression  =   rtrim($update_expression, ',');
        }
        else{
            return response()->json(['message' => 'Insufficient Input'],400);
        }

        foreach($update_items as $key => $value){
            $attribute_values[':'.$key]  =   DynamoDb::marshalValue($value);
        }
        //dd($update_expression,$attribute_values,$id);
        $execute    =   DynamoDb::table('publicity-video')
        ->setKey(DynamoDb::marshalItem(['_id' => $id]))
        ->setUpdateExpression('SET '.$update_expression)
        ->setExpressionAttributeValues($attribute_values)
        ->prepare()
        ->updateItem();

        if($execute){
            return response()->json([ 'message' => 'Publicity video updated successfully'], 200);
        }
        else{
            return response()->json([ 'message' => 'Publicity video  be updated'], 400);
        }

    }

}
