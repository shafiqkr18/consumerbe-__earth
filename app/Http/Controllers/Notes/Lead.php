<?php

namespace App\Http\Controllers\Notes;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Collection;
use BaoPham\DynamoDb\Facades\DynamoDb;

class Lead extends BaseController
{
    const leaf          =   'lead_notes';
    const datastore     =   'ddb';
    const metadata      =   [];

    protected $lead_notes_model, $request;
    use \App\Traits\JsonValidator;
    use \App\Traits\Common;

    public function __construct( Request $request = null ){
        $this->lead_notes_model =   new \DataPKG\Models\LeadNotesDdb;
        $this->payload          =   !is_null( $request ) ? !empty( $request->query('query') ) ? collect(json_decode( $request->query('query'), 1 ))->toArray() : collect($request->all())->toArray() : [];
    }

    public function index(...$var){
        $lead_id    =   array_get( $var, 0 );
        $filters    =   $this->payload;

        if(!array_has($filters,'active')){
            array_set($filters,'active',1);
        }

        // Get all notes of a lead
        $notes_data  =   DynamoDb::table('lead-notes')
                            ->setKeyConditionExpression('#lead_id = :lead_id')
                            ->setExpressionAttributeNames(['#lead_id' => 'lead_id'])
                            ->setExpressionAttributeValues([':lead_id' => DynamoDb::marshalValue($lead_id)])
                            ->prepare()
                            ->query()
                            ->toArray();

        $notes    =   [];
        $results  =   [];
        if(!empty($notes_data['Items'])){
            $get_notes  =  [];
            foreach($notes_data['Items'] as $item){
                $get_notes[]    =   array_undot(DynamoDb::unmarshalItem($item));
            }
            if(!empty($filters)){
                list($count,$notes)     =   $this->search_json($filters,$get_notes);
            }
        }

        return response()->json([
                'total' =>  count($notes),
                'data'  =>  $notes
            ], 200);
    }

    public function store(Request $request, ...$vars){
        /**
         * 1.   Get data
         * 2.   Store
         */
        $lead_id    =   array_get($vars,'0',false);
        $request    =   is_null( $request->all() ) ? $this->payload  : collect($request->all())->toArray();
        $notes      =   [];

        if($lead_id){
            $notes  =   $request;
            array_set($notes,'active',1);
            array_set($notes,'_id','zln-'.md5(serialize(random_number())));
            array_set($notes,'lead_id',$lead_id);

            foreach($notes as $key=>$value){
                $this->lead_notes_model->{$key}  = $value;
            }

            if($this->lead_notes_model->save()){
                return response()->json(array_undot($notes),200);
            }
            else{
                return response()->json(['Could not store note'], 503);
            }
        }
        else{
            return response()->json(['Lead ID is required.'], 400);
        }
    }

    public function update(Request $request, ...$vars){

        $exists =   $this->show(...$vars);

        if($exists->status() == 200){

            $request    =   is_null($request) ? $this->payload : $request->all();
            $lead_id    =   array_get($vars,'0',false);
            $id         =   array_get($vars,'1',false);

            array_set($request, 'updated_at', date('Y-m-d\TH:i:s+00:00'));

            $update_items       =   $request;
            $update_expression  =   '';
            $attribute_values   =   [];
            $primary_key        =   [];

            foreach($update_items as $key => $value){
                $update_expression  .=   $key.'=:'.$key.',';
            }

            if(!empty($update_expression)){
                $update_expression  =   rtrim($update_expression, ',');
            }
            else{
                return response()->json(['message' => 'Insufficient Input'],400);
            }

            foreach($update_items as $key => $value){
                $attribute_values[':'.$key]  =   DynamoDb::marshalValue($value);
            }

            $execute    =   DynamoDb::table('lead-notes')
                                    ->setKey(DynamoDb::marshalItem([ 'lead_id' => $lead_id, '_id' => $id ]))
                                    ->setUpdateExpression('SET '.$update_expression)
                                    ->setExpressionAttributeValues($attribute_values)
                                    ->prepare()
                                    ->updateItem();

            if($execute){
                return response()->json([ 'message' => 'Lead notes updated successfully'], 200);
            }
            else{
                return response()->json([ 'message' => 'Lead notes cannot be updated'], 400);
            }
        }
        else{
            return $exists;
        }
    }

    public function show(...$vars){
        $lead_id    =   array_get($vars,'0',false);
        $notes_id   =   array_get($vars,'1',false);
        try{
            if($lead_id){
                $notes     =   DynamoDb::table('lead-notes')
                                    ->setKey(DynamoDb::marshalItem([ 'lead_id' => $lead_id, '_id' => $notes_id ]))
                                    ->prepare()
                                    ->getItem()
                                    ->toArray();

                $notes_data =   array_undot(DynamoDb::unmarshalItem($notes['Item']));
                $notes      =   !empty($notes_data) ? $notes_data : [ 'message' => 'No Results' ];
                return response()->json($notes_data, 200);
            }
            else{
                return response()->json(['Lead ID is required.'], 400);
            }
        }
        catch (\Exception $e) {
            return response()->json([ 'message' => 'Lead note not found'], 500);
        }
    }
}
