<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use BaoPham\DynamoDb\Facades\DynamoDb;

class User extends BaseController
{
    const leaf          =   'user';

    protected $user_model,$request;
    use \App\Traits\JsonValidator;

    public function __construct( Request $request = null ){

        $this->user_model   =   new \DataPKG\Models\UserDdb;
        $this->payload      =   !is_null( $request ) ? !empty( $request->query('params') ) ? collect(json_decode( $request->query('params'), 1 ))->toArray() : collect($request->all())->toArray() : '';
    }

    public function store(Request $request, ...$vars){

        $request    =   $this->payload;

        $email      =   array_get($request,'email');
        $user       =   array_get($this->user_model->where('email',$email)->get()->toArray(),'0',false);

        if($user){
            return response()->json(['message' => 'User already exists'], 400);
        }

        // Assuming all values present
        if( array_get($request,'password',1) === array_get($request,'password_confirmation',2) ){
            return $this->register($request);
        }
        else{
            return response()->json(['message' => 'Passwords dont match'], 400);
        }
    }

    private function register(array $request){

        // Only used for dDb retrieval
        $this->user_model->email                            =   array_get($request,'email');
        $this->user_model->password                         =   password_hash(array_get($request,'password'),PASSWORD_DEFAULT);
        $this->user_model->{'contact.email'}                =   array_get($request,'email');
        $this->user_model->{'contact.name'}                 =   array_get($request,'name');
        $this->user_model->{'contact.nationality'}          =   array_get($request,'nationality');
        $this->user_model->{'contact.phone'}                =   array_get($request,'phone');
        $this->user_model->{'facebook_id'}                  =   array_get($request,'facebook_id');
        $this->user_model->{'google_id'}                    =   array_get($request,'google_id');
        $this->user_model->{'verification_token'}           =   array_get($request,'token');
        $this->user_model->{'settings.role_id'}             =   2;
        $this->user_model->{'settings.status'}              =   1;
        // Cuz each encryption is unique
        $encrypted_id                                       =   'zu-'.Crypt::encrypt(array_get($request,'email'));
        $this->user_model->_id                              =   $encrypted_id;

        if($this->user_model->save()){
            return $this->show($encrypted_id);
        }
    }

    public function show(...$vars){

        if(count($vars)==1){
            // Accessing for details
            $email      =   Crypt::decrypt(str_replace('zu-','',array_get($vars,'0')));
            $user       =   array_get($this->user_model->where('email',$email)->get()->toArray(),'0',false);
            if($user){
                $data   =   array_undot(drop_keys(['password','remember_token','verification_token','facebook_id','settings.facebook_id','google_id','settings.google_id'],$user));
                return response()->json($data);
            }
            else{
                return response()->json(['message' => 'These credentials do not match our records.'], 400);
            }

        }
        else{
            // Logging in
            $request    =   $this->payload;

            // Validate request
            $validate   =   $this   ->set_leaf($this::leaf)
                                    ->set_method('login')
                                    ->set_payload((object)json_decode(json_encode($request),false))
                                    ->validate_request();

            // Validation fail
            if( !is_bool( $validate) ){
                return response()->json( [ 'errors' => $validate ], 400 );
            }

            return $this->login(array_get($request,'email'),array_get($request,'password'));
        }
    }

    private function login($email, $password){

        // Get record
        $user   =   array_get($this->user_model->where('email',$email)->get()->toArray(),'0',false);

        if($user){
            if(password_verify( $password, array_get($user,'password'))){
                $encrypted_id   =   'zu-'.Crypt::encrypt($email);
                return $this->show($encrypted_id);
            }
            else{
                return response()->json(['message' => 'These credentials do not match our records.'], 400);
            }
        }
        else{
            return response()->json(['message' => 'These credentials do not match our records.'], 400);
        }
    }

    public function social_auth(){

        $request    =   $this->payload;

        $email          =   array_get($request,'email');
        $provider       =   array_get($request,'provider');
        $facebook_id    =   ( $provider == 'facebook' ) ? array_get($request,'provider_id') : '';
        $google_id      =   ( $provider == 'google' ) ? array_get($request,'provider_id') : '';

        $user           =   array_get($this->user_model->where('email',$email)->get()->toArray(),'0',false);

        if($user){
            // Update and Login
            $update['settings']['facebook_id']      =   !empty( $facebook_id ) ? $facebook_id : array_get($user,'facebook_id','');
            $update['settings']['google_id']        =   !empty( $google_id ) ? $google_id : array_get($user,'google_id','');
            $update['email']                        =   $email;

            $encrypted_id   =   'zu-'.Crypt::encrypt($email);
            return $this->show($encrypted_id);
        }
        else{
            // Register
            $password                           =   mt_rand();

            $request[ 'password' ]              =   $password;
            $request[ 'password_confirmation' ] =   $password;
            $request[ 'facebook_id' ]           =   $facebook_id;
            $request[ 'google_id' ]             =   $google_id;

            $request                            =   drop_keys(['provider','provider_id'],$request);

            return $this->register( $request );
        }
    }

    /**
    * Update the specified resource in storage.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function update(Request $request, ...$vars){

        $request    =   is_null($request) ? $this->payload : $request->all();

        $exists =   $this->show(...$vars);

        if($exists->status() == 200){

            $request    =   array_dot($request);
            $email      =   $this->decrypt_user(array_get($vars,'0',false));

            array_set($request, 'updated_at', date('Y-m-d\TH:i:s+00:00'));

            $update_items       =   $request;
            $update_expression  =   '';
            $attribute_names    =   [];
            $attribute_values   =   [];
            $primary_key        =   [];

            //Update Expression
            foreach($update_items as $key => $value){
                $update_expression  .=   '#'.str_replace('.','_',$key).'=:'.str_replace('.','_',$key).',';
            }
            if(!empty($update_expression)){
                $update_expression  =   rtrim($update_expression, ',');
            }
            else{
                return response()->json(['message' => 'Insufficient Input'],400);
            }

            //Expression Attribute Names to avoid reserved word error
            foreach($update_items as $key => $value){
                $attribute_names['#'.str_replace('.','_',$key)]  =  $key;
            }

            //Attribute Values
            foreach($update_items as $key => $value){
                $attribute_values[':'.str_replace('.','_',$key)]  =   DynamoDb::marshalValue($value);
            }

            $execute    =   DynamoDb::table('users-uae')
                                    ->setKey(DynamoDb::marshalItem([ 'email' => $email ]))
                                    ->setUpdateExpression('SET '.$update_expression)
                                    ->setExpressionAttributeValues($attribute_values)
                                    ->setExpressionAttributeNames($attribute_names)
                                    ->prepare()
                                    ->updateItem();

            if($execute){
                return response()->json([ 'message' => 'Profile updated successfully'], 200);
            }
            else{
                return response()->json([ 'message' => 'Profile cannot be updated'], 400);
            }
        }
        else{
            return $exists;
        }

    }

    public function change_password(Request $request, ...$vars){

        $user_id    =   array_get($vars,'0',false);
        $request    =   $request->all();
        $email      =   $this->decrypt_user($user_id);

        if( array_get($request,'password',1) === array_get($request,'password_confirmation',2) ){
            $user       =   array_get($this->user_model->where('email',$email)->get()->toArray(),'0',false);
            if($user){
                if(password_verify( array_get( $request,'old_password' ), array_get($user,'password'))){

                    $execute    =   DynamoDb::table('users-uae')
                                    ->setKey(DynamoDb::marshalItem(['email' => $email]))
                                    ->setUpdateExpression('SET password = :password')
                                    ->setExpressionAttributeValues([':password'=>DynamoDb::marshalValue(password_hash(array_get($request,'password'),PASSWORD_DEFAULT))])
                                    ->prepare()
                                    ->updateItem();
                    return response()->json(['message' => 'Password updated successfully'], 200);
                }
                else{
                    return response()->json(['message' => 'Incorrect old password.'], 400);
                }
            }
            else{
                return response()->json(['message' => 'These credentials do not match our records.'], 400);
            }
        }
        else{
            return response()->json(['message' => 'Passwords dont match'], 400);
        }
    }

    public function forgot_password(Request $request, ...$vars){

        $request    =   $request->all();
        $email      =   array_get($request,'email','');
        $user       =   array_get($this->user_model->where('email',$email)->get()->toArray(),'0',false);

        if($user){
            $execute    =   DynamoDb::table('users-uae')
                            ->setKey(DynamoDb::marshalItem(['email' => $email]))
                            ->setUpdateExpression('SET remember_token = :remember_token')
                            ->setExpressionAttributeValues([':remember_token'=>DynamoDb::marshalValue(array_get($request,'token',''))])
                            ->prepare()
                            ->updateItem();

            $user       =   array_get($this->user_model->where('email',$email)->get()->toArray(),'0',false);

            if(array_get($user,'remember_token','') != ''){
                return response()->json([
                    'message'   =>  'User token updated'
                ], 200 );
            }
            else{
                return response()->json(['message' => 'Request can not be served. Please try again'], 400);
            }
        }
        else{
            return response()->json(['message' => 'These credentials do not match our records.'], 400);
        }
    }

    public function reset_password(Request $request, ...$vars){

        $request    =   $request->all();
        $email      =   array_get($request,'email','');
        $token      =   array_get($request,'token','1');

        if( array_get($request,'password',1) === array_get($request,'password_confirmation',2) ){
            $user       =   array_get($this->user_model->where('email',$email)->where('remember_token',$token)->get()->toArray(),'0',false);

            if($user){
                if( $token == array_get($user,'remember_token','2') ){
                    // reset password
                    $reset      =   DynamoDb::table('users-uae')
                                    ->setKey(DynamoDb::marshalItem(['email' => $email]))
                                    ->setUpdateExpression('SET password = :password')
                                    ->setExpressionAttributeValues([':password'=>DynamoDb::marshalValue(password_hash(array_get($request,'password'),PASSWORD_DEFAULT))])
                                    ->prepare()
                                    ->updateItem();
                    // clear token
                    $clear      =   DynamoDb::table('users-uae')
                                    ->setKey(DynamoDb::marshalItem(['email' => $email]))
                                    ->setUpdateExpression('SET remember_token = :token')
                                    ->setExpressionAttributeValues([':token'=>DynamoDb::marshalValue('')])
                                    ->prepare()
                                    ->updateItem();

                    return response()->json(['message' => 'Password updated successfully'], 200);
                }
                else{
                    return response()->json(['message' => 'Request can not be served. Please try again'], 400);
                }
            }
            else{
                return response()->json(['message' => 'These credentials do not match our records.'], 400);
            }
        }
        else{
            return response()->json(['message' => 'Passwords do not match'], 400);
        }
    }

    public function verify_email(Request $request, ...$vars){

        $request    =   $request->all();
        $email      =   array_get($request,'email','');
        $token      =   array_get($request,'token','1');

        $user       =   array_get($this->user_model->where('email',$email)->get()->toArray(),'0',false);

        if($user){
            if($token == array_get($user,'verification_token')){

                $execute    =   DynamoDb::table('users-uae')
                                ->setKey(DynamoDb::marshalItem(['email' => $email]))
                                // ->setUpdateExpression('SET settings.verified = :verified, verification_token = :token')
                                // ->setExpressionAttributeValues([':verified'=>DynamoDb::marshalValue(1),':token'=>DynamoDb::marshalValue('')])
                                ->prepare()
                                ->updateItem();
                return response()->json(['message' => 'Account verified successfully'], 200);
            }
            else{
                return response()->json(['message' => 'Request can not be served. Please try again'], 400);
            }
        }
        else{
            return response()->json(['message' => 'These credentials do not match our records.'], 400);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(...$vars){
       //Code...
    }

    protected function decrypt_user($encrypted_id){
        return Crypt::decrypt(str_replace('zu-','',$encrypted_id));
    }

}
