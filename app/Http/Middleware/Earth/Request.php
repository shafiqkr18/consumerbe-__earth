<?php

namespace App\Http\Middleware\Earth;

use Closure;

class Request
{
    use \App\Traits\JsonValidator;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $uses       =   explode( "\\", strtolower($request->route()[1]['uses'] ));
        $leafs      =   [];

        //Include controller folder names in the $leaf
        $go         =   false;
        foreach($uses as $key => $value){
            if($go){
                $leafs[$key] = $value;
            }
            if($value == 'controllers'){
                $go     =   true;
            }
            if(str_contains($value,'@')){
                $go         =   false;
                $leafs[$key] =   explode( "@", $value)[0];
            }
        }

        $leaf       =   join($leafs,'.');

        $method     =   explode( "@", array_last($uses))[1];
        $payload    =   $request->has('query') ? (object)json_decode( $request->query('query') ) : (object)json_decode( json_encode( $request->all() ), 0);

        $this->set_leaf($leaf);
        $this->set_method($method);
        $this->set_payload($payload);

        $validate   =   $this->validate_request(storage_path().'/schemas/'.env('API_VERSION').'/request.json');

        if( is_bool( $validate ) ){
            return $next($request);
        }
        else{
            return response()->json( [ 'errors' => $validate ], 400 );
        }

    }

}
