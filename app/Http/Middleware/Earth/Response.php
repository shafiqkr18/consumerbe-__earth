<?php

namespace App\Http\Middleware\Earth;

use Closure;

class Response
{

    use \App\Traits\JsonValidator;
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle( $request, Closure $next ){

        /*
         * 1. Validate data on success
         * 2. Return response
         */

        $outgoing_response  =   $next( $request );
        $schema             =   $outgoing_response->headers->get('x-schema');

        # 1
        if( $outgoing_response->getStatusCode() === 200 ){
            $response       =   $outgoing_response->getData();
            $validate       =   $this->validate_json_response($response,$schema);
            if($validate && is_bool($validate)){
                return response()->json( $response, $outgoing_response->getStatusCode() );
            }
            else{
                return response( $validate, 401 );
            }
        }
        else{
            return $outgoing_response;
        }
    }

}
