<?php

return [

    // Used to identify the leaf of the resource
    // This is different for different models
    'id'            =>  'favourite_agency',
    'connection'    =>  [
        'index'         =>  'favourite_agency',
        'keys'            =>  [
          'partition'       =>  'consumer.email',
          'sort'            =>  '_id'
        ],
        'mapping'       =>  [
            '$favourite'    =>  [$favourite=array_merge( array_get( load('earth/ddb/mapping/favourite_agency'), 'favourite_agency' ), array_get( load('earth/ddb/mapping/favourite_agency'), 'favourite_agency_search' ) )],
            'fields'        =>  array_merge( load('earth/ddb/mapping/primary'), $favourite )
        ]
    ]
];

?>
