<?php

return [

    // Used to identify the leaf of the resource
    // This is different for different models
    'id'            =>  'favourite_agent',
    'connection'    =>  [
        'index'         =>  'favourite_agent',
        'keys'            =>  [
          'partition'       =>  'consumer.email',
          'sort'            =>  '_id'
        ],
        'mapping'       =>  [
            '$favourite'    =>  [$favourite=array_merge( array_get( load('earth/ddb/mapping/favourite_agent'), 'favourite_agent' ), array_get( load('earth/ddb/mapping/favourite_agent'), 'favourite_agent_search' ) )],
            'fields'        =>  array_merge( load('earth/ddb/mapping/primary'), $favourite )
        ]
    ]
];

?>
