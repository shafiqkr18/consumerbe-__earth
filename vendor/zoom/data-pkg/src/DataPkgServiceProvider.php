<?php

namespace DataPKG;

use Illuminate\Support\ServiceProvider;

class DataPkgServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
      // Publishing config files
      $this->publishes([
          __DIR__.'/config/earth/'        => config_path('earth')
      ], 'config');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->make('DataPKG\Controllers\DataController');

    }
}
