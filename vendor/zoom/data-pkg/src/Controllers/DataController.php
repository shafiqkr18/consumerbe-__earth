<?php

namespace DataPKG\Controllers;

use App\Http\Controllers\Controller;
use DataPKG\Traits\ValidationTrait;

class DataController extends Controller
{
    use ValidationTrait;

    protected $index;
    protected $config;
    protected $credentials;
    protected $connect_flag;

    public function __construct(){

        return $this;

    }

    /*
    |--------------------------------------------------------------------------
    | CONNECT functions
    |--------------------------------------------------------------------------
    |
    |
    */

    public function connect(){

        if( $this->null_check( $this->credentials ) ){

            if( isset( $this->credentials[ 'key' ] ) AND $this->null_check( $this->credentials['key'] ) ){

                if( isset( $this->credentials[ 'secret' ] ) AND $this->null_check( $this->credentials['secret'] ) ){

                    if( isset( $this->credentials[ 'region' ] ) AND $this->null_check( $this->credentials['region'] ) ){

                        // Create DDB Client
                        $clientBuilder    =   new \Aws\Sdk([
                            'region'        =>  $this->credentials[ 'region' ],
                            'version'       =>  'latest',
                            'credentials'   =>  [
                                'key'       =>  $this->credentials[ 'key' ],
                                'secret'    =>  $this->credentials[ 'secret' ]
                            ]
                        ]);

                        $this->client = $clientBuilder->createDynamoDb();

                        $this->connect_flag      =   true;

                        return $this;

                    }
                    else{

                        return response()->json( array( 'messaging' => 'AWS-ES Credentials-region not set' ), 403 );

                    }
                }
                else{

                    return response()->json( array( 'messaging' => 'AWS-ES Credentials-secret not set' ), 403 );

                }
            }
            else{

                return response()->json( array( 'messaging' => 'AWS-ES Credentials-key not set' ), 403 );

            }

        }
        else{

            return response()->json( array( 'messaging' => 'AWS-ES Credentials-key, Credentials-secret and Credentials-region not set' ), 403 );

        }


    }

    /**
    * Check if user has called the connect method
    * @return [type] [description]
    */
    protected function connect_check(){

        if( $this->connect_flag ){

            return true;

        }
        else{

            return response()->json( array( 'messaging' => 'Call the connect() method first' ), 403 );

        }

    }

    /**
    * Check if user has set search for
    */
    protected function set_search_index_check(){

        if( !is_null( $this->index ) ){

            return true;

        }
        else{

            return response()->json( array( 'messaging' => 'Call the set_search_for() method first' ), 403 );

        }

    }

}
