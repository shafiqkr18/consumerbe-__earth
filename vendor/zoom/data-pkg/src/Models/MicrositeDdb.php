<?php

namespace DataPKG\Models;

use BaoPham\DynamoDb\DynamoDbModel;

class MicrositeDdb extends DynamoDbModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table        =   'microsites';

    protected $primaryKey   =   'developer._id';

    protected $fillable     =   [
        'developer',
        'projects',
        'titles',
        'titles_ar',
        'subtitles',
        'subtitles_ar',
        'localization',
        'domain',
        'active',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
