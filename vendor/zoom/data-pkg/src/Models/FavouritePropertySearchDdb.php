<?php

namespace DataPKG\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use BaoPham\DynamoDb\DynamoDbModel;

class FavouritePropertySearchDdb extends DynamoDbModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table        =   'favourites-uae';
    protected $primaryKey   =   'email';

    protected $fillable = [
        'property._id',
        'property.ref_no',
        'property.permit_no',
        'property.web_link',
        'property.building',
        'property.city',
        'property.area',
        'property.coordinates',
        'property.bedroom',
        'property.bathroom',
        'property.dimension.builtup_area',
        'property.dimension.plot_size',
        'property.furnished',
        'property.amenities',
        'property.rent_buy',
        'property.residential_commercial',
        'property.type',
        'property.price',
        'property.settings.verified',
        'property.settings.organic_score',
        'property.settings.chat',
        'property.settings.status',
        'property.settings.is_manual',
        'property.settings.boost.status',
        'property.settings.boost.from',
        'property.settings.boost.to',
        'property.featured.status',
        'property.featured.from',
        'property.featured.to',
        'property.agent._id',
        'property.agent.contact.areas',
        'property.agent.contact.languages',
        'property.agent.contact.name',
        'property.agent.contact.email',
        'property.agent.contact.phone',
        'property.agent.contact.nationality',
        'property.agent.contact.gender',
        'property.agent.contact.license_no',
        'property.agent.settings.organic_score',
        'property.agent.settings.status',
        'property.agent.settings.verified',
        'property.agent.settings.chat',
        'property.agent.agency._id',
        'property.agent.agency.contact.name',
        'property.agent.agency.contact.email',
        'property.agent.agency.contact.phone',
        'property.agent.agency.contact.orn',
        'property.agent.agency.settings.status',
        'property.agent.agency.settings.chat',
        'property.agent.agency.settings.lead.admin.email',
        'property.agent.agency.settings.lead.admin.receive_emails',
        'property.agent.agency.settings.lead.admin.cc',
        'property.agent.agency.settings.lead.agent.receive_emails',
        'property.agent.agency.featured.status',
        'property.agent.agency.featured.from',
        'property.agent.agency.featured.to',
        'property.agent.timestamp.created',
        'property.agent.timestamp.updated',
        'property.agent.featured.status',
        'property.agent.featured.from',
        'property.agent.featured.to',
        'property.timestamp.created',
        'property.timestamp.updated',
        'property.cheques',
        'property.rental_frequency',
        'property.exclusive',
        'property.rooms',
        'property.off_plan',
        'property.primary_view',
        'property.completion_status',
        'property.size',
        'property.page',
        'property.sort',
        // Primary Key
        'email',
        // Sort key: Serialized Hash
        '_id',
        'type',
        // Secondary index : model, search
        'subtype'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'email',
    ];
}
