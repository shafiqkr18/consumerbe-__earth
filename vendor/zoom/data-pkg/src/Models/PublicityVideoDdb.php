<?php

namespace DataPKG\Models;

use BaoPham\DynamoDb\DynamoDbModel;

class PublicityVideoDdb extends DynamoDbModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table        =   'publicity-video';
    protected $primaryKey   =   '_id';

    protected $fillable     =   [
        '_id',
        'title',
        'thumbnail',
        'youtube_id',
        'pv_domain',
        'active'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
