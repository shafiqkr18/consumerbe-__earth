<?php

namespace DataPKG\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use BaoPham\DynamoDb\DynamoDbModel;

class AlertsDdb extends DynamoDbModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table        =   'alerts-staging';
    protected $primaryKey   =   'frequency';

    protected $fillable = [

        'consumer.email',
        'consumer.name',
        'data.rent_buy',
        'data.suburb',
        'data.price.min',
        'data.price.max',
        'type',
        'subtype',

        // Primary Key
        'frequency',
        // Sort key: Serialized hash
        'alert._id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
