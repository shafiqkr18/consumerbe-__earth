<?php

namespace DataPKG\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use BaoPham\DynamoDb\DynamoDbModel;

class AgencyPerformanceAlertsDdb extends DynamoDbModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table        =   'agency-performance-alerts';
    protected $primaryKey   =   '_id';

    protected $fillable = [
        'agency_id',
        'agency_email',
        'agency_name',
        'target_lead_count',
        'final_lead_count',
        'alert_frequency',
        'alert_type', // setting or report
        'alert_status',
        '_id' // Primary Key
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
