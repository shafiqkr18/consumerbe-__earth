<?php

namespace DataPKG\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use BaoPham\DynamoDb\DynamoDbModel;

class UserDdb extends DynamoDbModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table        =   'users-uae';
    protected $primaryKey   =   'email';

    protected $fillable = [
        'contact.name',
        'contact.email',
        'contact.nationality',
        'contact.phone',
        'contact.avatar',
        'settings.newsletter',
        'settings.status',
        'settings.verified',
        'settings.role_id',
        'settings.reengage',
        'password',
        'remember_token',
        'verification_token',
        'create_at',
        'update_at',
        'last_logged_in_at',
        'last_logged_out_at',
        'fcm_token',
        'apn_token',
        'facebook_id',
        'google_id',
        '_id',
        // Only used for  dDb retrieval
        'email'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'email'
    ];

}
