<?php

namespace DataPKG\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use BaoPham\DynamoDb\DynamoDbModel;

class SeoDdb extends DynamoDbModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table        =   'seo-contents';
    protected $primaryKey   =   '_id';

    protected $fillable     =   [
        '_id',
        'seo_label',
        'page_name',
        'seo_title',
        'seo_title_ar',
        'seo_description',
        'seo_description_ar',
        'seo_h1',
        'seo_h1_ar',
        'seo_h2',
        'seo_h2_ar',
        'seo_h3',
        'seo_h3_ar',
        'seo_image_alt',
        'seo_image_alt_ar',
        'project._id',
        'project.name',
        'project.developer._id',
        'project.developer.name',
        'seo_parameters',
        'seo_parameters_additional',
        'seo_parameters_id',
        'seo_domain',
        'active'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
