<?php

namespace DataPKG\Models;

use BaoPham\DynamoDb\DynamoDbModel;

class LandingBannerDdb extends DynamoDbModel
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table        =   'landing-banner';
    protected $primaryKey   =   '_id';

    protected $fillable     =   [
        '_id',
        'active',
        'web_link',
        'agency_id',
        'agency_name',
        'project_id',
        'project_name',
        'image_domain',
        'image_ad',
        'image_desktop',
        'image_mobile'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
