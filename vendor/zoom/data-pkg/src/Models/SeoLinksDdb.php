<?php

namespace DataPKG\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use BaoPham\DynamoDb\DynamoDbModel;

class SeoLinksDdb extends DynamoDbModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table        =   'seo-links';

    protected $primaryKey   =   'model';
    protected $compositeKey =   ['model', '_id'];

    protected $fillable     =   [
        '_id',
        'label',
        'model',
        'page_name',

        'links',
        'links_status',
        'links_keywords',
        'names',
        'names_ar',
        'titles',
        'titles_ar',

        'property.rent_buy',
        'property.residential_commercial',
        'property.type',
        'property.area',
        'property.city',
        'property.building',
        'property.price.min',
        'property.price.max',
        'property.bedroom.min',
        'property.bedroom.max',
        'property.keyword',

        'seo_links_id',
        'domain',
        'active'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
