<?php

namespace DataPKG\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use BaoPham\DynamoDb\DynamoDbModel;

class SeoArticlesDdb extends DynamoDbModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table        =   'seo-articles';

    protected $primaryKey   =   'model';
    protected $compositeKey =   ['model', '_id'];

    protected $fillable     =   [
        '_id',
        'model',
        'page_name',
        'titles',
        'descriptions',
        'titles_ar',
        'descriptions_ar',
        'images',
        'images_alt',
        'images_alt_ar',
        'domain',
        'active',

        'property.rent_buy',
        'property.residential_commercial',
        'property.type',
        'property.area',
        'property.city',
        'property.building',
        'property.price.min',
        'property.price.max',
        'property.bedroom.min',
        'property.bedroom.max',
        'property.bathroom.min',
        'property.bathroom.max',
        'property.dimension.builtup_area.min',
        'property.dimension.builtup_area.max',
        'property.keyword',
        'property.furnished',

        'seo_article_id',
        'seo_title',
        'seo_title_ar',
        'seo_description',
        'seo_description_ar',
        'seo_h1',
        'seo_h1_ar',
        'seo_h2',
        'seo_h2_ar',
        'seo_h3',
        'seo_h3_ar',
        'seo_image_alt',
        'seo_image_alt_ar'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
