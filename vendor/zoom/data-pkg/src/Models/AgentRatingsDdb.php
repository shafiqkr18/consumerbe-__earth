<?php

namespace DataPKG\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use BaoPham\DynamoDb\DynamoDbModel;

class AgentRatingsDdb extends DynamoDbModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table        =   'agent-ratings';
    protected $primaryKey   =   'agent_id';
    protected $compositeKey =   ['agent_id', '_id'];

    protected $fillable     =   [
        'agent_id', // Partition Key
        'user_id',
        'user_name',
        'user_picture',
        'ra_comment',
        'rate',
        'active',
        'created_at',
        'updated_at',
        '_id' // Sort Key
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
