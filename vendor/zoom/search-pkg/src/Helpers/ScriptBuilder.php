<?php

namespace SearchPKG\Helpers;

class ScriptBuilder{

    private $script;

    public function __construct(){
        $this->script   =   null;
        $this->params   =   null;
        return $this;

    }

    public function build( $script_array, $prefix = '' ){
        if(!empty($script_array)){
            $field = !empty($prefix) ? $prefix. '.' .key($script_array[$prefix]) : key($script_array);
            $this->script =  'ctx._source.' .$field . ' = params.' . $field;
            $this->params =  $script_array;
        }

        return $this;

    }

    public function getScript(){

        $return         =   [];
        $return[ 'inline' ]     =   $this->script;

        $return[ 'lang' ]       =   'painless';

        $return[ 'params' ]     =   $this->params;

        return $return;

    }

}
