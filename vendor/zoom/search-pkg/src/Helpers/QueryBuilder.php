<?php
namespace SearchPKG\Helpers;

class QueryBuilder{
    private $query;
    private $request;
    private $index;
    private $sort;
    private $sortQuery;

    public function __construct( $queryRequestValidation, $sort = false, $index = null ){
        $this->request      =   $queryRequestValidation;
        $this->query        =   [];
        $this->sortQuery    =   [];
        $this->index        =   $index;
        $this->sort         =   $sort;
        return $this;
    }

    public function build( $queryArray ){
        $this->query['must'][]      =   $this->addCondition( array_dot( $queryArray ) );
        $excludeArray               =   array_get($queryArray,'exclude',[]);
        if ( !empty($excludeArray) ){
            $this->query['must_not'][]    =   $this->addCondition( array_dot( $excludeArray ) );
        }
        if ( $this->sort !== null ){
            foreach( $this->sort as $column => $condition ){
                $this->sortQuery[]       =   $this->addSort( $column, $condition );
            }
        }
        return $this;
    }

    public function addCondition( $dotted_req ){
        $eachquery              =   [];
        $dotted_schema          =   array_dot($this->request);
        if(array_has($dotted_req,'keyword')){
            $eachquery['keyword']   =   array_get($dotted_req,'keyword');
            array_forget($dotted_req,'keyword');
        }

        foreach( $dotted_req as $k => $v ){
            // identify and group min, max range or from to range
            $suffix = substr(strrchr($k, "."), 1);
            if( $suffix == 'min' || $suffix == 'max' ){
                if( preg_match('/(.*?).'.$suffix.'/', $k, $match) == 1 ){
                    $dotted_request[$match[1]][$suffix]   =  $v;
                }
            }
            else if( $suffix == 'from' || $suffix == 'to' ){
                if( preg_match('/(.*?).'.$suffix.'/', $k, $match) == 1 ){
                    $dotted_request[$match[1]][$suffix]   =  $v;
                }
            }
            else{
                // identify and group multi_select array
                if( preg_match( "/.\d{1,2}/", $k, $match ) == 1 ){
                    $dotted_request[ preg_replace( "/.\d{1,2}/", "", $k ) ][]   =  $v;
                    array_forget( $dotted_req, $k );
                }
                else{
                    $dotted_request[ $k ]   =  $v;
                }
            }
        }
        // dd($dotted_request);
        if(!empty($dotted_request)){
            foreach( $dotted_request as $k => $v ){
                $dotted_k = str_replace(".",".properties.",$k);
                $dotted_r[$dotted_k] = $v;
            }
        }
        // dd($dotted_r,$dotted_schema);
        if(!empty($dotted_r)){
            foreach( $dotted_r as $column => $condition ){
                if( array_has( $dotted_schema, $column.'.type' ) ){
                    $queryBuilderCondition  =   array_get( $dotted_schema, $column.'.type' );
                    $column                 =   str_replace( ".properties", "", $column );
                    if( $queryBuilderCondition === "text" || $queryBuilderCondition === "keyword" ){
                        if( str_contains( $column, "," ) ){
                            $column     =   str_replace( ",", "? OR ", $column );
                            $column     .=  "?";
                            $replaceWith    =   [];
                            if( is_array( $condition ) ){
                                $subQuery               =   [];
                                foreach( $condition as $eachCondition ){
                                    $subQuery[]         =   '"'. $eachCondition .'"';
                                }
                                for( $i=0; $i<substr_count( $column, "?" ); $i++ ){
                                    $replaceWith[]  =   ':(' . implode( " OR ", $subQuery ) .')';
                                }
                            }
                            else{
                                for( $i=0; $i<substr_count( $column, "?" ); $i++ ){
                                    $replaceWith[]  =   ':(' . implode( " OR ", $subQuery ) .')';
                                }
                            }
                            $eachquery[][ 'query' ]       =   str_replace_array( '?', $replaceWith, $column );
                        }
                        else{
                            if( is_array( $condition ) ){
                                $subQuery               =   [];
                                if( ends_with($column, 'country') || $column == 'area' || $column == 'building' ){
                                    foreach( $condition as $eachCondition ){
                                        $eachCondition      =   str_replace( " ", '*', $eachCondition );
                                        $subQuery[]         =   $eachCondition .'*';
                                    }
                                }
                                else{
                                    foreach( $condition as $eachCondition ){
                                        $subQuery[]         =   '"'. $eachCondition .'"';
                                    }
                                }
                                $eachquery[][ 'query' ]       =   $column.':(' . implode( " OR ", $subQuery ) .')';
                            }
                            else{
                                if( ends_with($column, 'country') || $column == 'area' || $column == 'building' ){
                                    $condition          =   str_replace( " ", '*', $condition );
                                    $eachquery[][ 'query' ]   =  $column.':('.$condition.'*)';
                                }
                                else{
                                    $eachquery[][ 'query' ]   =  $column.':("'.$condition.'")';
                                }
                            }
                        }
                    }
                    else if( ( $queryBuilderCondition === "byte" ) || ( $queryBuilderCondition === "integer" ) ){
                        if( !is_array( $condition ) ){
                            // example: role_id field
                            $eachquery[][ 'query' ]   =   $column.':'.$condition;
                        }
                        else{
                            $min    =   isset( $condition[ 'min' ] ) ? (int) $condition[ 'min' ] : "*";
                            $max    =   isset( $condition[ 'max' ] ) ? (int) $condition[ 'max' ] : "*";
                            if( $queryBuilderCondition === "byte" ){
                                if( $min < -127 || $min > 127 ){
                                    die( "The ".implode( ",",$condition ) . " values are beyond the range of -127 to 127" );
                                }
                            }
                            // R-A-N-G-E
                            $eachquery[][ 'query' ]   =   "$column:[$min TO $max]";
                        }
                    }
                    else if( ( $queryBuilderCondition === "float" )){
                        if( !is_array( $condition ) ){
                            // example: role_id field
                            $eachquery[][ 'query' ]   =   $column.':'.$condition;
                        }
                        else{
                            $min    =   isset( $condition[ 'min' ] ) ? (float) $condition[ 'min' ] : "*";
                            $max    =   isset( $condition[ 'max' ] ) ? (float) $condition[ 'max' ] : "*";
                            // R-A-N-G-E
                            $eachquery[][ 'query' ]   =   "$column:[$min TO $max]";
                        }
                    }
                    else if( $queryBuilderCondition === "geo_point" ){
                        if( !is_array( $condition ) ){
                            die( "$condition needs to be an array, Range queries will be fired across this column. Possible inputs are [1,*] or [*,9]" );
                        }
                        die( 'query for geo_point' );
                    }
                    else if( $queryBuilderCondition === "boolean" ){
                        if( !is_bool( $condition ) ){
                            die( "$condition needs to be a bool. Possible inputs are [1,*] or [*,9]" );
                        }
                        else{
                            $condition                  =   ($condition) ? 'true' : 'false';
                            $eachquery[][ 'query' ]     =   $column.':'.$condition;
                        }
                    }
                    else if( $queryBuilderCondition === "date" ){
                        // R-A-N-G-E
                        if(is_array($condition)){
                            $from = array_get($condition, 'from', '2018-05-03');
                            $to = array_get($condition, 'to', 'now');
                            $eachquery[][ 'query' ]   =   "$column:[$from TO $to]";
                        }
                        else{
                            $eachquery[][ 'query' ]   =   "$column:[$condition]";
                        }
                    }
                    else if( $queryBuilderCondition === "array" ){
                        $subQuery               =   [];
                        foreach( $condition as $eachCondition ){
                            $subQuery[]         =   '"'. $eachCondition .'"';
                        }
                        $eachquery[][ 'query' ]       =   $column.':(' . implode( " OR ", $subQuery ) .')';
                    }
                    else{
                        die( $queryBuilderCondition . ' is not a validated column' );
                    }
                }
                else{
                    // die( $column . ' is not searchable' );
                }
            }
        }
        if( !empty( $eachquery ) )
            return $eachquery;
    }

    public function addSort( $column, $condition ){
        /**
        *
        *      1.  Check if inputs exist in queryRequestValidator
        *      2.  Imply sort values as approp.
        *
        */
        //1.
        if( isset( $this->sort[ $column ] ) ){
            $eachsort                =   [];
            $eachsort[ str_replace( '.order', '', $column ) ]    =   ( $condition == 'asc' || $condition == 'desc' ) ? [ 'order' => $condition ] : [ 'mode' => $condition ];
            return $eachsort;
        }
    }

    public function getQuery( $query_count ){
        $return         =   [];
        $finalReturn    =   [];

        $this->query    =   array_filter($this->query);

        for( $iterate = 0; $iterate < $query_count; $iterate++ ) {
            $must           =   [];
            $mustNot        =   [];
            $mustQuery      =   [];
            $mustNotQuery   =   [];

            /**
             *  Search
             */
            if(!empty($this->query[ 'must' ])){
                $mustQuery[$iterate] =   $this->query[ 'must' ][$iterate];
                if( !empty( $mustQuery ) ){
                    foreach( $mustQuery as $query ){
                        if(array_has($query,'keyword')){
                            $keyword   =   array_get($query,'keyword');
                            array_forget($query,'keyword');
                        }
                        if(!empty($query)){
                            foreach($query as $each_query){
                                $must[][ 'query_string' ]           =   $each_query;
                            }
                        }
                    }
                }
            }

            /**
             *  Keyword Search
             */
            if( !empty( $keyword ) ){
                // Get all fields keyword to be searched in
                // Allowing all keyword and text type of fields per index
                $searchKey      =   'type';
                $values         =   array('keyword','text');
                $searchValues   =   array_combine($values, $values);
                $fields         =   array_keys(array_dot($this->get_fields($searchKey,$searchValues)));

                $keyword_query[ 'query' ]   =   $keyword;
                $keyword_query[ 'type' ]    =   'best_fields';
                $keyword_query[ 'fields' ]  =   $fields;
                $must[][ 'multi_match' ]    =   $keyword_query;
            }

            $return[ 'query' ][ 'bool' ][ 'should' ][ $iterate ][ 'bool' ][ 'must' ]  =   $must;

            /**
             * Exclude Query
             */
            if(!empty($this->query[ 'must_not' ])){
                $mustNotQuery[$iterate] =   $this->query[ 'must_not' ][$iterate];
                if( !empty( $mustNotQuery ) ){
                    foreach( $mustNotQuery as $excludes ){
                        if( !empty( $excludes ) ){
                            foreach($excludes as $exclude){
                                $mustNot[][ 'query_string' ]            =   $exclude;
                            }
                        }
                    }
                    $return[ 'query' ][ 'bool' ][ 'should' ][ $iterate ][ 'bool' ][ 'must_not' ]  =   $mustNot;
                }
            }

            if( !$this->sort && empty( $keyword ) ){
                if( $this->index === 'property' ){
                    $boost_agency_properties     =   array_get(load( 'earth/els/boost_clients' ),'property.agency',[]);
                    if( !empty( $boost_agency_properties ) ){
                        $terms      =   [];
                        foreach( $boost_agency_properties as $key => $agency ){
                            $terms[ $key ][ 'term' ][ 'agent.agency._id' ][ 'value' ]   =   $agency;
                            $terms[ $key ][ 'term' ][ 'agent.agency._id' ][ 'boost' ]   =   50;
                        }
                        $return[ 'query' ][ 'bool' ][ 'should' ][ $iterate ][ 'bool' ][ 'should' ]    =   $terms;
                    }
                    $finalReturn[ 'query' ][ 'function_score' ]                                                               =  $return;
                    $finalReturn[ 'query' ][ 'function_score' ][ 'boost_mode' ]                                               =  "sum";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'score_mode' ]                                               =  "sum";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][0][ 'field_value_factor' ][ 'field' ]          =  "settings.boost.status";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][0][ 'field_value_factor' ][ 'modifier' ]       =  "log2p";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][0][ 'field_value_factor' ][ 'missing' ]        =  0;
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][1][ 'field_value_factor' ][ 'field' ]          =  "settings.organic_score";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][1][ 'field_value_factor' ][ 'modifier' ]       =  "square";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][1][ 'field_value_factor' ][ 'missing' ]        =  0;
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][2][ 'field_value_factor' ][ 'field' ]          =  "settings.verified";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][2][ 'field_value_factor' ][ 'modifier' ]       =  "square";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][2][ 'field_value_factor' ][ 'missing' ]        =  0;
                    // Randomization
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][3][ 'random_score' ]         =   new \stdClass;
                }
                elseif( $this->index === 'agent' ){
                    $finalReturn[ 'query' ][ 'function_score' ]                                                               =  $return;
                    $finalReturn[ 'query' ][ 'function_score' ][ 'boost_mode' ]                                               =  "sum";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'score_mode' ]                                               =  "sum";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][0][ 'field_value_factor' ][ 'field' ]          =  "settings.organic_score";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][0][ 'field_value_factor' ][ 'modifier' ]       =  "square";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][0][ 'field_value_factor' ][ 'missing' ]        =  0;
                    // Randomization
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][1][ 'random_score' ]                           =   new \stdClass;
                }
                elseif( $this->index === 'project' ){
                    $boost_developer_projects     =   array_get(load( 'earth/els/boost_clients' ),'project.developer',[]);
                    if( !empty( $boost_developer_projects ) ){
                        $terms      =   [];
                        foreach( $boost_developer_projects as $key => $developer ){
                            $terms[ $key ][ 'term' ][ 'developer._id' ][ 'value' ]   =   $developer;
                            $terms[ $key ][ 'term' ][ 'developer._id' ][ 'boost' ]   =   50;
                        }
                        $return[ 'query' ][ 'bool' ][ 'should' ][ $iterate ][ 'bool' ][ 'should' ]    =   $terms;
                    }
                    $finalReturn[ 'query' ][ 'function_score' ]                                                             =   $return;
                    $finalReturn[ 'query' ][ 'function_score' ][ 'boost_mode' ]                                             =   "sum";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'score_mode' ]                                             =   "sum";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][0][ 'field_value_factor' ][ 'field' ]        =   "settings.boost.status";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][0][ 'field_value_factor' ][ 'modifier' ]     =   "log2p";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][0][ 'field_value_factor' ][ 'missing' ]      =   0;
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][1][ 'field_value_factor' ][ 'field' ]        =   "settings.organic_score";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][1][ 'field_value_factor' ][ 'modifier' ]     =   "square";
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][1][ 'field_value_factor' ][ 'missing' ]      =   0;
                    // Randomization
                    $finalReturn[ 'query' ][ 'function_score' ][ 'functions' ][2][ 'random_score' ]                         =   new \stdClass;
                }
                else{
                    $finalReturn[ 'query' ][ 'function_score' ]                                                             =   $return;
                }
            }
            else{
                $finalReturn    =   $return;
            }
        }

        if( !empty( $this->sortQuery ) ){
            $finalReturn[ 'sort' ]      =   $this->sortQuery;
        }

        return $finalReturn;
    }

    public function get_fields($searchKey,$searchValues=[]){
        $config         =   array_get( load(env( 'API_VERSION' ) . '/els/' . env( 'ELS_ENVIRONMENT' ) . '/' . $this->index), 'connection.mapping' );
        $dotted_config  =   array_dot( $config );
        $fields         =   [];

        foreach($dotted_config as $key => $value){
            if(str_contains($key,'.')){
                $key    =   str_replace('.properties.','.',$key);
            }
            if(!str_contains($key,'settings')){
                if(ends_with($key,$searchKey) && isset($searchValues[$value])){
                    $key_ = str_replace(['.properties.','.'.$searchKey],['.',''], $key);
                    array_set( $fields, $key_, $value );
                }
            }
        }
        return $fields;
    }

}
