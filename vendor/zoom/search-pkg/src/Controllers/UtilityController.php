<?php

namespace SearchPKG\Controllers;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\Http\Controllers\Controller;
use SearchPKG\Traits\ConnectionTrait;

class UtilityController extends BaseController
{
    use ConnectionTrait;

    protected $client;

    /*
    |--------------------------------------------------------------------------
    | AWS ELS functions for UtilityController
    |--------------------------------------------------------------------------
    */

    public function get_locations_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'property';
        $query[ 'type' ]                            =   'property';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'terms' ][ 'field' ]  =   'city';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'terms' ][ 'size' ]   =   500;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ]['city']['top_hits']['_source']   =   'city';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ]['city']['top_hits']['size']      =   1;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'terms' ][ 'field' ]  =   'area';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'terms' ][ 'size' ]   =   1000;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ]['aggs' ]['area']['top_hits']['_source']   =   'area';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ]['aggs' ]['area']['top_hits']['size']      =   1;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'buildings' ][ 'terms' ][ 'field' ]  =   'building';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'buildings' ][ 'terms' ][ 'size' ]   =   1000;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'buildings' ]['aggs' ]['building']['top_hits']['_source']   =   'building';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'buildings' ]['aggs' ]['building']['top_hits']['size']      =   1;

        return $this->client->search( $query );

    }

    public function get_location_coordinates_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'property';
        $query[ 'type' ]                            =   'property';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'terms' ][ 'field' ]  =   'city';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'terms' ][ 'size' ]   =   500;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ]['city']['top_hits']['_source']   =   'city';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ]['city']['top_hits']['size']      =   1;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'terms' ][ 'field' ]  =   'area';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'terms' ][ 'size' ]   =   1000;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ]['aggs' ]['area']['top_hits']['_source']   =   'area';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ]['aggs' ]['area']['top_hits']['size']      =   1;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'lat' ][ 'terms' ][ 'field' ]  =   'coordinates.lat';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'lat' ][ 'terms' ][ 'size' ]   =   1000;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'lat' ]['aggs' ]['latitude']['top_hits']['_source']   =   'coordinates.lat';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'lat' ]['aggs' ]['latitude']['top_hits']['size']      =   1;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'lat' ][ 'aggs' ][ 'lng' ][ 'terms' ][ 'field' ]  =   'coordinates.lng';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'lat' ][ 'aggs' ][ 'lng' ][ 'terms' ][ 'size' ]   =   1000;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'lat' ][ 'aggs' ][ 'lng' ][ 'aggs' ][ 'longitude' ]['top_hits']['_source']   =   'coordinates.lng';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'lat' ][ 'aggs' ][ 'lng' ][ 'aggs' ][ 'longitude' ]['top_hits']['size']      =   1;

        return $this->client->search( $query );

    }

    public function get_property_types_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'property';
        $query[ 'type' ]                            =   'property';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'terms' ][ 'field' ]  =   'residential_commercial';
        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'terms' ][ 'size' ]   =   2;

        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'terms' ][ 'field' ]  =   'rent_buy';
        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'terms' ][ 'size' ]   =   4;

        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'aggs' ][ 'type' ][ 'terms' ][ 'field' ]  =   'type';
        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'aggs' ][ 'type' ][ 'terms' ][ 'size' ]   =   100;

        return $this->client->search( $query );

    }

    public function get_city_area_building_type_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'property';
        $query[ 'type' ]                            =   'property';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'terms' ][ 'field' ]  =   'residential_commercial';
        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'terms' ][ 'size' ]   =   2;

        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'terms' ][ 'field' ]  =   'rent_buy';
        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'terms' ][ 'size' ]   =   4;

        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'aggs' ][ 'cities' ][ 'terms' ][ 'field' ]  =   'city';
        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'aggs' ][ 'cities' ][ 'aggs' ]['city']['top_hits']['_source']   =   'city';
        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'aggs' ][ 'cities' ][ 'aggs' ]['city']['top_hits']['size']      =   1;

        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'aggs' ][ 'cities' ][ 'aggs' ][ 'areas' ][ 'terms' ][ 'field' ]    =   'area';
        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'aggs' ][ 'cities' ][ 'aggs' ][ 'areas' ][ 'terms' ][ 'size' ]     =   100;
        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'aggs' ][ 'cities' ][ 'aggs' ][ 'areas' ][ 'aggs' ]['area']['top_hits']['_source']  =   'area';
        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'aggs' ][ 'cities' ][ 'aggs' ][ 'areas' ][ 'aggs' ]['area']['top_hits']['size']     =   1;

        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'aggs' ][ 'cities' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'buildings' ][ 'terms' ][ 'field' ]    =   'building';
        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'aggs' ][ 'cities' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'buildings' ][ 'terms' ][ 'size' ]     =   250;
        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'aggs' ][ 'cities' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'buildings' ][ 'aggs' ]['building']['top_hits']['_source']     =   'building';
        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'aggs' ][ 'cities' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'buildings' ][ 'aggs' ]['building']['top_hits']['size']        =   1;

        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'aggs' ][ 'cities' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'buildings' ][ 'aggs' ][ 'types' ][ 'terms' ][ 'field' ]    =   'type';
        $query[ 'body' ][ 'aggs' ][ 'residential_commercial' ][ 'aggs' ][ 'rent_buy' ][ 'aggs' ][ 'cities' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'buildings' ][ 'aggs' ][ 'types' ][ 'terms' ][ 'size' ]     =   20;

        // die( json_encode( $query ) );
        return $this->client->search( $query );

    }

    public function get_broker_areas_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'agent';
        $query[ 'type' ]                            =   'agent';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 1 ][ 'exists' ][ 'field' ]                 =   'contact.areas';

        $query[ 'body' ][ 'aggs' ][ 'agency' ][ 'terms' ][ 'field' ]  =   'agency.contact.name';
        $query[ 'body' ][ 'aggs' ][ 'agency' ][ 'terms' ][ 'size' ]   =   1000;

        $query[ 'body' ][ 'aggs' ][ 'agency' ][ 'aggs' ]['areas']['top_hits']['_source']   =   ['agency.contact.name','contact.areas'];
        $query[ 'body' ][ 'aggs' ][ 'agency' ][ 'aggs' ]['areas']['top_hits']['size']      =   50;

        // die( json_encode( $query ) );
        return $this->client->search( $query );

    }

    public function get_brokerage_brokers_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'agent';
        $query[ 'type' ]                            =   'agent';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'agency_id' ][ 'terms' ][ 'field' ]  =   'agency._id';
        $query[ 'body' ][ 'aggs' ][ 'agency_id' ][ 'terms' ][ 'size' ]   =   200;

        $query[ 'body' ][ 'aggs' ][ 'agency_id' ][ 'aggs' ]['agency_name']['top_hits']['_source']   =   'agency.contact.name';
        $query[ 'body' ][ 'aggs' ][ 'agency_id' ][ 'aggs' ]['agency_name']['top_hits']['size']      =   1;

        $query[ 'body' ][ 'aggs' ][ 'agency_id' ][ 'aggs' ][ 'agent_name' ][ 'terms' ][ 'field' ]  =   'contact.name';
        $query[ 'body' ][ 'aggs' ][ 'agency_id' ][ 'aggs' ][ 'agent_name' ][ 'terms' ][ 'size' ]   =   150;

        $query[ 'body' ][ 'aggs' ][ 'agency_id' ][ 'aggs' ][ 'agent_name' ][ 'aggs' ][ 'agent_id' ][ 'terms' ][ 'field' ]  =   '_id';
        $query[ 'body' ][ 'aggs' ][ 'agency_id' ][ 'aggs' ][ 'agent_name' ][ 'aggs' ][ 'agent_id' ][ 'terms' ][ 'size' ]   =   1;

        return $this->client->search( $query );

    }

    public function get_project_developer_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'project';
        $query[ 'type' ]                            =   'project';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'terms' ][ 'field' ]  =   'developer._id';
        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'terms' ][ 'size' ]   =   150;

        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ]['developer_name']['top_hits']['_source']   =   'developer.contact.name';
        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ]['developer_name']['top_hits']['size']      =   1;

        // die( json_encode( $query ) );
        return $this->client->search( $query );

    }

    public function get_project_global_developer_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'project';
        $query[ 'type' ]                            =   'project';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'terms' ][ 'field' ]  =   'country';
        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'terms' ][ 'size' ]   =   50;

        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'country' ][ 'top_hits' ][ '_source' ]   =   'country';
        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'country' ][ 'top_hits' ][ 'size' ]      =   1;

        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'developer_name' ][ 'terms' ][ 'field' ]  =   'developer.contact.name';
        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'developer_name' ][ 'terms' ][ 'size' ]   =   150;

        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'developer_name' ][ 'aggs' ]['developer_name']['top_hits']['_source']   =   'developer.contact.name';
        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'developer_name' ][ 'aggs' ]['developer_name']['top_hits']['size']      =   1;

        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'developer_name' ][ 'aggs' ][ 'developer_id' ][ 'terms' ][ 'field' ]  =   'developer._id';
        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'developer_name' ][ 'aggs' ][ 'developer_id' ][ 'terms' ][ 'size' ]   =   1;

        // die( json_encode( $query ) );
        return $this->client->search( $query );

    }

    public function get_services_service_types_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'service';
        $query[ 'type' ]                            =   'service';

        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'service_type_name' ][ 'terms' ][ 'field' ]  =   'type.name';
        $query[ 'body' ][ 'aggs' ][ 'service_type_name' ][ 'terms' ][ 'size' ]   =   50;

        $query[ 'body' ][ 'aggs' ][ 'service_type_name' ][ 'aggs' ][ 'service_type_id' ][ 'terms' ][ 'field' ]  =   'type._id';
        $query[ 'body' ][ 'aggs' ][ 'service_type_name' ][ 'aggs' ][ 'service_type_id' ][ 'terms' ][ 'size' ]   =   1;

        // die( json_encode( $query ) );
        return $this->client->search( $query );

    }

    public function get_all_brokerages_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'agency';
        $query[ 'type' ]                            =   'agency';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 1 ][ 'query_string' ][ 'default_field' ]   =   'settings.role_id';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 1 ][ 'query_string' ][ 'query' ]           =   "4 OR 6";

        $query[ 'body' ][ 'aggs' ][ 'agency_id' ][ 'terms' ][ 'field' ]  =   '_id';
        $query[ 'body' ][ 'aggs' ][ 'agency_id' ][ 'terms' ][ 'size' ]   =   200;

        $query[ 'body' ][ 'aggs' ][ 'agency_id' ][ 'aggs' ]['name']['top_hits']['_source']   =   'contact.name';
        $query[ 'body' ][ 'aggs' ][ 'agency_id' ][ 'aggs' ]['name']['top_hits']['size']      =   1;

        $query[ 'body' ][ 'aggs' ][ 'agency_id' ][ 'aggs' ][ 'agency_email' ][ 'terms' ][ 'field' ]  =   'contact.email';
        $query[ 'body' ][ 'aggs' ][ 'agency_id' ][ 'aggs' ][ 'agency_email' ][ 'terms' ][ 'size' ]   =   1;

        // die( json_encode( $query ) );
        return $this->client->search( $query );

    }

    public function get_all_developers_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'agency';
        $query[ 'type' ]                            =   'agency';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 1 ][ 'query_string' ][ 'default_field' ]   =   'settings.role_id';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 1 ][ 'query_string' ][ 'query' ]           =   "5 OR 6";

        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'terms' ][ 'field' ]  =   '_id';
        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'terms' ][ 'size' ]   =   200;

        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ]['name']['top_hits']['_source']   =   'contact.name';
        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ]['name']['top_hits']['size']      =   1;

        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ][ 'developer_email' ][ 'terms' ][ 'field' ]  =   'contact.email';
        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ][ 'developer_email' ][ 'terms' ][ 'size' ]   =   1;

        // die( json_encode( $query ) );
        return $this->client->search( $query );

    }

    public function get_all_service_types_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'service_type';
        $query[ 'type' ]                            =   'service_type';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'service_type_name' ][ 'terms' ][ 'field' ]  =   'name';
        $query[ 'body' ][ 'aggs' ][ 'service_type_name' ][ 'terms' ][ 'size' ]   =   50;

        $query[ 'body' ][ 'aggs' ][ 'service_type_name' ][ 'aggs' ][ 'service_type_id' ][ 'terms' ][ 'field' ]  =   '_id';
        $query[ 'body' ][ 'aggs' ][ 'service_type_name' ][ 'aggs' ][ 'service_type_id' ][ 'terms' ][ 'size' ]   =   1;

        // die( json_encode( $query ) );
        return $this->client->search( $query );

    }

    public function get_brokerage_properties_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'property';
        $query[ 'type' ]                            =   'property';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'agency_name' ][ 'terms' ][ 'field' ]  =   'agent.agency.contact.name';
        $query[ 'body' ][ 'aggs' ][ 'agency_name' ][ 'terms' ][ 'size' ]   =   200;

        $query[ 'body' ][ 'aggs' ][ 'agency_name' ][ 'aggs' ]['name']['top_hits']['_source']   =   'agent.agency.contact.name';
        $query[ 'body' ][ 'aggs' ][ 'agency_name' ][ 'aggs' ]['name']['top_hits']['size']      =   1;

        $query[ 'body' ][ 'aggs' ][ 'agency_name' ][ 'aggs' ][ 'agency_id' ][ 'terms' ][ 'field' ]  =   'agent.agency._id';
        $query[ 'body' ][ 'aggs' ][ 'agency_name' ][ 'aggs' ][ 'agency_id' ][ 'terms' ][ 'size' ]   =   1;

        $query[ 'body' ][ 'aggs' ][ 'agency_name' ][ 'aggs' ][ 'agency_id' ][ 'aggs' ][ 'agency_email' ][ 'terms' ][ 'field' ]  =   'agent.agency.contact.email';
        $query[ 'body' ][ 'aggs' ][ 'agency_name' ][ 'aggs' ][ 'agency_id' ][ 'aggs' ][ 'agency_email' ][ 'terms' ][ 'size' ]   =   50;

        // die( json_encode( $query ) );
        return $this->client->search( $query );

    }

    public function get_developer_projects_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'project';
        $query[ 'type' ]                            =   'project';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'developer_name' ][ 'terms' ][ 'field' ]  =   'developer.contact.name';
        $query[ 'body' ][ 'aggs' ][ 'developer_name' ][ 'terms' ][ 'size' ]   =   150;

        $query[ 'body' ][ 'aggs' ][ 'developer_name' ][ 'aggs' ]['name']['top_hits']['_source']   =   'developer.contact.name';
        $query[ 'body' ][ 'aggs' ][ 'developer_name' ][ 'aggs' ]['name']['top_hits']['size']      =   1;

        $query[ 'body' ][ 'aggs' ][ 'developer_name' ][ 'aggs' ][ 'developer_id' ][ 'terms' ][ 'field' ]  =   'developer._id';
        $query[ 'body' ][ 'aggs' ][ 'developer_name' ][ 'aggs' ][ 'developer_id' ][ 'terms' ][ 'size' ]   =   1;

        $query[ 'body' ][ 'aggs' ][ 'developer_name' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ][ 'developer_email' ][ 'terms' ][ 'field' ]  =   'developer.contact.email';
        $query[ 'body' ][ 'aggs' ][ 'developer_name' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ][ 'developer_email' ][ 'terms' ][ 'size' ]   =   50;

        // die( json_encode( $query ) );
        return $this->client->search( $query );

    }

    public function get_all_brokerages(){
        $query                      =   [];
        $query[ 'index' ]           =   'agency';
        $query[ 'type' ]            =   'agency';
        $query[ 'body' ][ 'size' ]  =   200;

        return  $this->client->search( $query );
    }

    public function get_projects_by_developer_agg(){
        $query                      =   [];
        $query[ 'index' ]           =   'project';
        $query[ 'type' ]            =   'project';

        $query[ 'body' ][ 'size' ]  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'terms' ][ 'field' ]   =   'developer._id';
        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'terms' ][ 'size' ]    =   150;
        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ][ 'developer_name' ][ 'top_hits' ][ '_source' ] =   'developer.contact.name';
        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ][ 'developer_name' ][ 'top_hits' ][ 'size' ]    =   1;

        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ][ 'project_name' ][ 'terms' ][ 'field' ]    =   'name';
        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ][ 'project_name' ][ 'terms' ][ 'size' ]     =   100;
        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ][ 'project_name' ][ 'aggs' ][ 'project_name' ][ 'top_hits' ][ '_source' ]  =   'name';
        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ][ 'project_name' ][ 'aggs' ][ 'project_name' ][ 'top_hits' ][ 'size' ]     =   1;

        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ][ 'project_name' ][ 'aggs' ][ 'project_id' ][ 'terms' ][ 'field' ]  =   '_id';
        $query[ 'body' ][ 'aggs' ][ 'developer_id' ][ 'aggs' ][ 'project_name' ][ 'aggs' ][ 'project_id' ][ 'terms' ][ 'size' ]   =   200;

        // die( json_encode( $query ) );
        return $this->client->search( $query );
    }

    public function get_brokerage_call_tracking_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'agency';
        $query[ 'type' ]                            =   'agency';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 1 ][ 'query_string' ][ 'default_field' ]   =   'settings.role_id';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 1 ][ 'query_string' ][ 'query' ]           =   "4 OR 6";

        $query[ 'body' ][ 'aggs' ][ 'agency_name' ][ 'terms' ][ 'field' ]  =   'contact.name';
        $query[ 'body' ][ 'aggs' ][ 'agency_name' ][ 'terms' ][ 'size' ]   =   200;

        $query[ 'body' ][ 'aggs' ][ 'agency_name' ][ 'aggs' ]['name']['top_hits']['_source']   =   'contact.name';
        $query[ 'body' ][ 'aggs' ][ 'agency_name' ][ 'aggs' ]['name']['top_hits']['size']      =   1;

        $query[ 'body' ][ 'aggs' ][ 'agency_name' ][ 'aggs' ][ 'agency_tracking' ][ 'terms' ][ 'field' ]  =   'settings.lead.call_tracking.phone';
        $query[ 'body' ][ 'aggs' ][ 'agency_name' ][ 'aggs' ][ 'agency_tracking' ][ 'terms' ][ 'size' ]   =   1;

        // die( json_encode( $query ) );
        return $this->client->search( $query );

    }

    public function get_project_completion_status_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'project';
        $query[ 'type' ]                            =   'project';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'completion_status' ][ 'terms' ][ 'field' ]  =   'status';
        $query[ 'body' ][ 'aggs' ][ 'completion_status' ][ 'terms' ][ 'size' ]   =   100;

        $query[ 'body' ][ 'aggs' ][ 'completion_status' ][ 'aggs' ]['name']['top_hits']['_source']   =   'status';
        $query[ 'body' ][ 'aggs' ][ 'completion_status' ][ 'aggs' ]['name']['top_hits']['size']      =   1;

        // die( json_encode( $query ) );
        return $this->client->search( $query );

    }

    public function get_property_completion_status_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'property';
        $query[ 'type' ]                            =   'property';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'completion_status' ][ 'terms' ][ 'field' ]  =   'completion_status';
        $query[ 'body' ][ 'aggs' ][ 'completion_status' ][ 'terms' ][ 'size' ]   =   100;

        $query[ 'body' ][ 'aggs' ][ 'completion_status' ][ 'aggs' ]['name']['top_hits']['_source']   =   'completion_status';
        $query[ 'body' ][ 'aggs' ][ 'completion_status' ][ 'aggs' ]['name']['top_hits']['size']      =   1;

        // die( json_encode( $query ) );
        return $this->client->search( $query );

    }

    public function get_project_locations_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'project';
        $query[ 'type' ]                            =   'project';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'terms' ][ 'field' ]  =   'city';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'terms' ][ 'size' ]   =   500;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ]['city']['top_hits']['_source']   =   'city';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ]['city']['top_hits']['size']      =   1;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'terms' ][ 'field' ]  =   'area';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'terms' ][ 'size' ]   =   1000;

        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ]['aggs' ]['area']['top_hits']['_source']   =   'area';
        $query[ 'body' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ]['aggs' ]['area']['top_hits']['size']      =   1;

        return $this->client->search( $query );

    }

    public function get_project_global_locations_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'project';
        $query[ 'type' ]                            =   'project';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'terms' ][ 'field' ]  =   'country';
        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'terms' ][ 'size' ]   =   50;

        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'country' ][ 'top_hits' ][ '_source' ]   =   'country';
        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'country' ][ 'top_hits' ][ 'size' ]      =   1;

        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'city' ][ 'terms' ][ 'field' ]  =   'city';
        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'city' ][ 'terms' ][ 'size' ]   =   500;

        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'city' ][ 'top_hits' ][ '_source' ]   =   'city';
        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'city' ][ 'top_hits' ][ 'size' ]      =   1;

        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'terms' ][ 'field' ]  =   'area';
        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'terms' ][ 'size' ]   =   1000;

        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'area' ][ 'top_hits' ][ '_source' ]   =   'area';
        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'city' ][ 'aggs' ][ 'areas' ][ 'aggs' ][ 'area' ][ 'top_hits' ][ 'size' ]      =   1;

        return $this->client->search( $query );

    }

    public function get_project_global_completion_status_agg(){

        $query                                      =   array();
        $query[ 'index' ]                           =   'project';
        $query[ 'type' ]                            =   'project';
        $query[ 'body' ][ 'size' ]                  =   0;

        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'default_field' ]   =   'settings.status';
        $query[ 'body' ][ 'query' ][ 'bool' ][ 'must' ][ 0 ][ 'query_string' ][ 'query' ]           =   1;

        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'terms' ][ 'field' ]  =   'country';
        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'terms' ][ 'size' ]   =   50;

        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'country' ][ 'top_hits' ][ '_source' ]   =   'country';
        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'country' ][ 'top_hits' ][ 'size' ]      =   1;

        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'completion_status' ][ 'terms' ][ 'field' ]  =   'status';
        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'completion_status' ][ 'terms' ][ 'size' ]   =   100;

        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'completion_status' ][ 'aggs' ][ 'name' ][ 'top_hits' ][ '_source' ]   =   'status';
        $query[ 'body' ][ 'aggs' ][ 'country' ][ 'aggs' ][ 'completion_status' ][ 'aggs' ][ 'name' ][ 'top_hits' ][ 'size' ]      =   1;

        // die( json_encode( $query ) );
        return $this->client->search( $query );

    }
}
