<?php
namespace SearchPKG\Controllers;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use SearchPKG\Traits\ConnectionTrait;

class SearchController extends BaseController
{
    use ConnectionTrait;
    protected $index;
    protected $config;
    protected $credentials;
    protected $host;
    protected $client;
    protected $connect_flag;
    protected $sort;
    protected $size = 20;
    protected $page = 1;
    protected $search_array;
    protected $query_validations;
    protected $group_by = 'area';

    //
    //
    //  CRUDS
    //
    //
    public function index($type = 'organic'){
        // dd($this->search_array);
        // Transposing methods
        if($type==='budget'){
            $data   =   $this->set_query_validations('budget')->search_budget( $this->search_array );
        }
        else{
            $data   =   $this->set_query_validations()->search( $this->search_array );
        }
        return $this->response($data, $type);
    }

    public function show($id){
        // Transposing methods
        $data   =   $this->set_query_validations('search')->search_by_id( $id );
        return $this->response($data);
    }

    public function store( $data_to_store ){
        if(!array_has($data_to_store, 'timestamp.created')){
            array_set($data_to_store, 'timestamp.created', date('Y-m-d\TH:i:s\Z'));
        }
        if(!array_has($data_to_store, 'timestamp.updated')){
            array_set($data_to_store, 'timestamp.updated', date('Y-m-d\TH:i:s\Z'));
        }
        if(!array_has($data_to_store, '_id')){
            return  response()->json( [ 'message' => 'Bad request' ], 400 );
        }
        // die( json_encode( $data_to_store ) );
        if( in_array( array_get($this->config,'id'), [ 'agent', 'property', 'project' ] )){
            $data_to_store      =   $this->compute_organic_score( $data_to_store );
        }
        // die( json_encode( $data_to_store ) );
        $params_to_store    =   $this->add_defaults($data_to_store);
        // die( json_encode( $params_to_store ) );
        return $this->store_els( $params_to_store );
    }

    public function update( $data_to_update, $id = null ){
        if(!array_has($data_to_update, 'timestamp.updated')){
            array_set($data_to_update, 'timestamp.updated', date('Y-m-d\TH:i:s\Z'));
        }
        if(!array_has($data_to_update, '_id') && is_null($id)){
            return  response()->json( [ 'message' => 'Bad request' ], 400 );
        }
        $key    =   array_get($data_to_update, '_id', $id);
        $index  =   array_get($this->config,'id');
        if( in_array( $index, [ 'agent', 'property', 'project' ] )){
            $details                =   array_get($this->search_by_id( $key ), '_source' );
            $details                =   $this->merge_objects($details,$data_to_update);
            $updated_organic_score  =   $this->compute_organic_score( $details );
            $organic_score          =   array_get($updated_organic_score, 'settings.organic_score');
            array_set($data_to_update,'settings.organic_score',$organic_score);
        }
        // die( json_encode( $data_to_update ) );
        return $this->update_els( $data_to_update, $key );
    }

    private function merge_objects($one,$two){
        $dotted_one     =   array_dot($one);
        $dotted_two     =   array_dot($two);
        foreach($dotted_two as $key => $value){
            if(array_key_exists($key, $dotted_one)){
                $dotted_one[ $key ]    =   $value;
            }
        }
        return array_undot($dotted_one);
    }

    public function update_by_query( $data_to_update, $prefix = '' ){
        if(empty($data_to_update)){
            return  response()->json( [ 'message' => 'Bad request' ], 400 );
        }
        return $this->update_by_query_els( $this->search_array, $data_to_update, $prefix );
    }

    // Response formatter
    public function response($response,$type='organic'){
        // dd($response);
        $stripped_data  =   [];
        // What type of response
        if($type === 'budget'){
            $response   =   collect( $response[ 'aggregations' ][ $this->group_by ][ 'buckets' ] );
            $return     =   [];
            if( $response->isNotEmpty() ){
                $return     =   $response->map( function( $item, $key ){
                    $response   =   [];
                    array_set( $response, 'area.name', array_get( $item, 'key', '' ) );
                    array_set( $response, 'area.total', array_get( $item, 'doc_count', '' ) );
                    foreach( $item[ 'property' ][ 'hits' ][ 'hits' ] as $key => $property ){
                        $response[ 'area' ][ 'property' ][ $key ]                   =   $property[ '_source' ];
                        $response[ 'area' ][ 'property' ][ $key ][ '_id' ]          =   $property[ '_id' ];
                        $response[ 'area' ][ 'property' ][ $key ][ 'favourite' ]    =   0;
                    }
                    return $response;
                });
                $return = $return->toArray();
            }
            $stripped_data  =   $return;
        }
        else{
            // Organic responses
            // Read
            if(array_has($response,'hits.hits',false)){
                // Set total
                array_set($stripped_data,'total',array_get($response,'hits.total',0));
                $hits           =   array_get($response,'hits.hits',[]);
                for($i=0;$i<count($hits);$i++){
                    array_set($stripped_data,'data.'.$i,array_get($hits[$i],'_source',[]));
                    // Set id
                    array_set($stripped_data,'data.'.$i.'._id',array_get($hits[$i],'_id',null));
                }
            }
            // Show
            else{
                $stripped_data  =   array_get($response,'_source',[]);
                array_set($stripped_data,'_id',array_get($response,'_id',null));
            }
        }
        // dd($stripped_data);
        return response()->json($stripped_data,200);
    }

    /*
    |--------------------------------------------------------------------------
    | Elasticsearch functions
    |--------------------------------------------------------------------------
    |
    |
    */
    /**
    * [Search model in Els
    * @param  [type] $search_array [description]
    * @return [type]              [description]
    */
    public function search( $search_array ){

        try{
            $query                      =   [];
            $query[ 'index' ]           =   $this->index;
            $query[ 'type' ]            =   $this->index;
            $search_array               =   $search_array['query'];

            if ( empty($search_array) ){
                $query[ 'body' ][ 'query' ][ 'match_all' ]  =   new \stdClass();
            }
            else{
                $queryBuilder           =   new \SearchPKG\Helpers\QueryBuilder( $this->query_validations, $this->sort, $this->index );
                foreach($search_array as $key => $value){
                    $q                  =   $queryBuilder->build( $value );
                }
                $query[ 'body' ]        =    $q->getQuery(count($search_array));
            }
            // Isolating pagination parameters
            $query[ 'body' ][ 'from' ]  =   ( $this->page == 1 OR $this->page == 0 ) ? 0 : ($this->page * $this->size - $this->size );
            $query[ 'body' ][ 'size' ]  =   $this->size;
            // die( json_encode( $query ) );
            return $this->client->search( $query );
        }
        catch( \Aws\ElasticsearchService\Exception $e ){
            return  response()->json( [ 'message' => $e->getMessage() ], $e->getCode() );
        }
    }

    /**
    * Search id assigned to the model
    * @param  [type] $id
    * @return [type]     [description]
    */
    public function search_by_id( $id ){
        try{
            $query                  =   [];
            $query[ 'index' ]       =   $this->index;
            $query[ 'type' ]        =   $this->index;
            $query[ 'id' ]          =   $id;
            return $this->client->get( $query );
        }
        catch( \Aws\ElasticsearchService\Exception $e ){
            return  response()->json( [ 'message' => $e->getMessage() ], $e->getCode() );
        }
        catch( \Elasticsearch\Common\Exceptions\Missing404Exception $e ){
            return  response()->json( [ 'message' => 'That '.$this->index.' does not exist'  ], $e->getCode() );
        }
    }

    /**
    * [Search budget model in Els
    * @param  [type] $search_array [description]
    * @return [type]              [description]
    */
    public function search_budget( $search_array ){
        // Validating search_array
        // only those elements which we need
        $search_array     =   [ array_only( $search_array, ['settings','price','rent_buy','residential_commercial','city'] ) ];
        // dd($search_array);
        try{
            $query                      =   [];
            $query[ 'index' ]           =   $this->index;
            $query[ 'type' ]            =   $this->index;
            $queryBuilder               =   new \SearchPKG\Helpers\QueryBuilder( $this->query_validations, $this->sort, $this->index );
            foreach($search_array as $key => $value){
                $q                  =   $queryBuilder->build( $value );
            }
            $query[ 'body' ]        =    $q->getQuery(count($search_array));
            // dd($this->query_validations,$query);
            // aggs
            $query[ 'body' ][ 'size' ]  =   0;
            $query[ 'body' ][ 'aggs' ][ $this->group_by ][ 'terms' ][ 'field' ]     =   $this->group_by;
            $query[ 'body' ][ 'aggs' ][ $this->group_by ][ 'terms' ][ 'order' ][ 'top_score' ]     =   'desc';
            $query[ 'body' ][ 'aggs' ][ $this->group_by ][ 'terms' ][ 'size' ]                     =   20;
            $query[ 'body' ][ 'aggs' ][ $this->group_by ][ 'aggs' ][ 'top_score' ][ 'max' ][ 'script' ]     =   '_score';
            $query[ 'body' ][ 'aggs' ][ $this->group_by ][ 'aggs' ][ $this->index ][ 'top_hits' ][ 'from' ]     =   ( $this->page === 1 OR $this->page === 0 ) ? 0 : ($this->page * $this->size - $this->size -1 );
            $query[ 'body' ][ 'aggs' ][ $this->group_by ][ 'aggs' ][ $this->index ][ 'top_hits' ][ 'size' ]     =   $this->size;
            // die( json_encode( $query ) );
            return $this->client->search( $query );
        }
        catch( \Aws\ElasticsearchService\Exception $e ){
            return  response()->json( [ 'message' => $e->getMessage() ], $e->getCode() );
        }
    }

    public function store_els( $data_to_store ){
        /**
        * 1.-  Els query
        */
        try{
            $id                                             =   array_get( $data_to_store, '_id' );
            array_forget( $data_to_store, '_id' );
            $elastic                                        =   array();
            $elastic[ 'index' ]                             =   $this->index;
            $elastic[ 'type' ]                              =   $this->index;
            $elastic[ 'body' ][ 'doc' ]                     =   $this->typecasting( $data_to_store );
            $elastic[ 'body' ][ 'doc_as_upsert' ]           =   true;
            $elastic[ 'id' ]                                =   $id;
            $this->client->update( $elastic );
            return response()->json( [ 'message' => $this->index.' created with _id: '.$id ], 200 );
        }
        catch( \Elasticsearch\Common\Exceptions\BadRequest400Exception $e ){
            return  response()->json( [ 'message' => $e->getMessage() ], $e->getCode() );
        }
        catch( \ErrorException $e ){
            $code   =   $e->getCode() == 0 ? 400 : $e->getCode();
            return  response()->json( [ 'message' => $e->getMessage() ], $code );
        }
    }

    public function update_els( $data_to_update, $id = null ){
        /**
        * 1.-  Els query
        */
        try{
            // dd($id);
            $elastic                                        =   array();
            $elastic[ 'index' ]                             =   $this->index;
            $elastic[ 'type' ]                              =   $this->index;
            $elastic[ 'body' ][ 'doc' ]                     =   $this->typecasting( $data_to_update );
            $elastic[ 'id' ]                                =   $id;
            $this->client->update( $elastic );
            return response()->json( [ 'message' => $this->index.' with _id: '.$id .' updated'], 200 );
        }
        catch( \Aws\ElasticsearchService\Exception $e ){
            return  response()->json( [ 'message' => $e->getMessage() ], $e->getCode() );
        }
    }

    public function update_by_query_els( $search_array, $data_to_update,$prefix ){
        /**
        * 1.-  Els query
        */
        try{
            $this->set_query_validations();
            $params_to_update                               =   $this->typecasting( $data_to_update );
            $elastic                                        =   array();
            $elastic[ 'index' ]                             =   $this->index;
            $elastic[ 'type' ]                              =   $this->index;
            $search_array                                   =   $search_array['query'];
            if ( empty($search_array) ){
                $elastic[ 'body' ][ 'query' ][ 'match_all' ]  =   new \stdClass();
            }
            else{
                $query_builder              =   new \SearchPKG\Helpers\QueryBuilder( $this->query_validations, $this->sort, $this->index );
                foreach($search_array as $key => $value){
                    $q                  =   $query_builder->build( $value );
                }
                $elastic[ 'body' ]        =    $q->getQuery(count($search_array));
                array_forget($elastic, 'body.query.function_score.boost_mode');
                array_forget($elastic, 'body.query.function_score.functions');
                array_forget($elastic, 'body.query.function_score.score_mode');
                // array_forget($elastic, 'body.query.function_score.query.bool.should');
            }
            $script_builder                 =   new \SearchPKG\Helpers\ScriptBuilder();
            $elastic[ 'body' ][ 'script' ]  =   $script_builder->build( $params_to_update,$prefix )->getScript();
            // die( json_encode( $elastic ) );
            $this->client->updateByQuery( $elastic );
            return response()->json( [ 'message' => $this->index.' was updated by query'], 200 );
        }
        catch( \Aws\ElasticsearchService\Exception $e ){
            return  response()->json( [ 'message' => $e->getMessage() ], $e->getCode() );
        }
    }

    /*
    |--------------------------------------------------------------------------
    | ELS INDEX functions
    |--------------------------------------------------------------------------
    |
    |
    */
    /**
    * Recreate the entire index
    * @return [type] [description]
    */
    public function truncate_index(){
        //1.-   Check if aws is connected and the index has been set
        if( $this->connect_check()   &&  $this->set_search_index_check() ){
            $this->delete_index();
            $this->create_index();
            return response()->json( array( 'messaging' => $this->index.' reindexed' ), 403 );
        }
        else{
            return response()->json( array( 'messaging' => 'Call the set_search_for() method first' ), 403 );
        }
    }

    /**
    * Delete index
    * @return [type] [description]
    */
    public function delete_index(){
        //1.-   Check if aws is connected and the index has been set
        if( $this->connect_check()   &&  $this->set_search_index_check() ){
            return $this->client->indices()->delete( [ 'index' => $this->index ] );
        }
        else{
            return response()->json( array( 'messaging' => 'Call the set_search_for() method first' ), 403 );
        }
    }

    /**
    * Create index
    * @return [type] [description]
    */
    public function create_index(){
        //1.-   Check if aws is connected and the index has been set
        if( $this->connect_check()   &&  $this->set_search_index_check() && $this->null_check( $this->index ) ){
            //2.-   Checking if Index exist
            if( !$this->client->indices()->exists( [ 'index' => $this->index ] ) ){
                #   Index doesn't exist!!
                #
                #   Create
                //2.1.-   Checking if reindex exist
                $reindex  =   $this->config[ 'datastores' ][ 'els' ][ 'reindex' ];
                if( isset( $reindex ) AND $this->null_check( $reindex ) ){
                    $mapping    =   [ 'mappings'  =>    [
                        $this->index    =>  [
                            'properties'    =>  $reindex
                        ]
                        ]];
                        $this->client->indices()->create([
                            'index'     =>  $this->index,
                            'body'      =>  $mapping
                        ]);
                    }
                    else{
                        $this->client->indices()->create([
                            'index'     =>  $this->index
                        ]);
                    }
                }
                else{
                    return response()->json( array( 'messaging' => 'AWS-ES connected, but no index chosen' ), 403 );
                }
            }
            else{
                return response()->json( array( 'messaging' => 'Call the set_search_for() method first' ), 403 );
            }
        }

        /**
        * Check if user has called the connect method
        * @return [type] [description]
        */
        protected function connect_check(){
            if( $this->connect_flag ){
                return true;
            }
            else{
                return response()->json( array( 'messaging' => 'Call the connect() method first' ), 403 );
            }
        }

        /**
        * Check if user has set search for
        */
        protected function set_search_index_check(){
            if( !is_null( $this->index ) ){
                return true;
            }
            else{
                return response()->json( array( 'messaging' => 'Call the set_search_for() method first' ), 403 );
            }
        }

        /*
        |--------------------------------------------------------------------------
        | UTILITY functions
        |--------------------------------------------------------------------------
        |
        |
        */
        /**
        * Validating request
        */
        // private function validate_request( $request, $action ){
        //
        //     if( count( $request ) > 0 ){
        //
        //         $validation_rules    =  $this->config[ 'actions' ][ $action ][ 'validation' ][ 'request' ];
        //
        //         return  $this->validate_requests( $request, $validation_rules );
        //
        //     }
        //     else{
        //
        //         return  response()->json( [ 'message' => 'Insufficient input' ], 400 );
        //
        //     }
        //
        // }
        /**
        * Adding defaults to request
        */
        private function add_defaults( $data ){
            $name       =   'default';
            $values     =   [];
            $defaults   =   $this->get_fields($name,$values);

            if ( isset( $defaults ) && !empty( $defaults ) ){
                $data       =   $this->array_dot_formatted( $data );
                $defaults   =   array_dot( $defaults );
                foreach( $defaults as $key => $value ){
                    if ( !array_key_exists( $key, $data ) ){
                        $data[ $key ]    =   $value;
                    }
                }
            }
            return $data;
        }

        private function compute_organic_score( $data ){
            $name       =   'weight';
            $values     =   [];
            $weights    =   $this->get_fields($name);

            $index          =   array_get($this->config,'id');
            $organic_score  =   0;
            if ( isset( $weights ) && !empty( $weights ) ){
                $data       =   $this->array_dot_formatted( $data );
                $weights    =   array_dot( $weights );
                foreach( $weights as $key => $value ){
                    if ( array_key_exists( $key, $data ) && !empty($data[$key]) ){
                        if( is_numeric( $value ) ){
                            //TODO Remove this condition when agent organic score is included in property organic score in polling also
                            if( $index == 'property' && !starts_with( $key, 'agent.' ) || $index == 'agent' || $index == 'project' ){
                                $organic_score    +=   $value;
                            }
                        }
                        else{
                            $extracted_value = array_get( $data, $key );
                            if( $index == 'property' ){
                                if( $key == 'amenities' ){
                                    $organic_score   +=  log( count( $extracted_value ) + 2 );
                                }
                                if( $key == 'writeup.description' && strlen( $extracted_value ) > 300 ){
                                    $organic_score++;
                                }
                                if( $key == 'images.original' && count( $extracted_value ) >= 10 ){
                                    $organic_score++;
                                }
                                if( $key == 'exclusive' && ( !empty( $extracted_value ) || !is_null( $extracted_value ) ) ){
                                    $organic_score++;
                                }
                                if( $key == 'cheques' && array_get( $data, 'rent_buy' ) == 'Rent' ){
                                    $organic_score++;
                                }
                            }
                            if( $index == 'agent' ){
                                if( $key == 'contact.picture' && !str_contains($extracted_value,'no-agent-img') ){
                                    $organic_score = $organic_score + 3;
                                }
                            }
                            if( $index == 'project' ){
                                if( $key == 'image.logo' && !str_contains($extracted_value,'no-img-available') ){
                                    $organic_score++;
                                }
                                if( $key == 'image.banner' ){
                                    $organic_score++;
                                }
                                if( $key == 'image.floor_plans' && count( $extracted_value ) >= 2 ){
                                    $organic_score++;
                                }
                                if( $key == 'writeup.description' && strlen( $extracted_value ) > 300 ){
                                    $organic_score++;
                                }
                                if( $key == 'writeup.description_ar' && strlen( $extracted_value ) > 300 ){
                                    $organic_score++;
                                }
                                if( $key == 'image.portal.desktop' && count( $extracted_value ) >= 5 ){
                                    $organic_score++;
                                }
                                if( $key == 'image.portal.mobile' && count( $extracted_value ) >= 5 ){
                                    $organic_score++;
                                }
                            }
                        }
                    }
                }
            }
            $data   =   array_undot( $data );
            if( !empty( $organic_score ) )
                array_set( $data, 'settings.organic_score', $organic_score );
            return $data;
        }

        private function translate_to_es_keys( $params ){
            $translation    =   [];
            /**
            *      1.  Array dot everything
            *      2.  Traverse the array
            *          2.1.    If final key is
            *              2.1.1. numeric  =   make array
            *              2.1.2. min/max  =   make array
            */
            if( is_object( $params ) ){
                $params     =  json_decode(json_encode($params), true);
            }
            // 1.
            $params     =   array_dot( $params );
            // 2.
            foreach( $params as $key => $value ){
                preg_match('/(.*)\.([^.]*)$/', $key, $matches);
                if( !empty( $matches ) ){
                    $final_key   =   $matches[ 2 ];
                    if( is_numeric( $final_key ) ){
                        $translation[ $matches[ 1 ] ][ $matches[ 2 ] ]  =   $value;
                    }
                    else if( $final_key === "min" || $final_key === "max" ){
                        $translation[ $matches[ 1 ] ][ $matches[ 2 ] ]  =   (int) $value;
                    }
                    else{
                        $translation[ $key ]    =   $value;
                    }
                }
                else{
                    $translation[ $key ]    =   $value;
                }
            }
            return $translation;
        }

        private function typecasting( $document ){
            $mapping        =   array_get( $this->config, 'connection.mapping' );
            $document       =   $this->array_dot_formatted( $document );
            $typecastedDoc  =   [];
            $mapping        =   array_dot( $mapping );

            foreach( $document as $field => $value ){
                $checkArray     =   explode( ".", $field );
                if( is_numeric( end( $checkArray ) ) ){
                    $checkField     =   str_replace( ".", ".properties.", rtrim( preg_replace( "/[^A-Za-z\.\_]/", "", $field ), "." ) );
                }
                else{
                    $checkField     =   str_replace( ".", ".properties.", $field );
                }
                if( isset( $mapping[ $checkField . '.type' ] ) ){
                    switch ( $mapping[ $checkField .'.type' ] ) {
                        case 'byte':
                        case 'short':
                        case 'integer':
                        $typecastedDoc[ $field ]    =   ( int ) $value;
                        break;
                        case 'geo_point':
                        break;
                        case 'boolean':
                        $typecastedDoc[ $field ]    =   filter_var( (int) $value, FILTER_VALIDATE_BOOLEAN );
                        break;
                        default:
                        $typecastedDoc[ $field ]    =   $value;
                        break;
                    }
                }
            }

            $typecastedDoc    =   $this->array_unflat( $typecastedDoc );
            return $typecastedDoc;
        }

        /**
        * Custom Function to group array and nested data together in a dotted array
        */
        private function array_dot_formatted( $data ){
            /**
            *  1.  Store incoming data in a new variable
            *  2.  Filter out all array and nested types of data from the incoming data
            *      2.1.  Identify array and nested types of data by detecting numeric values in the keys
            *      2.2.  Group all array types together in a new non-numeric key index
            *      2.3.  Save nested types in a new array for later use
            *      2.4.  Clean incoming data by removing keys with numeric indices
            *  3.  Put back nested data in the new data array
            */

            #   1
            $mapping            =   array_get( $this->config, 'connection.mapping' );
            $original_data      =   $data;
            $mapping            =   array_dot($mapping);
            $data               =   array_dot($data);
            $nested_fields      =   [];

            foreach($mapping as $key => $value){
                if(ends_with($key,'type')){
                    array_set( $types, str_replace(['.properties.'],['.'],$key), $value );
                }
            }

            #   2
            foreach( $data as $k => $v ){
                #   2.1
                if( preg_match( "/\d{1,2}/", $k, $match ) == 1 ){
                    list( $field, $stub ) = preg_split('/(?=\d)/', $k, 2);
                    $field  =   rtrim($field, '.');
                    $type   =   array_get( $types, $field.'.type' );
                    if( $type == 'array' ){
                        #   2.2
                        $data[ preg_replace( "/.\d{1,2}/", "", $k ) ][]   =  $v;
                    }
                    elseif( $type == 'nested' ){
                        #   2.3
                        $nested_fields[] = $field;
                    }
                    #   2.4
                    array_forget( $data, $k );
                }
            }

            #   3
            $nested_fields = array_unique($nested_fields);
            if( count($nested_fields) > 0){
                foreach( $nested_fields as $nested_field ){
                    array_forget( $data, $nested_field );
                    $data[$nested_field] = array_get($original_data,$nested_field);
                }
            }
            return $data;
        }

        public function set_source( $search_array ){
            if( array_has( $search_array, 'source' ) ){
                $this->source     =   array_get( $search_array, 'source' );
            }
            return $search_array;
        }
    }
