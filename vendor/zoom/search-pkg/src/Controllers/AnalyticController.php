<?php

namespace SearchPKG\Controllers;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\Http\Controllers\Controller;
use SearchPKG\Traits\ConnectionTrait;

class AnalyticController extends BaseController
{
    use ConnectionTrait;

    protected $index;
    protected $config;
    protected $credentials;
    protected $host;
    protected $client;
    protected $connect_flag;
    protected $sort;
    protected $size = 20;
    protected $page = 1;
    protected $aggs = null;
    protected $source = null;
    protected $search_array;
    protected $query_validations;



    /*
    |--------------------------------------------------------------------------
    | ANALYTIC fxs
    |--------------------------------------------------------------------------
    |
    |
    */

    public function total_entity(){

        $this->set_query_validations();

        try{
            $query                      =   [];
            $query[ 'index' ]           =   $this->index;
            $query[ 'type' ]            =   $this->index;
            $search_array               =   $this->search_array['query'];

            if ( empty($search_array) ){
                $query[ 'body' ][ 'query' ][ 'match_all' ]  =   new \stdClass();
            }
            else{
                $queryBuilder           =   new \SearchPKG\Helpers\QueryBuilder( $this->query_validations, $this->sort, $this->index );
                foreach($search_array as $key => $value){
                    $q                  =   $queryBuilder->build( $value );
                }
                $query[ 'body' ]        =    $q->getQuery(count($search_array));
            }

            // die( json_encode( $query ) );
            return $this->client->count( $query );

        }
        catch( \Aws\ElasticsearchService\Exception $e ){

            return  response()->json( [ 'message' => $e->getMessage() ], $e->getCode() );

        }
    }

    public function aggs_per_action(){

        $this->set_query_validations();
        try{
            $aggs               =   $this->aggs_query();
            $query_body         =   [];
            $query              =   [];
            $query[ 'index' ]   =   $this->index;
            $query[ 'type' ]    =   $this->index;
            $search_array       =   $this->search_array['query'];

            if ( empty($search_array) ){
                $query_body[ 'query' ][ 'match_all' ]  =   new \stdClass();
            }
            else{
                $queryBuilder      =   new \SearchPKG\Helpers\QueryBuilder( $this->query_validations, $this->sort, $this->index );
                foreach($search_array as $key => $value){
                    $q                  =   $queryBuilder->build( $value );
                }
                $query_body        =    $q->getQuery(count($search_array));
            }
            $query[ 'body' ]            =   $aggs;
            $query[ 'body' ]['query']   =   $query_body['query'];
            $query[ 'body' ]['size']    =   0;
            // die( json_encode( $query ) );
            return $this->client->search( $query );
        }
        catch( \Aws\ElasticsearchService\Exception $e ){
            return  response()->json( [ 'message' => $e->getMessage() ], $e->getCode() );
        }
    }

    public function guide_me_properties_radius(){
        $search_query = array_get($this->search_array,'query.0',[]);
        $km = array_get($search_query, 'meters', 100)/1000;
        $coordinates  = array_get($search_query, 'coordinates');
        $search_filters = array_except($search_query, ['meters','coordinates']);
        $distance = "6367 * Math.acos( Math.cos( ".array_get($coordinates, 'lat')." * Math.PI / 180D ) * Math.cos( doc['coordinates.lat'].value * Math.PI / 180D ) * Math.cos( doc['coordinates.lng'].value * Math.PI / 180D - ".array_get($coordinates, 'lng')." * Math.PI / 180D ) + Math.sin( ".array_get($coordinates, 'lat')." * Math.PI / 180D) * Math.sin( doc['coordinates.lat'].value * Math.PI / 180D  ) )  < " .$km;

        $this->set_query_validations();
        try{

            $query_body         =   [];
            $query              =   [];
            $query[ 'index' ]   =   $this->index;
            $query[ 'type' ]    =   $this->index;
            $search_array[]     =   $search_filters;

            if ( empty($search_array) ){
                $query_body[ 'query' ][ 'match_all' ]  =   new \stdClass();
            }
            else{
                $queryBuilder      =   new \SearchPKG\Helpers\QueryBuilder( $this->query_validations, $this->sort, $this->index );
                // $query_body        =   $queryBuilder->build( $search_filters )->getQuery();
                foreach($search_array as $key => $value){
                    $q                  =   $queryBuilder->build( $value );
                }
                $query_body        =    $q->getQuery(count($search_array));
            }
            // Building aggregation
            // TODO make size and net(hits) dymanic
            $query[ 'body' ]['aggs']['lat']['terms']['field']                               =   "coordinates.lng";
            $query[ 'body' ]['aggs']['lat']['terms']['size']                                =   15;
            $query[ 'body' ]['aggs']['lat']['aggs']['top_hits']['top_hits']['size']         =   3;

            //Building queri
            $query[ 'body' ]['query']   =   $query_body['query'];

            //Building script
            $script['script']['source'] = $distance;
            $script['script']['lang']   = 'painless';

            $query[ 'body' ]['query']['function_score']['query']['bool']['should'][0]['bool']['must'][]['script']    =   $script;

            $query[ 'body' ]['size']    =   0;
            $query[ 'body' ]['sort']    =   [ 'timestamp.created' => [ 'order' => 'asc' ] ];
            // die( json_encode( $query ) );
            return $this->client->search( $query );
        }
        catch( \Aws\ElasticsearchService\Exception $e ){
            return  response()->json( [ 'message' => $e->getMessage() ], $e->getCode() );
        }
    }

    public function aggs_guide_me_stats(){
      //Query
      $this->set_query_validations();
      //Aggregation
      $aggs = [];
      array_set($aggs, 'aggs.type.terms.field', 'type');
      array_set($aggs, 'aggs.type.terms.size', 5);
      array_set($aggs, 'aggs.type.aggs.rent_buy.terms.field', 'rent_buy');
      array_set($aggs, 'aggs.type.aggs.rent_buy.terms.size', 2);
      array_set($aggs, 'aggs.type.aggs.rent_buy.aggs.price_per_month.date_histogram.field', 'timestamp.created');
      array_set($aggs, 'aggs.type.aggs.rent_buy.aggs.price_per_month.date_histogram.interval', '1M');
      array_set($aggs, 'aggs.type.aggs.rent_buy.aggs.price_per_month.date_histogram.format', 'MMM');
      array_set($aggs, 'aggs.type.aggs.rent_buy.aggs.price_per_month.aggs.price.avg.field', 'price');
      array_set($aggs, 'aggs.type.aggs.rent_buy.aggs.avg_price.avg.field', 'price');
      array_set($aggs, 'aggs.type.aggs.rent_buy.aggs.avg_price.avg.missing', 0);
      array_set($aggs, 'aggs.type.aggs.rent_buy.aggs.avg_builtup_area.avg.field', 'dimension.builtup_area');
      array_set($aggs, 'aggs.type.aggs.rent_buy.aggs.avg_builtup_area.avg.missing', 0);

      try{

          $query_body         =   [];
          $query              =   [];
          $query[ 'index' ]   =   $this->index;
          $query[ 'type' ]    =   $this->index;
          $search_array       =   $this->search_array['query'];

          if ( empty($search_array) ){
              $query_body[ 'query' ][ 'match_all' ]  =   new \stdClass();
          }
          else{
              $queryBuilder      =   new \SearchPKG\Helpers\QueryBuilder( $this->query_validations, $this->sort, $this->index );
              foreach($search_array as $key => $value){
                  $q                  =   $queryBuilder->build( $value );
              }
              $query_body        =    $q->getQuery(count($search_array));
          }
          $query[ 'body' ]            =   $aggs;
          $query[ 'body' ]['query']   =   $query_body['query'];
          $query[ 'body' ]['size']    =   0;
          // die( json_encode( $query ) );
          return $this->client->search( $query );
      }
      catch( \Aws\ElasticsearchService\Exception $e ){
          return  response()->json( [ 'message' => $e->getMessage() ], $e->getCode() );
      }
    }

    public function aggs_query(){
        $aggs    =   [];
        $prefix  =   '';
        foreach($this->aggs as $key => $agg){
            $agg_name   =   str_replace('.', '_', $agg['field']);
            $prefix    .=  'aggs.'.$agg_name ;
            if($agg['field'] == 'timestamp.created'){
                $interval = $this->date_interval();
                array_set($aggs, $prefix.'.date_histogram.field', $agg['field']);
                array_set($aggs, $prefix.'.date_histogram.interval', array_get($interval,'interval'));
                array_set($aggs, $prefix.'.date_histogram.format', array_get($interval,'format'));
            }else{
                array_set($aggs, $prefix.'.terms.field', $agg['field']);
                array_set($aggs, $prefix.'.terms.size', $agg['size']);
            }
            $prefix    .=  '.';
        }
        $source     =   $this->source_query();
        if(!empty($source)){
            array_set($aggs, $prefix.'aggs', $source);
        }
        return $aggs;
    }

    private function source_query(){
        $source =   [];
        if(!is_null($this->source) && isset($this->source['fields'])){
            $source['hits']['top_hits']['_source'] = $this->source['fields'];
            $source['hits']['top_hits']['size']    = 1;
        }
        return $source;
    }

    private function date_interval(){

        $interval['interval'] = 'year';
        $interval['format'] = 'yyyy';

        $from   = array_get($this->search_array, 'timestamp.created.from', '2018-05-03');
        $to     = array_get($this->search_array, 'timestamp.created.to', date('Y-m-d'));
        $diff   = abs(strtotime($to) - strtotime($from));
        $years  = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        // dd($years,$months, $days);
        if($years !== 0.0){
            array_set($interval, 'interval', 'year');
            array_set($interval, 'format', 'yyyy');
        }
        else if($months !== 0.0){
            array_set($interval, 'interval', '1M');
            array_set($interval, 'format', 'MMM');
        }
        else if($days <= 7 ){
            array_set($interval, 'interval', '1d');
            array_set($interval, 'format', "EEEEEE");
        }
        else if($days !== 0.0){
            array_set($interval, 'interval', '1d');
            array_set($interval, 'format', 'yyyy-MM-dd');
        }
        return $interval;
    }

    public function set_source( $search_array ){

        if( array_has( $search_array, 'source' ) ){
            $this->source     =   array_get( $search_array, 'source' );
            array_forget($search_array,'source');
        }
        return $search_array;

    }

}
