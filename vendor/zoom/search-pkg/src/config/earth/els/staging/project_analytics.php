<?php

return [

    // Used to identify the leaf of the resource
    // This is different for different models
    'id'            =>  'project_analytics',
    'connection'    =>  [
        'index'         =>  'project_analytics',
        'primary_key'   =>  '_id',
        'mapping'       =>  load('earth/els/staging/mapping/project_analytics')
    ],
    'query'     =>  [
        '$organic'          =>  ($organic = ['_id', 'action','source','subsource','call_back','country_source','message','settings.status','settings.approved','qualification.status','target.adset_id','target.adset_name','target.campaign_id','target.campaign_name','timestamp.created','timestamp.approved' ]),
        '$ref_project'      =>  ($ref_project = ['project'=> array_get(load('earth/els/project'),'query.organic')]),
        '$ref_user'         =>  ($ref_user = ['user'=> array_get(load('earth/els/user'),'query.organic')]),
        '$ref'              =>  ($ref = array_merge($ref_project,$ref_user)),
        'organic'           =>  array_dot(array_merge(array_undot(array_flip($organic)),$ref))
    ],
    'metadata'  =>  [ 'time_taken' => 'took', 'total' => 'hits.total' ],
    'defaults'  =>  []
];

?>
