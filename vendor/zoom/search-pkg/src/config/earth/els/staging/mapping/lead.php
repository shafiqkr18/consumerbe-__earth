<?php

    return  [

        '_id'               =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],

        # Lead information
        'action'            =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
        'source'            =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
        'subsource'         =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
        'call_back'         =>  [ 'type' => 'boolean' ],
        'message'           =>  [ 'type' => 'text' ],

        'country_source'    =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],

        # Settings
        'settings'      =>  [
            'properties'        =>  [
                'status'            =>  [ 'type' => 'byte', 'default' => 1 ],
                'approved'          =>  [ 'type' => 'boolean', 'default' => true ],
                'flag'              =>  [ 'type' => 'byte' ]
            ]
        ],
        
        # Qualification
        'qualification'      =>  [
            'properties'        =>  [
                'status'            =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive', 'default' => 'approved' ],
                'note'              =>  [ 'type' => 'text', 'default' => '']
            ]
        ],

        # Target
        'target'      =>  [
            'properties'        =>  [
                'adset_id'            =>  [ 'type' => 'keyword', 'default' => '' ],
                'adset_name'          =>  [ 'type' => 'keyword', 'default' => '' ],
                'campaign_id'         =>  [ 'type' => 'keyword', 'default' => '' ],
                'campaign_name'       =>  [ 'type' => 'keyword', 'default' => '' ]
            ]
        ],

        # Time
        'timestamp'     =>  [
            'properties'        =>  [
                'created'           =>  [ 'type' => 'date' ],
                'updated'           =>  [ 'type' => 'date' ],
                'approved'          =>  [ 'type' => 'date' ]
            ]
        ]

    ];

?>
