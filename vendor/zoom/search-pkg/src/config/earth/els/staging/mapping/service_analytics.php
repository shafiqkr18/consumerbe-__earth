<?php

    $ref = [

        # User
        'user'              =>  [
            'properties'        =>  load('earth/els/staging/mapping/user')
        ],

        # Service
        'service'          =>  [
            'properties'        =>  load('earth/els/staging/mapping/service')
        ]

    ];

    return array_merge( load('earth/els/staging/mapping/lead'), $ref );

?>
