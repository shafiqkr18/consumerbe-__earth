<?php

return [

    // Used to identify the leaf of the resource
    // This is different for different models
    'id'            =>  'user',
    'connection'    =>  [
        'index'         =>  'user',
        'primary_key'   =>  '_id',
        'mapping'       =>  load('earth/els/staging/mapping/user')
    ],
    'query'     =>  [
        '$organic'  =>  ($organic = ['_id', 'contact.name','contact.email','contact.phone','contact.nationality','settings.role_id','settings.status','settings.verified','settings.newsletter','settings.reengaged','timestamp.created' ]),
        '$ref'      =>  ($ref = []),
        'organic'   =>  array_dot(array_merge(array_undot(array_flip($organic)),$ref))
    ],
    'metadata'  =>  [ 'time_taken' => 'took', 'total' => 'hits.total' ]
];

?>
