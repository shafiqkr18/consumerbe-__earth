<?php

    return  [

        '_id'                       =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],

        # References
        'ref_no'                    =>  [ 'type' => 'keyword', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],
        'permit_no'                 =>  [ 'type' => 'keyword', 'default' => '', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],
        'web_link'                  =>  [ 'type' => 'text', 'default' => '' ],

        # Location
        'building'                  =>  [ 'type' => 'keyword', 'default' => '', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],
        'city'                      =>  [ 'type' => 'keyword', 'default' => '', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],
        'area'                      =>  [ 'type' => 'keyword', 'default' => '', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],
        'coordinates'               =>  [
            'properties'                =>  [
                'lat'                       =>  [ 'type' => 'float', 'default' => -1, 'weight' => 1 ],
                'lng'                       =>  [ 'type' => 'float', 'default' => -1, 'weight' => 1 ]
            ]
        ],

        # Dimensions
        'bedroom'                   =>  [ 'type' => 'byte', 'default' => -1, 'weight' => 1 ],
        'bathroom'                  =>  [ 'type' => 'byte', 'default' => -1, 'weight' => 1 ],
        'dimension'                 =>  [
            'properties'                =>  [
                'builtup_area'              =>  [ 'type' => 'float', 'default' => 0, 'weight' => 1 ],
                'plot_size'                 =>  [ 'type' => 'float', 'default' => 0 ]
            ]
        ],

        'furnished'                 =>  [ 'type' => 'byte', 'default' => -1 ],
        'amenities'                 =>  [ 'type' => 'array', 'default' => [], 'weight' => 'calc' ],

        # Type
        'rent_buy'                  =>  [ 'type' => 'keyword', 'default' => '', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],
        'residential_commercial'    =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ],
        'type'                      =>  [ 'type' => 'keyword', 'default' => '', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],

        # Marketing
        'writeup'                   =>  [
            'properties'                =>  [
                'title'                     =>  [ 'type' => 'text', 'default' => '', 'weight' => 1 ],
                'description'               =>  [ 'type' => 'text', 'default' => '', 'weight' => 'calc' ]
            ]
        ],

        # Price
        'price'                     =>  [ 'type' => 'integer', 'default' => 0, 'weight' => 1 ],

        # Other
        'images'                    =>  [
            'properties'                =>  [
                'original'                  =>  [ 'type' => 'array', 'default' => [], 'weight' => 'calc' ],
                'portal'                    =>  [ 'type' => 'array', 'default' => [] ]
                ]
        ],

        # Settings
        'settings'                  =>  [
            'properties'                =>  [
                'verified'                  =>  [ 'type' => 'byte', 'default' => 1 ],
                'organic_score'             =>  [ 'type' => 'integer', 'default' => 0 ],
                'chat'                      =>  [ 'type' => 'boolean', 'default' => false ],
                'approved'                  =>  [ 'type' => 'boolean', 'default' => false ],
                'status'                    =>  [ 'type' => 'byte', 'default' => 1 ],
                'is_manual'                 =>  [ 'type' => 'byte', 'default' => 1 ],
                'boost'                     =>  [
                    'properties'                =>  [
                        'status'                =>  [ 'type' => 'byte', 'default' => false ],
                        'from'                  =>  [ 'type' => 'date', 'default' => null ],
                        'to'                    =>  [ 'type' => 'date', 'default' => null ]
                    ]
                ]
            ]
        ],

        # Featured
        'featured'  =>  [
            'properties'    =>  [
                'status'        =>  [ 'type' => 'boolean', 'default' => false ],
                'from'          =>  [ 'type' => 'date', 'default' => null ],
                'to'            =>  [ 'type' => 'date', 'default' => null ]
            ]
        ],

        'video'                     =>  [
            'properties'                    =>  [
                'normal'                        =>  [
                    'properties'                    =>  [
                        'link'                          =>  [ 'type' => 'text', 'default' => '' ]
                    ]
                ],
                'degree'                        =>  [
                    'properties'                    =>  [
                        'link'                          =>  [ 'type' => 'text', 'default' => '' ]
                    ]
                ]
            ]
        ],

        'agent'             =>  [
            'properties'        =>  load('earth/els/production/mapping/agent')
        ],

        'timestamp'             =>  [
            'properties'        =>  [
                'created'           =>  [ 'type' => 'date' ],
                'updated'           =>  [ 'type' => 'date' ]
            ]
        ],

        'cheques'           =>  [ 'type' => 'byte', 'default' => 0, 'weight' => 'calc' ],
        'rental_frequency'  =>  [ 'type' => 'keyword', 'default' => 'Yearly', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],
        'exclusive'         =>  [ 'type' => 'boolean', 'default' => 0, 'weight' => 'calc' ],
        'rooms'             =>  [ 'type' => 'byte' ], 'default' => -1,
        'off_plan'          =>  [ 'type' => 'boolean', 'default' => false ],
        'primary_view'      =>  [ 'type' => 'text', 'default' => '' ],
        'completion_status' =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ]
    ];

?>
