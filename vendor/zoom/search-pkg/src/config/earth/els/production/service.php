<?php

return [

    // Used to identify the leaf of the resource
    // This is different for different models
    'id'            =>  'service',
    'connection'    =>  [
        'index'         =>  'service',
        'primary_key'   =>  '_id',
        'mapping'       =>  load('earth/els/production/mapping/service')
    ],
    'query'     =>  [
        '$organic'  =>  ($organic = ['_id','contact.name','contact.email','contact.phone','contact.logo','contact.website','featured.status','featured.from','featured.to','settings.status','settings.approved','settings.lead.admin.email','type._id','type.name','timestamp.created']),
        '$ref'      =>  ($ref = []),
        'organic'   =>  array_dot(array_merge(array_undot(array_flip($organic)),$ref))
    ],
    'metadata'  =>  [ 'time_taken' => 'took', 'total' => 'hits.total' ]
];

?>
