<?php

return [

    // Used to identify the leaf of the resource
    // This is different for different models
    'id'            =>  'property_analytics',
    'connection'    =>  [
        'index'         =>  'property_analytics',
        'primary_key'   =>  '_id',
        'mapping'       =>  load('earth/els/production/mapping/property_analytics')
    ],
    'query'     =>  [
        '$organic'      =>  ($organic = ['_id', 'action','source','subsource','call_back','message','settings.status','settings.approved','qualification.status','target.adset_id','target.adset_name','target.campaign_id','target.campaign_name','timestamp.created','timestamp.approved' ]),
        '$ref_property' =>  ($ref_property = ['property'=> array_get(load('earth/els/property'),'query.organic')]),
        '$ref_user'     =>  ($ref_user = ['user'=> array_get(load('earth/els/user'),'query.organic')]),
        '$ref'          =>  ($ref = array_merge($ref_property,$ref_user)),
        'organic'       =>  array_dot(array_merge(array_undot(array_flip($organic)),$ref))
    ],
    'metadata'  =>  [ 'time_taken' => 'took', 'total' => 'hits.total' ],
    'defaults'  =>  []
];

?>
