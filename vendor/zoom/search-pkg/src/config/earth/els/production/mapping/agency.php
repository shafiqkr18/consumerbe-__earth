<?php

    return [
        '_id'               =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
        # Contact information
        'contact'           =>  [
            'properties'    =>  [
                'name'          =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                'name_ar'       =>  [ 'type' => 'text' ],
                'email'         =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                'phone'         =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                'whatsapp_link' =>  [ 'type' => 'text', 'default' => '' ],
                'logo'          =>  [ 'type' => 'text', 'default' => cdn_asset('assets/img/default/no-img-available.jpg') ],
                'orn'           =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ],
                'about'         =>  [ 'type' => 'text', 'default' => '' ],
                'about_ar'      =>  [ 'type' => 'text' ],
                'address'       =>  [
                    'properties'    =>  [
                        'address_line_one'  =>  [ 'type' => 'text', 'default' => '' ],
                        'state'             =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ],
                        'city'              =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ],
                        'country'           =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ],
                        'postal_code'       =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ]
                    ]
                ]
            ]
        ],
        # Settings information
        'settings'                  =>  [
            'properties'                =>  [
                'role_id'                   =>  [ 'type' => 'byte', 'default' => 4 ],
                'status'                    =>  [ 'type' => 'byte', 'default' => 1 ],
                'approved'                  =>  [ 'type' => 'boolean', 'default' => true ],
                'chat'                      =>  [ 'type' => 'boolean', 'default' => false ],
                'payment_tier'              =>  [ 'type' => 'boolean', 'default' => false ],
                # Lead Preferences
                'lead'      =>  [
                    'properties'    =>  [
                        'qualification' =>  [ 'type' => 'boolean', 'default' => false ],
                        'admin'         =>  [
                            'properties'    =>  [
                                'email'             =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                                'receive_emails'    =>  [ 'type' => 'boolean', 'default' => false ],
                                'cc'                =>  [ 'type' => 'array', 'default' => [] ],
                                'receive_calls'     =>  [ 'type' => 'boolean', 'default' => false ],
                                'phone'             =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ]
                            ]
                        ],
                        'agent'         =>  [
                            'properties'    =>  [
                                'receive_emails'    =>  [ 'type' => 'boolean', 'default' => false ]
                            ]
                        ],
                        'call_tracking'   =>  [
                            'properties'  =>  [
                                'track'       =>  [ 'type' => 'boolean', 'default' => false ],
                                'phone'       =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ]
                            ]
                        ]
                    ]
                ]
            ]
        ],
        # Featured
        'featured'  =>  [
            'properties'    =>  [
                'status'        =>  [ 'type' => 'boolean', 'default' => false ],
                'from'          =>  [ 'type' => 'date', 'default' => null ],
                'to'            =>  [ 'type' => 'date', 'default' => null ],
                'total'         =>  [
                    'properties'    =>  [
                        'property'         =>  [ 'type' => 'integer', 'default' => 0 ],
                        'project'          =>  [ 'type' => 'integer', 'default' => 0 ],
                        'agent'            =>  [ 'type' => 'integer', 'default' => 0 ]
                    ]
                ]
            ]
        ],
        # Interview
        'interview'             =>  [
            'properties'        =>  [
                'youtube_id'       =>  [ 'type' => 'text', 'default' => '' ]
            ]
        ],
        # Timestamp
        'timestamp'             =>  [
            'properties'        =>  [
                'created'           =>  [ 'type' => 'date' ],
                'updated'           =>  [ 'type' => 'date' ]
            ]
        ]
    ];

?>
