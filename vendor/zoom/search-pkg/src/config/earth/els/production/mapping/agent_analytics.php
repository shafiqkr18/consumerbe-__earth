<?php

    $ref = [

        # User
        'user'              =>  [
            'properties'        =>  load('earth/els/production/mapping/user')
        ],

        # Agent
        'agent'          =>  [
            'properties'        =>  load('earth/els/production/mapping/agent')
        ]

    ];

    return array_merge( load('earth/els/production/mapping/lead'), $ref );

?>
