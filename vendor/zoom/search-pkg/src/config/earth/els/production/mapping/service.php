<?php

    return  [

        '_id'                   =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
        'stub'                  =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],

        # Contact information
        'contact'               =>  [
            'properties'            =>  [
                'name'                  =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                'name_ar'               =>  [ 'type' => 'text', 'default' => '' ],
                'email'                 =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ],
                'phone'                 =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ],
                'logo'                  =>  [ 'type' => 'text', 'default' => cdn_asset('assets/img/default/no-img-available.jpg') ],
                'website'               =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ]
            ]
        ],

        # Marketing
        'writeup'                   =>  [
            'properties'                =>  [
                'title'                     =>  [ 'type' => 'text', 'default' => '' ],
                'description'               =>  [ 'type' => 'text', 'default' => '' ],
                'title_ar'                  =>  [ 'type' => 'text', 'default' => '' ],
                'description_ar'            =>  [ 'type' => 'text', 'default' => '' ],
                'title_ch'                  =>  [ 'type' => 'text', 'default' => '' ],
                'description_ch'            =>  [ 'type' => 'text', 'default' => '' ]
            ]
        ],

        # Images
        'images'                   =>  [
            'properties'              =>  [
                'desktop'   => [ 'type' => 'array', 'default' => [] ],
                'mobile'    => [ 'type' => 'array', 'default' => [] ]
            ]
        ],

        # Featured
        'featured'  =>  [
            'properties'    =>  [
                'status'        =>  [ 'type' => 'boolean', 'default' => false ],
                'from'          =>  [ 'type' => 'date', 'default' => null ],
                'to'            =>  [ 'type' => 'date', 'default' => null ]
            ]
        ],

        # Settings
        'settings'                  =>  [
            'properties'                =>  [
                'status'                    =>  [ 'type' => 'byte', 'default' => 1  ],
                'approved'                  =>  [ 'type' => 'boolean', 'default' => true ],
                # Lead Preferences
                'lead'      =>  [
                    'properties'    =>  [
                        'admin'         =>  [
                            'properties'    =>  [
                                'email'             =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                                'receive_emails'    =>  [ 'type' => 'boolean', 'default' => false ],
                                'cc'                =>  [ 'type' => 'array', 'default' => [] ],
                                'receive_calls'     =>  [ 'type' => 'boolean', 'default' => false ],
                                'phone'             =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ]
                            ]
                        ],
                        'call_tracking'   =>  [
                            'properties'  =>  [
                                'track'       =>  [ 'type' => 'boolean', 'default' => false ],
                                'phone'       =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ]
                            ]
                        ]
                    ]
                ]
            ]
        ],

        #   Timestamp
        'timestamp'             =>  [
            'properties'        =>  [
                'created'           =>  [ 'type' => 'date' ],
                'updated'           =>  [ 'type' => 'date' ]
            ]
        ],

        # Service Information
        'type'             =>  [
            'properties'        =>  load('earth/els/production/mapping/service_type')
        ]
    ];

?>
