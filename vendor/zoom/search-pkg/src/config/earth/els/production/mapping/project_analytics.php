<?php

    $ref =  [

        # User
        'user'              =>  [
            'properties'        =>  load('earth/els/production/mapping/user')
        ],

        # Projects
        'project'          =>  [
            'properties'        =>  load('earth/els/production/mapping/project')
        ]

    ];

    return array_merge( load('earth/els/production/mapping/lead'), $ref );

?>
