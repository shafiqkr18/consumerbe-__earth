<?php

namespace SearchPKG\Traits;

trait ValidationTrait
{

    /**
    * Validate Requests using Valitron
    * 1.- Array dot request data
    * 2.- Format validation rules
    * 3.- Get validation rules
    * 4.- Validate
    */

    public function validate_requests( $request, $check ){

        //1.-
        $v      =   new \Valitron\Validator( $request );

        $request   =   array_dot( $request );

        //2.-
        $validation_rules    =   array();

        foreach( $check as $field => $rules ){

            foreach( $rules as $each_rule ){

                if( is_array($each_rule)){

                    $validation_rules[ $each_rule[ 0 ] ][]   =   array( $each_rule[ 1 ], array( $field ) );

                }
                else{

                    $validation_rules[ $each_rule ][]   =   array( $field );

                }

            }

        }

        //3.-
        foreach( $validation_rules as $field => $rules ){

            if( count( $rules ) == 1 ){

                $validation_rules[ $field ]  =   $rules[0][0];

            }

        }

        //4.-
        $v->rules( $validation_rules );

        if( $v->validate( ) ) {

            return true;

        } else {

            // Errors
            return $v->errors();

        }
    }

    private function validate_query_requests( $request, $request_validation ){

        $query_request   =   [];

        $request   =   array_dot( $request );

        foreach( $request as $key_request => $value_request ){

            if( ends_with( $key_request, 'min') || ends_with( $key_request, 'max') ){

                $key_value      =   str_after( $key_request, '.');

                $key_request    =   str_before( $key_request, '.');

                $value_request   =    [ $key_value => $value_request ];

            }

            if( isset( $request_validation[ $key_request ] ) ){

                if( is_array( $value_request ) ){

                    foreach( $value_request as $key_value => $value ){

                        $query_request[ $key_request ][ $key_value ]   =   $value;

                    }

                }
                else{

                    $query_request[ $key_request ]   =   $value_request;

                }

            }
            else{

                $flat   =   [];

                array_set( $flat, $key_request, $value_request );

                $key_flat   =   key( $flat );

                if( isset( $request_validation[ $key_flat ] ) ){

                    if( $request_validation[ $key_flat ][ 'type' ] == 'array' ){

                        foreach( $flat[ $key_flat ] as $key => $value ){

                            $query_request[ $key_flat ][ $key ]   =   $value;
                        }

                    }
                }

            }

        }

        if( !empty( $query_request ) ){

            return  $query_request;
        }
        else{

            $query_request[ 'match_all' ] = 'match_all';

            return  $query_request;

        }

    }

    /**
    * Check if value is null
    *
    */
    public function null_check( $value = null ){

        // Arrays and Strings
        if( is_string( $value ) ){

            if( $value === null ){

                return false;

            }
            else if( strlen( trim( $value ) ) === 0 ){

                return false;

            }
            else if( empty( $value ) && $value != "0" ){

                return false;

            }
            else{

                return true;

            }

        }
        else if( is_array( $value ) ){

            if( empty( $value ) ){

                return false;

            }
            else{

                return true;

            }

        }
        else if( is_numeric( $value ) ){

            return true;

        }

    }

    /**
    * Unflatten Array - Format Request
    * @param  array $request  [description]
    * @return array           [description]
    */
    public function array_unflat( $request ){

        $array  =   [];

        foreach ($request as $key => $value) {

            array_set($array, $key, $value);

        }

        return $array;

    }
}
