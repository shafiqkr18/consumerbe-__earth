<?php
namespace SearchPKG\Traits;
use SearchPKG\Traits\ValidationTrait;
use App\Traits\JsonValidator;

trait ConnectionTrait
{
    use ValidationTrait;
    use \App\Traits\JsonValidator;

    /*
    |--------------------------------------------------------------------------
    | CONNECT functions
    |--------------------------------------------------------------------------
    |
    |
    */
    public function connect(){
        // 1. Connect to Els
        if( $this->null_check( $this->credentials ) ){
            if( isset( $this->credentials[ 'key' ] ) AND $this->null_check( $this->credentials['key'] ) ){
                if( isset( $this->credentials[ 'secret' ] ) AND $this->null_check( $this->credentials['secret'] ) ){
                    if( isset( $this->credentials[ 'region' ] ) AND $this->null_check( $this->credentials['region'] ) ){
                        if( isset( $this->host ) AND $this->null_check( $this->host ) ){
                            $credentials    = new \Aws\Credentials\Credentials( $this->credentials[ 'key' ], $this->credentials[ 'secret' ] );
                            $signature      = new \Aws\Signature\SignatureV4( 'es', $this->credentials[ 'region' ] );
                            $middleware     = new \Wizacha\Middleware\AwsSignatureMiddleware( $credentials, $signature );
                            $defaultHandler = \Elasticsearch\ClientBuilder::defaultHandler();
                            $awsHandler     = $middleware( $defaultHandler );
                            $clientBuilder  =  \Elasticsearch\ClientBuilder::create();
                            $clientBuilder
                            ->setHandler( $awsHandler )
                            ->setHosts([ $this->host ]);
                            $this->client = $clientBuilder->build();
                            $this->connect_flag      =   true;
                            return $this;
                        }
                        else{
                            return response()->json( array( 'messaging' => 'AWS-ES host not set' ), 403 );
                        }
                    }
                    else{
                        return response()->json( array( 'messaging' => 'AWS-ES Credentials-region not set' ), 403 );
                    }
                }
                else{
                    return response()->json( array( 'messaging' => 'AWS-ES Credentials-secret not set' ), 403 );
                }
            }
            else{
                return response()->json( array( 'messaging' => 'AWS-ES Credentials-key not set' ), 403 );
            }
        }
        else{
            return response()->json( array( 'messaging' => 'AWS-ES Credentials-key, Credentials-secret and Credentials-region not set' ), 403 );
        }
    }

    /*
    |--------------------------------------------------------------------------
    | SETTERS AND GETTERS
    |--------------------------------------------------------------------------
    |
    |
    */
    public function set_index( $index ){
        $this->index        =   $index;
        return $this;
    }

    public function set_configuration_file( $config ){
        $this->config        =   $config;
        return $this;
    }

    public function setClient( $client ){
        $this->client        =   $client;
        return $this;
    }

    public function set_connection_credentials( $credentials ){
        $this->credentials    =   $credentials;
        return $this;
    }

    public function set_host( $host ){
        $this->host        =   $host;
        return $this;
    }

    public function set_page($search_array){
        if( array_has( $search_array, 'page' ) ){
            $this->page     =   array_get( $search_array, 'page' );
            array_forget($search_array,'page');
        }
        return $search_array;
    }

    public function set_sort($search_array){
        if( array_has( $search_array, 'sort' ) ){
            $this->sort     =   $this->translate_to_es_keys(array_get( $search_array, 'sort' ));
            array_forget($search_array,'sort');
        }
        return $search_array;
    }

    public function set_size($search_array){
        if( array_has( $search_array, 'size' ) ){
            $this->size     =   array_get( $search_array, 'size' );
            array_forget($search_array,'size');
        }
        return $search_array;
    }

    public function set_group_by( $search_array ){
        if( array_has( $search_array, 'groupBy' ) ){
            $this->group_by     =   array_get( $search_array, 'groupBy' );
            array_forget($search_array,'groupBy');
        }
        return $search_array;
    }

    public function set_aggs( $search_array ){
        if( array_has( $search_array, 'aggs' ) ){
            $this->aggs     =   array_get( $search_array, 'aggs' );
            array_forget($search_array,'aggs');
        }
        return $search_array;
    }

    public function set_search_array( $search_array ){
        // Extract page, sort, size
        $search_array   =   $this->set_page($search_array);
        $search_array   =   $this->set_sort($search_array);
        $search_array   =   $this->set_size($search_array);
        $search_array   =   $this->set_group_by($search_array);
        $search_array   =   $this->set_aggs($search_array);
        $search_array   =   $this->set_source($search_array);

        $this->search_array     =   $search_array;
        return $this;
    }

    public function set_query_validations($action='organic'){
        $searchable_keys            =   array_keys(array_get($this->config, 'query.'.$action, []));
        $this->query_validations    =   [];
        // dd(array_get($this->config,'connection.mapping'), $searchable_keys);
        foreach($searchable_keys as $key){
            if(str_contains($key,'.')){
                $key    =   str_replace('.','.properties.',$key);
            }
            $mapping_type =  array_get(array_get($this->config,'connection.mapping',[]),$key,'');
            $this->query_validations[$key]  =   array_get(array_get($this->config,'connection.mapping',[]),$key,'');
        }
        $this->query_validations    =   array_undot($this->query_validations);
        return $this;
    }

    public function get_fields($what,$values=[]){
        $mapping        =   array_get( $this->config, 'connection.mapping' );
        $dotted_config  =   array_dot( $mapping );
        $fields         =   [];

        foreach($dotted_config as $key => $value){
            if(ends_with($key,$what)){
                $key_ = str_replace(['.properties.','.'.$what],['.',''], $key);
                array_set( $fields, $key_, $value );
            }
        }
        return $fields;
    }
}
