<?php

namespace SearchPKG;

use Illuminate\Support\ServiceProvider;

class SearchPkgServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Publishing config files
        $this->publishes([
            __DIR__.'/config/earth'        => config_path('earth')
        ], 'config');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->make('SearchPKG\Controllers\SearchController');

    }
}
