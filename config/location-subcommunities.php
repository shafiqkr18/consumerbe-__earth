<?php

    return[
        'Dubai'    =>  [
            'areas' => [
                'The Palm Jumeirah' => [
                    'buildings' => [
                        'Serenia Residences',
                        'Tiara Residences',
                        'Golden Mile'
        	        ]
                ],
                'Akoya Oxygen' => [
                    'buildings' => [
                        'Akoya Selfie',
                        'Akoya Imagine',
                        'Akoya Play',
                        'Akoya Drive',
                        'Akoya Fresh'
        	        ]
                ],
                'Al Barsha' => [
                    'buildings' => [
                        'Al Barsha 1',
                        'Al Barsha 2',
                        'Al Barsha 3'
                    ]
                ],
                'Al Furjan' => [
                    'buildings' => [
                        'Masakin',
                        'Eight'
                    ]
                ],
                'Al Quoz' => [
                    'buildings' => [
                        'Al Quoz 1',
                        'Al Quoz 2',
                        'Al Quoz 3',
                        'Al Quoz 4'
                    ]
                ],
                'Al Quoz Industrial Area' => [
                    'buildings' => [
                        'Al Quoz Industrial Area 1',
                        'Al Quoz Industrial Area 2',
                        'Al Quoz Industrial Area 3',
                        'Al Quoz Industrial Area 4'
                    ]
                ],
                'Al Qusais' => [
                    'buildings' => [
                        'Al Qusais Area'
                    ]
                ],
                'Al Warqaa' => [
                    'buildings' => [
                        'Al Warqaa 1',
                        'Al Warqaa 2',
                        'Al Warqaa 3',
                        'Al Warqaa 4'
                    ]
                ],
                'Arabian Ranches' => [
                    'buildings' => [
                        'Saheel',
                        'Savannah',
                        'Mirador',
                        'Al Mahra',
                        'Terra Nova',
                        'Hattan',
                        'Alvorada'
                    ]
                ],
                'Arabian Ranches 2' => [
                    'buildings' => [
                        'Casa',
                        'Rosa',
                        'Palma',
                        'Samara',
                        'Lila',
                        'Rasha',
                        'Yasmin'
                    ]

                ],
                'Bluewaters' => [
                    'buildings' => [
                        'Bluewaters Residences'
                    ]
                ],
                'Dubai Hills Estate' => [
                    'buildings' => [
                        'Maple'
                    ]
                ],
                'Green Community' => [
                    'buildings' => [
                        'Green Commuity West',
                        'Green Community East'
                    ]

                ],
                'La Mer' => [
                    'buildings' => [
                        'Port de La Mer',
                        'La Mer South Island',
                        'La Mer North Island'
                    ]
                ],
                'Mudon' => [
                    'buildings' => [
                        'Arabella Townhouses',
                        'Arabella'
                    ]

                ],
                'Jumeirah' => [
                    'buildings' => [
                        'Pearl Jumeirah'
                    ]
        	      ]
            ]
        ]
    ]

?>
