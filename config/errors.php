<?php

return [

    'multiple_leaves'   =>  [

        'status'                =>  400,
        'developerMessage'      =>  'Request has more than one leaves.',
        'developerErrorCode'    =>  4000,
        'userMessage'           =>  'Whoops! I did something wrong, give me a moment while I rectify it.',

    ],
    'desired_leaf_not_found'   =>  [

        'status'                =>  400,
        'developerMessage'      =>  'Request does not have the leaf it should. Check if you are addressing the correct endpoint and following the earth format.',
        'developerErrorCode'    =>  4001,
        'userMessage'           =>  'Whoops! I did something wrong, give me a moment while I rectify it.',

    ],
    'invalid_response_format'   =>  [

        'status'                =>  400,
        'developerMessage'      =>  'Response does not match the earth format. CONTACT BACKEND DEV NOW!!!! ',
        'developerErrorCode'    =>  4003,
        'userMessage'           =>  'Whoops! I did something wrong, give me a moment while I rectify it.',

    ],
    'method_not_allowed'   =>  [

        'status'                =>  405,
        'developerMessage'      =>  'Method Not Allowed',
        'developerErrorCode'    =>  4005,
        'userMessage'           =>  'Method Not Allowed',

    ],
    'invalid_request_format'   =>  [

        'status'                =>  400,
        'developerMessage'      =>  'Request does not match the earth format. CONTACT BACKEND DEV NOW!!!! ',
        'developerErrorCode'    =>  4003,
        'userMessage'           =>  'Whoops! I did something wrong, give me a moment while I rectify it.',

    ]

];

?>
