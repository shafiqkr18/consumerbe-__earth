<?php

return [

    // Used to identify the leaf of the resource
    // This is different for different models
    'id'            =>  'favourite_property',
    'connection'    =>  [
        'index'         =>  'favourite_property',
        'keys'            =>  [
          'partition'       =>  'consumer.email',
          'sort'            =>  '_id'
        ],
        'mapping'       =>  [
            '$favourite'    =>  [$favourite=array_merge( array_get( load('earth/ddb/mapping/favourite_property'), 'favourite_property' ), array_get( load('earth/ddb/mapping/favourite_property'), 'favourite_property_search' ) )],
            'fields'        =>  array_merge( load('earth/ddb/mapping/primary'), $favourite )
        ]
    ]
];

?>
