<?php

return [

    '_id'               =>  [ 'type' => 'string' ],
    'type'              =>  [ 'type' => 'string' ],
    'subtype'           =>  [ 'type' => 'string' ],
    'consumer.email'    =>  [ 'type' => 'string' ],
    'consumer.name'     =>  [ 'type' => 'string' ]

];

?>
