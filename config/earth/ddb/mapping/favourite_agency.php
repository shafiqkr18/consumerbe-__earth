<?php

return [
    '$leaf'     =>  ($leaf='agency'),
    '$search'   =>  ($search='search'),
    'favourite_agency'     =>  [
        $leaf.'._id'                                  =>  [ 'type' => 'string' ],
        $leaf.'.contact.name'                         =>  [ 'type' => 'string' ],
        $leaf.'.contact.email'                        =>  [ 'type' => 'string' ],
        $leaf.'.contact.phone'                        =>  [ 'type' => 'string' ],
        $leaf.'.contact.orn'                          =>  [ 'type' => 'string' ],
        $leaf.'.contact.about'                        =>  [ 'type' => 'string' ],
        $leaf.'.contact.address.address_line_one'     =>  [ 'type' => 'string' ],
        $leaf.'.contact.address.state'                =>  [ 'type' => 'string' ],
        $leaf.'.contact.address.city'                 =>  [ 'type' => 'string' ],
        $leaf.'.contact.address.country'              =>  [ 'type' => 'string' ],
        $leaf.'.contact.address.postal_code'          =>  [ 'type' => 'string' ],
        $leaf.'.settings.role_id'                     =>  [ 'type' => 'byte' ],
        $leaf.'.settings.status'                      =>  [ 'type' => 'byte' ],
        $leaf.'.settings.chat'                        =>  [ 'type' => 'boolean' ],
        $leaf.'.settings.lead.admin.email'            =>  [ 'type' => 'string' ],
        $leaf.'.settings.lead.admin.receive_emails'   =>  [ 'type' => 'boolean' ],
        $leaf.'.settings.lead.admin.cc'               =>  [ 'type' => 'string' ],
        $leaf.'.settings.lead.agent.receive_emails'   =>  [ 'type' => 'boolean' ],
        $leaf.'.featured.status'                      =>  [ 'type' => 'boolean' ],
        $leaf.'.featured.from'                        =>  [ 'type' => 'date' ],
        $leaf.'.featured.to'                          =>  [ 'type' => 'date' ]
    ],
    'favourite_agency_search'  =>  [
        $search.'.'.$leaf.'.url'                                   =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'._id'                                  =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.contact.name'                         =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.contact.email'                        =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.contact.phone'                        =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.contact.orn'                          =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.contact.about'                        =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.contact.address.address_line_one'     =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.contact.address.state'                =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.contact.address.city'                 =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.contact.address.country'              =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.contact.address.postal_code'          =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.settings.role_id'                     =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.settings.status'                      =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.settings.chat'                        =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.settings.lead.admin.email'            =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.settings.lead.admin.receive_emails'   =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.settings.lead.admin.cc'               =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.settings.lead.agent.receive_emails'   =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.featured.status'                      =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.featured.from'                        =>  [ 'type' => 'string' ],
        $search.'.'.$leaf.'.featured.to'                          =>  [ 'type' => 'string' ]
    ]
];

?>
