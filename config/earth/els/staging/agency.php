<?php

return [

    // Used to identify the leaf of the resource
    // This is different for different models
    'id'            =>  'agency',
    'connection'    =>  [
        'index'         =>  'agency',
        'primary_key'   =>  '_id',
        'mapping'       =>  load('earth/els/staging/mapping/agency')
    ],
    'query'     =>  [
        '$organic'  =>  ($organic = ['_id','contact.name','contact.email','contact.phone','contact.orn','contact.address.city','contact.address.country','settings.status','settings.approved','settings.chat','settings.payment_tier','settings.lead.admin.receive_emails','settings.lead.agent.receive_emails','featured.status','featured.from','featured.to','timestamp.created']),
        '$ref'      =>  ($ref=[]),
        'organic'   =>  array_merge(array_undot(array_flip($organic)),$ref)
    ],
    'metadata'  =>  [ 'time_taken' => 'took', 'total' => 'hits.total' ]
];

?>
