<?php

return [

    // Used to identify the leaf of the resource
    // This is different for different models
    'id'            =>  'property',
    'connection'    =>  [
        'index'         =>  'property',
        'primary_key'   =>  '_id',
        'mapping'       =>  load('earth/els/staging/mapping/property')
    ],
    'query'     =>  [
        '$organic'  =>  ($organic = ['_id','ref_no','permit_no','building','city','area','coordinates.lat','coordinates.lng','bedroom','bathroom','dimension.builtup_area','dimension.plot_size','furnished','amenities','rent_buy','residential_commercial','type','price','settings.verified','settings.chat','settings.status','settings.approved','settings.boost.status','settings.is_manual','featured.status','featured.from','featured.to','cheques','rental_frequency','exclusive','rooms','off_plan','primary_view','completion_status','timestamp.created']),
        '$budget'   =>  ($budget = ['rent_buy','type','residential_commercial','price','city','settings']),
        '$ref'      =>  ($ref = ['agent'=> array_get(load('earth/els/agent'),'query.organic')]),
        'organic'   =>  array_dot(array_merge(array_undot(array_flip($organic)),$ref)),
        'budget'    =>  array_dot(array_merge(array_undot(array_flip($budget)),$ref))
    ],
    'metadata'  =>  [ 'time_taken' => 'took', 'total' => 'hits.total' ]
];

?>
