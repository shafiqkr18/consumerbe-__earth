<?php

    return  [

        '_id'        =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],

        'name'       =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
        'name_ar'    =>  [ 'type' => 'text', 'default' => '' ],
        'image'      =>  [ 'type' => 'text', 'default' => cdn_asset('assets/img/default/no-img-available.jpg') ],
        'stub'       =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ],

        # Featured
        'featured'  =>  [
            'properties'    =>  [
                'status'        =>  [ 'type' => 'boolean', 'default' => false ],
                'from'          =>  [ 'type' => 'date', 'default' => null ],
                'to'            =>  [ 'type' => 'date', 'default' => null ]
            ]
        ],

        # Settings
        'settings'                  =>  [
            'properties'                =>  [
                'status'                    =>  [ 'type' => 'byte', 'default' => 1 ],
                'approved'                  =>  [ 'type' => 'boolean', 'default' => true ]
            ]
        ],

        #   Timestamp
        'timestamp'             =>  [
            'properties'        =>  [
                'created'           =>  [ 'type' => 'date' ],
                'updated'           =>  [ 'type' => 'date' ]
            ]
        ]
    ];

?>
