<?php

    $ref =  [

        # User
        'user'              =>  [
            'properties'        =>  load('earth/els/staging/mapping/user')
        ],

        # Projects
        'project'          =>  [
            'properties'        =>  load('earth/els/staging/mapping/project')
        ]

    ];

    return array_merge( load('earth/els/staging/mapping/lead'), $ref );

?>
