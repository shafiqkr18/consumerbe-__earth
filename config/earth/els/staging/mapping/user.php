<?php

    return  [

        '_id'           =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],

        # Contact information
        'contact'       =>  [
            'properties'    =>  [
                'name'          =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                'email'         =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                'phone'         =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                'alternative_phone' =>  [ 'type' => 'text', 'default' => '' ],
                'nationality'   =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ]
            ]
        ],

        # Settings
        'settings'      =>  [
            'properties'    =>  [
                'role_id'       =>  [ 'type' => 'byte' ],
                'status'        =>  [ 'type' => 'boolean' ],
                'verified'      =>  [ 'type' => 'boolean' ],
                'newsletter'    =>  [ 'type' => 'boolean' ],
                'reengaged'     =>  [ 'type' => 'boolean' ]
            ]
        ],

        # Timestamp
        'timestamp'     =>  [
            'properties'    =>  [
                'created'       =>  [ 'type' => 'date' ],
                'updated'       =>  [ 'type' => 'date' ]
            ]
        ]

    ];

?>
