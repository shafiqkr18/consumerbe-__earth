<?php

    return  [
        '_id'                       =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],

        # References
        'ref_no'                    =>  [ 'type' => 'keyword', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],
        'name'                      =>  [ 'type' => 'keyword', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],
        'name_ar'                   =>  [ 'type' => 'text', 'weight' => 1, 'default' => '' ],
        'web_link'                  =>  [ 'type' => 'text', 'default' => '' ],

        # Location
        'country'                   =>  [ 'type' => 'keyword', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],
        'country_ar'                =>  [ 'type' => 'text', 'weight' => 1, 'default' => '' ],
        'city'                      =>  [ 'type' => 'keyword', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],
        'city_ar'                   =>  [ 'type' => 'text', 'weight' => 1, 'default' => '' ],
        'area'                      =>  [ 'type' => 'keyword', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],
        'area_ar'                   =>  [ 'type' => 'text', 'weight' => 1, 'default' => '' ],
        'coordinates'               =>  [
            'properties'                =>  [
                'lat'                   =>  [ 'type' => 'float', 'weight' => 1, 'default' => -1 ],
                'lng'                   =>  [ 'type' => 'float', 'weight' => 1, 'default' => -1 ]
            ]
        ],

        # Images
        'image'                     =>  [
            'properties'                =>  [
                'logo'                    =>  [ 'type' => 'text', 'weight' => 'calc', 'default' => cdn_asset('assets/img/default/no-img-available.jpg') ],
                'banner'                  =>  [ 'type' => 'text', 'weight' => 'calc', 'default' => '' ],
                'floor_plans'             =>  [ 'type' => 'nested', 'weight' => 'calc', 'default' => [] ],
                'floor_plans_file'        =>  [ 'type' => 'text', 'weight' => 1, 'default' => '' ],
                'gallery'                 =>  [ 'type' => 'array', 'default' => [] ],
                'portal'                  =>  [
                    'properties'            =>  [
                        'desktop'              =>  [ 'type' => 'array', 'weight' => 'calc', 'default' => [] ],
                        'mobile'               =>  [ 'type' => 'array', 'weight' => 'calc', 'default' => [] ]
                    ]
                ]
            ]
        ],

        # Files
        'files'                   =>  [
            'properties'                =>  [
                'brochure'                     =>  [ 'type' => 'text', 'weight' => 1, 'default' => '' ]
            ]
        ],

        # Marketing
        'writeup'                   =>  [
            'properties'                =>  [
                'title'                     =>  [ 'type' => 'text', 'weight' => 1, 'default' => '' ],
                'description'               =>  [ 'type' => 'text', 'weight' => 'calc', 'default' => '' ],
                'title_ar'                  =>  [ 'type' => 'text', 'weight' => 1, 'default' => '' ],
                'description_ar'            =>  [ 'type' => 'text', 'weight' => 'calc', 'default' => '' ]
            ]
        ],

        # Units information
        'unit'                   =>  [
            'properties'                =>  [
                'count'                       =>  [ 'type' => 'integer', 'weight' => 1 ],
                'property_type'               =>  [ 'type' => 'keyword', 'weight' => 1, 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ],
                'property_type_ar'            =>  [ 'type' => 'text', 'weight' => 1, 'default' => '' ],
                'property_types'              =>  [ 'type' => 'array', 'weight' => 1, 'default' => [] ],
                'property_types_ar'           =>  [ 'type' => 'array', 'weight' => 1, 'default' => [] ],
                'type'                        =>  [ 'type' => 'keyword', 'weight' => 1, 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ],
                'type_ar'                     =>  [ 'type' => 'text', 'weight' => 1, 'default' => '' ],
                'amenities'                   =>  [ 'type' => 'array', 'weight' => 1, 'default' => [] ],
                'amenities_ar'                =>  [ 'type' => 'array', 'weight' => 1, 'default' => [] ]
            ]
        ],

        # Pricing
        'pricing'                   =>  [
            'properties'                =>  [
                'starting'                  =>  [ 'type' => 'integer', 'weight' => 1 ],
                'emi'                       =>  [ 'type' => 'integer' ],
                'currency'                  =>  [ 'type' => 'text', 'default' => '' ],
                'currency_ar'               =>  [ 'type' => 'text', 'default' => '' ],
                'per_sqft'                  =>  [ 'type' => 'integer' ],
                'payment_text'              =>  [ 'type' => 'text', 'default' => '' ],
                'payment_text_ar'           =>  [ 'type' => 'text', 'default' => '' ],
                'payment_plans'             =>  [ 'type' => 'array', 'weight' => 1, 'default' => [] ],
                'payment_plans_ar'          =>  [ 'type' => 'array', 'weight' => 1, 'default' => [] ]
            ]
        ],

        # Videos
        'video'                     =>  [
            'properties'                    =>  [
                'normal'                        =>  [
                    'properties'                    =>  [
                        'link'                          =>  [ 'type' => 'text', 'weight' => 1, 'default' => '' ]
                    ]
                ],
                'degree'                        =>  [
                    'properties'                    =>  [
                        'link'                          =>  [ 'type' => 'text', 'weight' => 1, 'default' => '' ]
                    ]
                ]
            ]
        ],

        # Settings
        'settings'                  =>  [
            'properties'                =>  [
                'organic_score'             =>  [ 'type' => 'integer', 'default' => 0 ],
                'status'                    =>  [ 'type' => 'byte', 'default' => 1 ],
                'approved'                  =>  [ 'type' => 'boolean', 'default' => false ],
                'custom_form'               =>  [ 'type' => 'boolean', 'default' => false ],
                'lead'                      =>  [
                    'properties'                =>  [
                        'email'                     =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ]
                    ]
                ],
                'boost'                     =>  [
                    'properties'                =>  [
                        'status'                    =>  [ 'type' => 'byte', 'default' => 0 ],
                        'level'                     =>  [ 'type' => 'byte', 'default' => 0 ],
                        'from'                      =>  [ 'type' => 'date', 'default' => null ],
                        'to'                        =>  [ 'type' => 'date', 'default' => null ]
                    ]
                ]
            ]
        ],

        # Timestamp
        'timestamp'             =>  [
            'properties'        =>  [
                'created'           =>  [ 'type' => 'date' ],
                'updated'           =>  [ 'type' => 'date' ]
            ]
        ],

        # Others
        'expected_completion_date'    =>  [ 'type' => 'keyword', 'weight' => 1, 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ],
        'expected_completion_date_ar' =>  [ 'type' => 'text', 'default' => '' ],

        'status'                      =>  [ 'type' => 'keyword', 'weight' => 1, 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ],
        # Completion
        'completion'  =>  [
            'properties'    =>  [
                'percentage'   =>  [ 'type' => 'integer', 'default' => 0 ],
                'image_mobile'  =>  [ 'type' => 'text', 'default' => '' ],
                'image_desktop'  =>  [ 'type' => 'text', 'default' => '' ]
            ]
        ],
        # Featured
        'featured'  =>  [
            'properties'    =>  [
                'status'        =>  [ 'type' => 'boolean', 'default' => false ],
                'from'          =>  [ 'type' => 'date', 'default' => null ],
                'to'            =>  [ 'type' => 'date', 'default' => null ]
            ]
        ],

        # Developer Information
        'developer'             =>  [
            'properties'        =>  load('earth/els/staging/mapping/agency')
        ]
    ];

?>
