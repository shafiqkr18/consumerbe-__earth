<?php

    $ref = [

        # User
        'user'              =>  [
            'properties'        =>  load('earth/els/staging/mapping/user')
        ],

        # Agent
        'agent'          =>  [
            'properties'        =>  load('earth/els/staging/mapping/agent')
        ]

    ];

    return array_merge( load('earth/els/staging/mapping/lead'), $ref );

?>
