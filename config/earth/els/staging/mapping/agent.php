<?php

    return [

        '_id'               =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],

        # Contact information
        'contact'           =>  [
            'properties'    =>  [
                'areas'         =>  [ 'type' => 'array', 'default' => [], 'weight' => 1 ],
                'specialty'     =>  [ 'type' => 'array', 'default' => [], 'weight' => 1 ],
                'languages'     =>  [ 'type' => 'array', 'default' => [] ],
                'name'          =>  [ 'type' => 'keyword', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],
                'email'         =>  [ 'type' => 'keyword', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],
                'phone'         =>  [ 'type' => 'keyword', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ],
                'nationality'   =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ],
                'title'         =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ],
                'picture'       =>  [ 'type' => 'text', 'default' => cdn_asset('assets/img/default/no-agent-img.png'), 'weight' => 'calc' ],
                'gender'        =>  [ 'type' => 'keyword', 'default' => '', 'normalizer' => 'normalizer_case_insensitive' ],
                'about'         =>  [ 'type' => 'text', 'default' => '' ],
                'license_no'    =>  [ 'type' => 'keyword', 'default' => '', 'weight' => 1, 'normalizer' => 'normalizer_case_insensitive' ]
            ]
        ],

        # Settings
        'settings'                  =>  [
            'properties'                =>  [
                'organic_score'             =>  [ 'type' => 'integer', 'default' => 0 ],
                'role_id'                   =>  [ 'type' => 'byte', 'default' => 3 ],
                'approved'                  =>  [ 'type' => 'boolean', 'default' => true ],
                'status'                    =>  [ 'type' => 'byte', 'default' => 1 ],
                'is_manual'                 =>  [ 'type' => 'byte', 'default' => 1 ],
                'verified'                  =>  [ 'type' => 'byte', 'default' => 1 ],
                'chat'                      =>  [ 'type' => 'boolean', 'default' => false ]
            ]
        ],

        # Agency information
        'agency'             =>  [
            'properties'        =>  load('earth/els/staging/mapping/agency')
        ],

        # Interview
        'interview'             =>  [
            'properties'        =>  [
                'youtube_id'       =>  [ 'type' => 'text', 'default' => '' ]
            ]
        ],
        
        # Timestamp
        'timestamp'             =>  [
            'properties'        =>  [
                'created'           =>  [ 'type' => 'date' ],
                'updated'           =>  [ 'type' => 'date' ]
            ]
        ],

        # Featured
        'featured'  =>  [
            'properties'    =>  [
                'status'        =>  [ 'type' => 'boolean', 'default' => false ],
                'from'          =>  [ 'type' => 'date', 'default' => null ],
                'to'            =>  [ 'type' => 'date', 'default' => null ]
            ]
        ]
    ];

?>
