<?php

    $ref = [

        # User
        'user'              =>  [
            'properties'        =>  load('earth/els/staging/mapping/user')
        ],

        # Property
        'property'          =>  [
            'properties'        =>  load('earth/els/staging/mapping/property')
        ],

        # Preferences
        'preferences'          =>  [
            "properties"    =>  [
                'bedroom'                   =>  [
                    'properties'                =>  [
                        'min'                       =>  [ 'type' => 'byte' ],
                        'max'                       =>  [ 'type' => 'byte' ]
                    ]
                ],
                'bathroom'                   =>  [
                    'properties'                =>  [
                        'min'                       =>  [ 'type' => 'byte' ],
                        'max'                       =>  [ 'type' => 'byte' ]
                    ]
                ],
                'city'                      =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                'price'                   =>  [
                    'properties'                =>  [
                        'min'                       =>  [ 'type' => 'integer' ],
                        'max'                       =>  [ 'type' => 'integer' ]
                    ]
                ],
                'type'                      =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                'rent_buy'                  =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                'area'                      =>  [ 'type' => 'array' ],
                'agent'                   =>  [
                    'properties'                =>  [
                        '_id'                   =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                        'contact'               =>   [
                            'properties'            =>  [
                                'name'                  =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                                'email'                 =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ]
                            ]
                        ],
                        'agency'                    =>  [
                            'properties'                =>  [
                                '_id'                   =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                                'contact'                  =>   [
                                    'properties'            =>  [
                                        'name'                  =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ],
                                        'email'                 =>  [ 'type' => 'keyword', 'normalizer' => 'normalizer_case_insensitive' ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ];

    return array_merge( load('earth/els/staging/mapping/lead'), $ref );

?>
