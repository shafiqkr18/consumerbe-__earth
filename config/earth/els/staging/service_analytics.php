<?php

return [

    // Used to identify the leaf of the resource
    // This is different for different models
    'id'            =>  'service_analytics',
    'connection'    =>  [
        'index'         =>  'service_analytics',
        'primary_key'   =>  '_id',
        'mapping'       =>  load('earth/els/staging/mapping/service_analytics')
    ],
    'query'     =>  [
        '$organic'          =>  ($organic = ['_id', 'action','source','subsource','call_back','message','settings.status','settings.approved','timestamp.created' ]),
        '$ref_service'      =>  ($ref_service = ['service'=> array_get(load('earth/els/service'),'query.organic')]),
        '$ref_user'         =>  ($ref_user = ['user'=> array_get(load('earth/els/user'),'query.organic')]),
        '$ref'              =>  ($ref = array_merge($ref_service,$ref_user)),
        'organic'           =>  array_dot(array_merge(array_undot(array_flip($organic)),$ref))
    ],
    'metadata'  =>  [ 'time_taken' => 'took', 'total' => 'hits.total' ],
    'defaults'  =>  []
];

?>
