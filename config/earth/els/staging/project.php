<?php

return [

    // Used to identify the leaf of the resource
    // This is different for different models
    'id'            =>  'project',
    'connection'    =>  [
        'index'         =>  'project',
        'primary_key'   =>  '_id',
        'mapping'       =>  load('earth/els/staging/mapping/project')
    ],
    'query'     =>  [
        '$organic'  =>  ($organic = ['_id','ref_no','name','country','city','area','status','coordinates.lat','coordinates.lng','unit.property_type','unit.count','unit.type','pricing.starting','pricing.per_sqft','expected_completion_date','featured.status','featured.from','featured.to','settings.status','settings.approved','timestamp.created']),
        '$ref'      =>  ($ref = ['developer'=> array_get(load('earth/els/developer'),'query.organic')]),
        'organic'   =>  array_dot(array_merge(array_undot(array_flip($organic)),$ref))
    ],
    'metadata'  =>  [ 'time_taken' => 'took', 'total' => 'hits.total' ]
];

?>
