<?php

return [

    // Used to identify the leaf of the resource
    // This is different for different models
    'id'            =>  'agent',
    'connection'    =>  [
        'index'         =>  'agent',
        'primary_key'   =>  '_id',
        'mapping'       =>  load('earth/els/production/mapping/agent')
    ],
    'query'     =>  [
        '$organic'  =>  ($organic = ['_id','contact.areas','contact.languages','contact.name','contact.email','contact.nationality','contact.title','contact.gender','contact.license_no','contact.phone','settings.status','settings.verified','settings.approved','settings.chat','featured.status','featured.from','featured.to','timestamp.created']),
        '$ref'      =>  ($ref = ['agency'=>array_get(load('earth/els/agency'),'query.organic')]),
        'organic'   =>  array_merge(array_undot(array_flip($organic)),$ref)
    ],
    'metadata'  =>  [ 'time_taken' => 'took', 'total' => 'hits.total' ]
];

?>
