<?php

return [

    // Used to identify the leaf of the resource
    // This is different for different models
    'id'            =>  'agent_analytics',
    'connection'    =>  [
        'index'         =>  'agent_analytics',
        'primary_key'   =>  '_id',
        'mapping'       =>  load('earth/els/production/mapping/agent_analytics')
    ],
    'query'     =>  [
        '$organic'      =>  ($organic = ['_id', 'action','source','subsource','call_back','message','settings.status','settings.approved','timestamp.created' ]),
        '$ref_agent'    =>  ($ref_agent = ['agent'=> array_get(load('earth/els/agent'),'query.organic')]),
        '$ref_user'     =>  ($ref_user = ['user'=> array_get(load('earth/els/user'),'query.organic')]),
        '$ref'          =>  ($ref = array_merge($ref_agent,$ref_user)),
        'organic'       =>  array_dot(array_merge(array_undot(array_flip($organic)),$ref))
    ],
    'metadata'  =>  [ 'time_taken' => 'took', 'total' => 'hits.total' ],
    'defaults'  =>  []
];

?>
