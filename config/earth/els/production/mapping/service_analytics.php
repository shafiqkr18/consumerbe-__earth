<?php

    $ref = [

        # User
        'user'              =>  [
            'properties'        =>  load('earth/els/production/mapping/user')
        ],

        # Service
        'service'          =>  [
            'properties'        =>  load('earth/els/production/mapping/service')
        ]

    ];

    return array_merge( load('earth/els/production/mapping/lead'), $ref );

?>
