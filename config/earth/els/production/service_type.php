<?php

return [

    // Used to identify the leaf of the resource
    // This is different for different models
    'id'            =>  'service_type',
    'connection'    =>  [
        'index'         =>  'service_type',
        'primary_key'   =>  '_id',
        'mapping'       =>  load('earth/els/production/mapping/service_type')
    ],
    'query'     =>  [
        '$organic'  =>  ($organic = ['_id','settings.status','settings.approved','featured.status','name','timestamp.created']),
        '$ref'      =>  ($ref = []),
        'organic'   =>  array_dot(array_merge(array_undot(array_flip($organic)),$ref))
    ],
    'metadata'  =>  [ 'time_taken' => 'took', 'total' => 'hits.total' ]
];

?>
