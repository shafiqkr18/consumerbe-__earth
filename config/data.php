<?php
    return [
        "keywords"    =>  [
            "new",
            'daily',
            'cheap',
            'luxury',
            'family',
            'featured',
            'furnished',
            'independent',
            'private-pool'
        ],
        "links"     =>  [
            "zp_consumer"  =>   [
                "local"         =>  "https://consumerfe.app/",
                "staging"       =>  "https://staging.zoomproperty.com/",
                "production"    =>  "https://www.zoomproperty.com/"
            ]
        ]
    ];
