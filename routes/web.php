<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group( [ 'prefix' => 'api' ], function () use ($router) {

    $router->group( [ 'prefix' => 'earth', 'middleware' => ['request'] ], function () use ($router) {

        $router->group( [ 'prefix' => 'property' ], function () use ($router) {

            $router->get( '/',  [ 'uses' => 'Property@index' ] );
            $router->post( '/',  [ 'uses' => 'Property@store' ] );
            $router->group( [ 'prefix' => '{id}' ], function ($model_id) use ($router) {
                $router->get( '/', [ 'uses' => 'Property@show' ] );
                $router->get( '/similar', [ 'uses' => 'Property@similar' ] );
                $router->put( '/', [ 'uses' => 'Property@update' ] );
                $router->delete( '/', [ 'uses' => 'Property@destroy' ] );
            });
        });

        $router->group( [ 'prefix' => 'agent' ], function () use ($router) {

            $router->group( [ 'namespace' => 'Ratings', 'prefix' => '{agent_id}' ], function ($agent_id) use ($router) {
                $router->group( [ 'prefix' => 'ratings' ], function () use ($router) {
                    $router->group( [ 'prefix' => '{id}' ], function ($id) use ($router) {
                        $router->get( '/', [ 'uses' => 'AgentRatings@show' ] );
                        $router->put( '/', [ 'uses' => 'AgentRatings@update' ] );
                    });
                    $router->get( '/', [ 'uses' => 'AgentRatings@index' ] );
                    $router->post( '/', [ 'uses' => 'AgentRatings@store' ] );
                });
            });

            $router->get( '/', [ 'uses' => 'Agent@index' ] );
            $router->post( '/', [ 'uses' => 'Agent@store' ] );
            $router->group( [ 'prefix' => '{id}' ], function ($model_id) use ($router) {
                $router->get( '/', [ 'uses' => 'Agent@show' ] );
                $router->get( '/similar', [ 'uses' => 'Agent@similar' ] );
                $router->put( '/', [ 'uses' => 'Agent@update' ] );
                $router->delete( '/', [ 'uses' => 'Agent@destroy' ] );
            });

        });

        $router->group( [ 'prefix' => 'agency' ], function () use ($router) {

            $router->get( '/', [ 'uses' => 'Agency@index' ] );
            $router->post( '/', [ 'uses' => 'Agency@store' ] );
            $router->group( [ 'prefix' => '{id}' ], function ($model_id) use ($router) {
                $router->get( '/', [ 'uses' => 'Agency@show' ] );
                $router->get( '/similar', [ 'uses' => 'Agency@similar' ] );
                $router->put( '/', [ 'uses' => 'Agency@update' ] );
                $router->delete( '/', [ 'uses' => 'Agency@destroy' ] );
            });

        });

        $router->group( [ 'prefix' => 'project' ], function () use ($router) {

            $router->get( '/', [ 'uses' => 'Project@index' ] );
            $router->post( '/', [ 'uses' => 'Project@store' ] );
            $router->group( [ 'prefix' => '{id}' ], function ($model_id) use ($router) {
                $router->get( '/', [ 'uses' => 'Project@show' ] );
                $router->get( '/similar', [ 'uses' => 'Project@similar' ] );
                $router->put( '/', [ 'uses' => 'Project@update' ] );
                $router->delete( '/', [ 'uses' => 'Project@destroy' ] );
            });

        });

        $router->group( [ 'prefix' => 'service' ], function () use ($router) {

            $router->group( [ 'prefix' => 'type' ], function () use ($router) {
                $router->get( '/', [ 'uses' => 'ServiceType@index' ] );
                $router->post( '/', [ 'uses' => 'ServiceType@store' ] );
                $router->group( [ 'prefix' => '{id}' ], function ($model_id) use ($router) {
                    $router->get( '/', [ 'uses' => 'ServiceType@show' ] );
                    $router->get( '/similar', [ 'uses' => 'ServiceType@similar' ] );
                    $router->put( '/', [ 'uses' => 'ServiceType@update' ] );
                    $router->delete( '/', [ 'uses' => 'ServiceType@destroy' ] );
                });
            });

            $router->get( '/', [ 'uses' => 'Service@index' ] );
            $router->post( '/', [ 'uses' => 'Service@store' ] );
            $router->group( [ 'prefix' => '{id}' ], function ($model_id) use ($router) {
                $router->get( '/similar', [ 'uses' => 'Service@similar' ] );
                $router->get( '/', [ 'uses' => 'Service@show' ] );
                $router->put( '/', [ 'uses' => 'Service@update' ] );
                $router->delete( '/', [ 'uses' => 'Service@destroy' ] );
            });

        });

        $router->group( [ 'prefix' => 'lead' ], function () use ($router) {

            $router->group( [ 'namespace' => 'Notes', 'prefix' => '{lead_id}' ], function ($lead_id) use ($router) {
                $router->group( [ 'prefix' => 'notes' ], function () use ($router) {
                    $router->group( [ 'prefix' => '{id}' ], function ($id) use ($router) {
                        $router->get( '/', [ 'uses' => 'Lead@show' ] );
                        $router->put( '/', [ 'uses' => 'Lead@update' ] );
                    });
                    $router->get( '/', [ 'uses' => 'Lead@index' ] );
                    $router->post( '/', [ 'uses' => 'Lead@store' ] );
                });
            });

            $router->group( [ 'prefix' => '{model}' ], function ($model) use ($router) {
                $router->post( '/', [ 'uses' => 'Lead@store' ] );
                $router->get( '/', [ 'uses' => 'Lead@all' ] );
                $router->group( [ 'prefix' => '{id}' ], function ($model_id) use ($router) {
                    $router->get( '/', [ 'uses' => 'Lead@show' ] );
                    $router->put( '/', [ 'uses' => 'Lead@update' ] );
                    $router->delete( '/', [ 'uses' => 'Lead@destroy' ] );
                });
            });

        });

        $router->group( [ 'prefix' => 'auth' ], function () use ($router) {

            $router->post( '/login', [ 'uses' => 'Auth@login' ] );
            $router->post( '/register', [ 'uses' => 'Auth@register' ] );
            $router->post( '/forgot-password', [ 'uses' => 'Auth@forgot_password' ] );
            $router->put( '/reset-password', [ 'uses' => 'Auth@reset_password' ] );
            $router->put( '/change-password/{id}', [ 'uses' => 'Auth@change_password' ] );

        });

        $router->group( [ 'prefix' => 'analytic' ], function () use ($router) {

            $router->group( [ 'prefix' => '{model}' ], function ($model) use ($router) {
                $router->get( '/total', [ 'uses' => 'Analytics@total_count' ] );
                $router->get( '/action', [ 'uses' => 'Analytics@leads_per_action_count' ] );
                $router->get( '/histogram', [ 'uses' => 'Analytics@leads_histogram' ] );
                $router->get( '/top', [ 'uses' => 'Analytics@top_entity' ] );
                $router->group( [ 'prefix' => 'guideme' ], function ($model) use ($router) {
                    $router->get( '/listings', [ 'uses' => 'Analytics@guide_me_listings' ] );
                    $router->get( '/stats', [ 'uses' => 'Analytics@guide_me_stats' ] );
                });
            });

        });

        $router->group( [ 'prefix' => 'user' ], function () use ($router) {

            $router->group( [ 'prefix' => 'auth' ], function () use ($router) {
                $router->post( 'register', [ 'uses' => 'User@store' ] );
                $router->post( 'login', [ 'uses' => 'User@show' ] );
                $router->post( 'social', [ 'uses' => 'User@social_auth' ] );
                // Verify Email
                $router->post( 'verify', [ 'uses' => 'User@verify_email' ] );
                // Forgot Password
                $router->post( 'forgot', [ 'uses' => 'User@forgot_password' ] );
                // Reset Password
                $router->post( 'reset', [ 'uses' => 'User@reset_password' ] );
            });

            $router->group( [ 'prefix' => '{user_id}' ], function ($user_id) use ($router) {
                // User Details
                $router->get( '/', [ 'uses' => 'User@show' ] );
                // Update User Profile
                $router->put( '/', [ 'uses' => 'User@update' ] );

                // User specific non authed actions
                $router->group( [ 'prefix' => 'auth' ], function () use ($router) {
                    // Change password
                    $router->put( 'change', [ 'uses' => 'User@change_password' ] );
                });

                // Favourite
                $router->group( [ 'prefix' => 'favourite' ], function () use ($router) {
                    // All Favourites
                    $router->get( '/', [ 'uses' => 'Favourite@index' ] );

                    // Favourite Specific actions
                    $router->group( [ 'prefix' => '{model}' ], function ($model) use ($router) {
                        // All of model
                        $router->get( '/', [ 'uses' => 'Favourite@index' ] );
                        // Favourite a search
                        $router->post( '/', [ 'uses' => 'Favourite@store' ] );

                        // Actions on models
                        $router->group( [ 'prefix' => '{model_id}' ], function ($model_id) use ($router) {
                            // Favourite a particular model
                            $router->post( '/', [ 'uses' => 'Favourite@store' ] );
                            // Details of particular favourite
                            $router->get( '/', [ 'uses' => 'Favourite@show' ] );
                            // Delete search
                            $router->delete( '/', [ 'uses' => 'Favourite@destroy' ] );
                        });

                    });

                });

                // Alert
                $router->group( [ 'prefix' => 'alert' ], function () use ($router) {
                    // All Alerts
                    $router->get( '/', [ 'uses' => 'Alert@index' ] );

                    // Alert Specific actions
                    $router->group( [ 'prefix' => '{model}' ], function ($model) use ($router) {
                        // All of model
                        $router->get( '/', [ 'uses' => 'Alert@index' ] );
                        // Alert a search
                        $router->post( '/', [ 'uses' => 'Alert@store' ] );
                        // Delete alert
                        $router->delete( '/', [ 'uses' => 'Alert@destroy' ] );

                        // Actions on models
                        $router->group( [ 'prefix' => '{model_id}' ], function ($model_id) use ($router) {
                            // Alert a particular model
                            $router->post( '/', [ 'uses' => 'Alert@store' ] );
                            // Update an alert
                            $router->put( '/', [ 'uses' => 'Alert@update' ] );
                            // Details of particular alert
                            $router->get( '/', [ 'uses' => 'Alert@show' ] );
                            // Delete alert
                            $router->delete( '/', [ 'uses' => 'Alert@destroy' ] );
                        });

                    });

                });

                // Lead
                $router->group( [ 'prefix' => 'lead' ], function () use ($router) {
                    // All Leads
                    $router->get( '/', [ 'uses' => 'Lead@index' ] );

                    $router->group( [ 'prefix' => '{model}' ], function ($model) use ($router) {
                        // All of model
                        $router->get( '/', [ 'uses' => 'Lead@index' ] );
                    });

                });

            });

        });

        $router->group( [ 'namespace' => 'Alerts', 'prefix' => 'alerts' ], function () use ($router) {
            $router->group( [ 'prefix' => 'agency-performance' ], function () use ($router) {
                $router->get( '/', [ 'uses' => 'AgencyPerformance@index' ] );
                $router->post( '/', [ 'uses' => 'AgencyPerformance@store' ] );
                $router->group( [ 'prefix' => '{id}' ], function ($id) use ($router) {
                    $router->get( '/', [ 'uses' => 'AgencyPerformance@show' ] );
                    $router->put( '/', [ 'uses' => 'AgencyPerformance@update' ] );
                    $router->delete( '/', [ 'uses' => 'AgencyPerformance@destroy' ] );
                });
            });
            $router->group( [ 'prefix' => 'property' ], function () use ($router) {
                $router->get( '/', [ 'uses' => 'Property@index' ] );
            });
        });

        $router->group( [ 'prefix' => 'utility' ], function () use ($router) {

            $router->get( 'locations', [ 'uses' => 'Utility@location_list' ] );
            $router->get( 'locations-coordinates', [ 'uses' => 'Utility@location_coordinates_list' ] );
            $router->get( 'types', [ 'uses' => 'Utility@property_type_list' ] );
            $router->get( 'brokerage-areas', [ 'uses' => 'Utility@broker_areas' ] );

            $router->get( 'brokerage-brokers', [ 'uses' => 'Utility@brokerage_brokers' ] );
            $router->get( 'project-developers', [ 'uses' => 'Utility@project_developers' ] );
            $router->get( 'project-global-developers', [ 'uses' => 'Utility@project_global_developers' ] );
            $router->get( 'services-service-types', [ 'uses' => 'Utility@services_service_types' ] );

            $router->get( 'brokerages', [ 'uses' => 'Utility@all_brokerages' ] );
            $router->get( 'developers', [ 'uses' => 'Utility@all_developers' ] );
            $router->get( 'service-types', [ 'uses' => 'Utility@all_service_types' ] );

            $router->get( 'clients', [ 'uses' => 'Utility@get_clients' ] );
            $router->get( 'agency_email_preferences', [ 'uses' => 'Utility@agency_email_preferences' ] );
            $router->get( 'projects-by-developers', [ 'uses' => 'Utility@projects_by_developers' ] );

            $router->get( 'export-leads/{model}', [ 'uses' => 'Utility@export_leads' ] );

            $router->get( 'brokerage-tracking', [ 'uses' => 'Utility@brokerage_call_tracking' ] );

            $router->get( 'project-completion-status', [ 'uses' => 'Utility@project_completion_status' ] );
            $router->get( 'project-global-completion-status', [ 'uses' => 'Utility@project_global_completion_status' ] );
            $router->get( 'property-completion-status', [ 'uses' => 'Utility@property_completion_status' ] );

            $router->get( 'project-locations', [ 'uses' => 'Utility@project_location_list' ] );
            $router->get( 'project-global-locations', [ 'uses' => 'Utility@project_global_location_list' ] );

            $router->get( 'keywords', [ 'uses' => 'Utility@keywords' ] );

        });

        $router->group( [ 'namespace' => 'Seo', 'prefix' => 'seo' ], function () use ($router) {
            $router->group( [ 'prefix' => 'contents' ], function () use ($router) {
                $router->get( '/', [ 'uses' => 'Contents@index' ] );
                $router->get( 'details', [ 'uses' => 'Contents@get_details' ] );
                $router->post( '/', [ 'uses' => 'Contents@store' ] );
                $router->group( [ 'prefix' => '{id}' ], function ($id) use ($router) {
                    $router->get( '/', [ 'uses' => 'Contents@show' ] );
                    $router->put( '/', [ 'uses' => 'Contents@update' ] );
                });
            });
            $router->group( [ 'prefix' => 'article' ], function () use ($router) {
                $router->group( [ 'prefix' => '{model}' ], function ($model) use ($router) {
                    $router->get( '/', [ 'uses' => 'Article@index' ] );
                    $router->get( 'details', [ 'uses' => 'Article@get_details' ] );
                    $router->post( '/', [ 'uses' => 'Article@store' ] );
                    $router->group( [ 'prefix' => '{id}' ], function ($id) use ($router) {
                        $router->get( '/', [ 'uses' => 'Article@show' ] );
                        $router->put( '/', [ 'uses' => 'Article@update' ] );
                    });
                });
            });
            $router->group( [ 'prefix' => 'links' ], function () use ($router) {
                $router->group( [ 'prefix' => '{model}' ], function ($model) use ($router) {
                    $router->get( '/', [ 'uses' => 'Links@index' ] );
                    $router->get( 'details', [ 'uses' => 'Links@get_details' ] );
                    $router->post( '/', [ 'uses' => 'Links@store' ] );
                    $router->group( [ 'prefix' => '{id}' ], function ($id) use ($router) {
                        $router->get( '/', [ 'uses' => 'Links@show' ] );
                        $router->put( '/', [ 'uses' => 'Links@update' ] );
                    });
                });
            });
        });

        $router->group( [ 'prefix' => 'takeover' ], function () use ($router) {
            $router->get( '/', [ 'uses' => 'Takeover@index' ] );
            $router->post( '/', [ 'uses' => 'Takeover@store' ] );
            $router->group( [ 'prefix' => '{id}' ], function ($id) use ($router) {
                $router->get( '/', [ 'uses' => 'Takeover@show' ] );
                $router->put( '/', [ 'uses' => 'Takeover@update' ] );
            });
        });

        $router->group( [ 'prefix' => 'publicity-video' ], function () use ($router) {
            $router->get( '/', [ 'uses' => 'PublicityVideo@index' ] );
            $router->post( '/', [ 'uses' => 'PublicityVideo@store' ] );
            $router->group( [ 'prefix' => '{id}' ], function ($id) use ($router) {
                $router->get( '/', [ 'uses' => 'PublicityVideo@show' ] );
                $router->put( '/', [ 'uses' => 'PublicityVideo@update' ] );
            });
        });

        $router->group( [ 'namespace' => 'Team', 'prefix' => 'team' ], function () use ($router) {
            $router->group( [ 'prefix' => 'department' ], function () use ($router) {
                $router->get( '/', [ 'uses' => 'Department@index' ] );
                $router->post( '/', [ 'uses' => 'Department@store' ] );
                $router->group( [ 'prefix' => '{id}' ], function ($id) use ($router) {
                    $router->get( '/', [ 'uses' => 'Department@show' ] );
                    $router->put( '/', [ 'uses' => 'Department@update' ] );
                });
            });
            $router->group( [ 'prefix' => 'role' ], function () use ($router) {
                $router->get( '/', [ 'uses' => 'Role@index' ] );
                $router->post( '/', [ 'uses' => 'Role@store' ] );
                $router->group( [ 'prefix' => '{id}' ], function ($id) use ($router) {
                    $router->get( '/', [ 'uses' => 'Role@show' ] );
                    $router->put( '/', [ 'uses' => 'Role@update' ] );
                });
            });
            $router->get( '/', [ 'uses' => 'Team@index' ] );
            $router->post( '/login', [ 'uses' => 'Team@login' ] );
            $router->post( '/register', [ 'uses' => 'Team@store' ] );
            $router->group( [ 'prefix' => '{id}' ], function ($id) use ($router) {
                $router->get( '/', [ 'uses' => 'Team@show' ] );
                $router->put( '/', [ 'uses' => 'Team@update' ] );
                $router->put( '/change-password', [ 'uses' => 'Team@change_password' ] );
            });
        });

        $router->group( [ 'prefix' => 'microsite' ], function () use ($router) {

            $router->get( '/', [ 'uses' => 'Microsite@index' ] );
            $router->post( '/', [ 'uses' => 'Microsite@store' ] );
            $router->get( 'details', [ 'uses' => 'Microsite@get_details' ] );
            $router->group( [ 'prefix' => '{id}' ], function ($model_id) use ($router) {
                $router->get( '/', [ 'uses' => 'Microsite@show' ] );
                $router->put( '/', [ 'uses' => 'Microsite@update' ] );
                $router->delete( '/', [ 'uses' => 'Microsite@destroy' ] );
            });

        });

    });

});
